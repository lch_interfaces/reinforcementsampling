# Import reinforcement sampling module
import RS4CEH
from RS4CEH import MCTS, AbstractBindingSite, Partition, AbstractPath, Pattern, OccupiedSite
import copy
import itertools

def test_nb_patterns_with_same_sites(mcts, pattern_target):
	# Retrieve distinct pattern indexes with same sites
	indexes = set()
	for pattern, index in mcts.equivalency_storage.pattern_index.items():
		if pattern.generic_sites == pattern_target.generic_sites:
			indexes.add(index)
	
	# Return number of unique indexes found
	return(len(indexes))

if __name__ == '__main__':
	# Create MCTS instance
	mcts = MCTS(body_term_order=2, max_length_cutoff=3, nb_pre_exploration_steps=10)
	
	# Define adsorbate binding sites types
	A_site = AbstractBindingSite('A', None, '+', mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', None, '-', mcts.equivalency_storage))
	B1_site = AbstractBindingSite('B', 0, '+', mcts.equivalency_storage)
	B1_site.set_equivalent_to(AbstractBindingSite('B', 0, '-', mcts.equivalency_storage))
	B2_site = AbstractBindingSite('B', 180, '+', mcts.equivalency_storage)
	B2_site.set_equivalent_to(AbstractBindingSite('B', 180, '-', mcts.equivalency_storage))
	B3_site = AbstractBindingSite('B', 90, '+', mcts.equivalency_storage)
	B3_site.set_equivalent_to(AbstractBindingSite('B', 90, '-', mcts.equivalency_storage))
	B4_site = AbstractBindingSite('B', -90, '+', mcts.equivalency_storage)
	B4_site.set_equivalent_to(AbstractBindingSite('B', -90, '-', mcts.equivalency_storage))
	
	# Define surface sites types
	p_site = AbstractBindingSite('p', None, '+', mcts.equivalency_storage)
	p_site.set_equivalent_to(AbstractBindingSite('p', None, '-', mcts.equivalency_storage))
	n_site = AbstractBindingSite('n', None, '+', mcts.equivalency_storage)
	n_site.set_equivalent_to(AbstractBindingSite('n', None, '-', mcts.equivalency_storage))
	
	# Define surface sites positions and connections
	board_size = 2
	for i, j in itertools.product(range(board_size), repeat=2):
		n = i*board_size + j
		if (i+j)%2:
			mcts.surface.add_site(n, p_site)
		else:
			mcts.surface.add_site(n, n_site)
	for i, j in itertools.product(range(board_size), repeat=2):
		n = i*board_size + j
		if j+1 < board_size:
			mcts.surface.add_connections(n, n+1, 1.0, -90, 90)
		elif i+1 < board_size:
			mcts.surface.add_connections(n, n+board_size, 1.0, 180, 0)
	
	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('A',), ('B',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('p',), ('n',)]:
			node_label = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label, new_partition, parent_node=node_label_0)
			for site_type in [('top',)]:
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label)
				for site_number in ([(0,), (3,)] if 'n' in site_atom_type else [(1,), (2,)]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in ([(None,)] if 'A' in ads_type else [(-90,),(0,),(90,),(180,)]): # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	for final_partition in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
		ads_type, site_atom_type, _, site_number, orientation, _ = sites_partition_tree.get_node_partition(final_partition).properties
		dft_energy = (-3 if 'A' in ads_type else -2) + 1*(('A' in ads_type and 'p' in site_atom_type) or ('B' in ads_type and 'n' in site_atom_type))
		print(ads_type, site_atom_type, site_number, orientation, dft_energy)
		sites_partition_tree.add_occupied_site_node(OccupiedSite(site_number[0], None, ads_type, None, None, dihedral=orientation, dft_energy=dft_energy), final_partition)
	
	# Define translation + rotation symmetry
	def custom_symmetry_function(configuration_object):
		# Define configuration data
		partition_tree = configuration_object.partition_tree
		cur_node = configuration_object.current_node
		occupied_sites = configuration_object.occupied_sites
		tree_root = configuration_object.partition_tree_object.tree_root
		site_number_root = configuration_object.partition_tree_object.site_number_root
		
		# All sites numbers are equivalent for first adsorbate
		if cur_node == tree_root and len(occupied_sites) == 0:
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep site number 0
				if configuration_object.get_node_partition(site_number_node).site_number != (0,):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
		
#		# Only some sites are not equivalent for second adsorbate
#		if cur_node == tree_root and len(occupied_sites) == 1:
#			for site_number_node in tuple(partition_tree.successors(site_number_root)):
#				# Only keep sites number 2, 3 or 4
#				if configuration_object.get_node_partition(site_number_node).site_number not in ((2,), (3,), (4,)):
#					# Delete subtree
#					configuration_object.delete_partition_subtree(site_number_node)
	sites_partition_tree.set_symmetry(custom_symmetry_function)
	
	# Initialize linear model with single body terms
	mcts.initialize_model()
	
	# Create target model Hamiltonian
	RS4CEH.target_model_handler = copy.deepcopy(mcts.model_handler)
	path = AbstractPath.from_surface_path([0,1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (B1_site, B2_site), (path,)).get_index()
	RS4CEH.target_model_handler.add_config_to_model({index_target: 1}, -3)
	path = AbstractPath.from_surface_path([0,1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (B3_site, B4_site), (path,)).get_index()
	RS4CEH.target_model_handler.add_config_to_model({index_target: 1}, +1)
	path = AbstractPath.from_surface_path([0,1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (B4_site, B3_site), (path,)).get_index()
	RS4CEH.target_model_handler.add_config_to_model({index_target: 1}, +1)
	
	# Set exploration depth to 2 adsorbates
	mcts.nb_adsorbates = 2
	
	# Run complete Reinforcement Sampling
	for step in range(63):
		mcts.single_run()
	
	# Check number of distinct patterns (2 1-body + 16 2-body (length 2) + 15 2-body (length 3))
	print('{}/{} total patterns found'.format(mcts.equivalency_storage.nb_distinct_patterns, 33))
