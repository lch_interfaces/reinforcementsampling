#!/usr/bin/env python3

import copy
import sys
import itertools
import scipy.stats as stats
import numpy as np
from RS4CEH import MCTS, AbstractBindingSite, Partition, OccupiedSite, AbstractPath, Pattern, DEBUG_MODE
import RS4CEH
import ase.build

# Define surface cell numbers
nx, ny = 6, 6
unit_cell_numbers = [[y*nx+x for x in range(nx)] for y in range(ny)]

# Define translation + rotation symmetry
def custom_symmetry_function(configuration_object):
	# Define configuration data
	partition_tree = configuration_object.partition_tree
	full_partition_tree = configuration_object.sites_partition_tree
	cur_node = configuration_object.current_node
	occupied_sites = configuration_object.occupied_sites
	tree_root = configuration_object.partition_tree_object.tree_root
	site_number_root = configuration_object.partition_tree_object.site_number_root

#	# All unit cells are equivalent for first adsorbate
#	if cur_node == tree_root and len(occupied_sites) == 0:
#		for site_number_node in tuple(partition_tree.successors(site_number_root)):
#			# Only keep first unit cell (i.e. site_number = (0,) or (1,))
#			if configuration_object.get_node_partition(site_number_node).site_number[0] > 1:
#				# Delete subtree
#				configuration_object.delete_partition_subtree(site_number_node)

#	# Only some cells are not equivalent for second adsorbate
#	if cur_node == tree_root and len(occupied_sites) == 1:
#		for site_number_node in tuple(partition_tree.successors(site_number_root)):
#			# Only keep cells that equal their minimum image convention with respect first cell
#			if ((configuration_object.get_node_partition(site_number_node).site_number[0]//2) % nx > nx//2 or
#			    (configuration_object.get_node_partition(site_number_node).site_number[0]//2) // nx > ny//2):
#				# Delete subtree
#				configuration_object.delete_partition_subtree(site_number_node)
	
	# Prevent close fcc-hcp clashes
	if cur_node == tree_root and len(occupied_sites) >= 1:
		# Retrieve already occupied site numbers
		site_numbers = iter(full_partition_tree.get_site_partition(site).site_number for site in occupied_sites)
		occupied_site_numbers = set(itertools.chain.from_iterable(site_numbers))
		
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep sites that not close from occupied site
			site_number = configuration_object.get_node_partition(site_number_node).site_number[0] # Assume unidentate
			cell_number = site_number//2
			x, y = cell_number%nx, cell_number//nx
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			# Check if fcc site
			if site_number%2 == 0:
				if (site_number+1 in occupied_site_numbers or
				    2*n_W+1 in occupied_site_numbers or
				    2*n_S+1 in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
			else:
				if (site_number-1 in occupied_site_numbers or
				    2*n_E in occupied_site_numbers or
				    2*n_N in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)

def initialize_mcts(nb_steps=10, nb_select=10, kwargs=dict()):
	RS4CEH.DEBUG_MODE = False
	RS4CEH.chemcat.VERBOSE = False

	# Create MCTS instance
#	mcts = MCTS(body_term_order=4, max_length_cutoff=4, max_sites_cutoff=4, nb_pre_exploration_steps=nb_steps, nb_selection=nb_select, exact_no_keep=exact)
	mcts = MCTS(body_term_order=3, max_length_cutoff=3, max_sites_cutoff=3, nb_pre_exploration_steps=nb_steps, nb_selection=nb_select, exact_no_keep=kwargs.get('exact_no_keep', False))
	mcts.min_range = 0.05 # 0.5 # 0.05
	mcts.c = 0 # c
#	mcts.population_bias = False

	# Add config
	for key,value in kwargs.items():
		formatted_value = int(float(value)) if int(float(value)) == float(value) else float(value)
		mcts.__setattr__(key,formatted_value)

	# Define adsorbate binding sites types
	O_site = AbstractBindingSite('O', None, '+', mcts.equivalency_storage)
	O_site.set_equivalent_to(AbstractBindingSite('O', None, '-', mcts.equivalency_storage))
	CO_site = AbstractBindingSite('CO', None, '+', mcts.equivalency_storage)
	CO_site.set_equivalent_to(AbstractBindingSite('CO', None, '-', mcts.equivalency_storage))

	# Define surface sites types
	#fcc_site = AbstractBindingSite('Pd_fcc', 0, '+', mcts.equivalency_storage)
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 120, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 240, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 180, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 60, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 300, '-', mcts.equivalency_storage))
	#hcp_site = AbstractBindingSite('Pd_hcp', 0, '+', mcts.equivalency_storage)
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 120, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 240, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 180, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 60, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 300, '-', mcts.equivalency_storage))
	# Zacros do not consider oriented surface sites
	fcc_site = AbstractBindingSite('Pd_fcc', None, '+', mcts.equivalency_storage)
	fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', None, '-', mcts.equivalency_storage))
	hcp_site = AbstractBindingSite('Pd_hcp', None, '+', mcts.equivalency_storage)
	hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', None, '-', mcts.equivalency_storage))

	# Define surface sites positions and connections (Set arbitrary position to →)
	for y in range(ny):
		for x in range(nx):
			n = unit_cell_numbers[y][x]
			mcts.surface.add_site(2*n, fcc_site)
			mcts.surface.add_site(2*n+1, hcp_site)
	for y in range(ny):
		for x in range(nx):
			# Defines adjacent unit cells
			n = unit_cell_numbers[y][x]
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			n_NW = unit_cell_numbers[(y-1)%ny][(x-1)%nx]
			n_SE = unit_cell_numbers[(y+1)%ny][(x+1)%nx]
			# Add fcc-fcc connections
			mcts.surface.add_connections(2*n, 2*n_E, 1.0, 0)
			mcts.surface.add_connections(2*n, 2*n_N, 1.0, 60)
			mcts.surface.add_connections(2*n, 2*n_NW, 1.0, 120)
			# Add hcp-hcp connections
			mcts.surface.add_connections(2*n+1, 2*n_E+1, 1.0, 0)
			mcts.surface.add_connections(2*n+1, 2*n_N+1, 1.0, 60)
			mcts.surface.add_connections(2*n+1, 2*n_NW+1, 1.0, 120)
			# Add fcc-hcp connections
			mcts.surface.add_connections(2*n, 2*n+1, 0.58, 30)
			mcts.surface.add_connections(2*n, 2*n_W+1, 0.58, 150)
			mcts.surface.add_connections(2*n, 2*n_S+1, 0.58, -90)

	#
	# Setup ASE geometry handler
	#
	mcts.geometries_handler.set_database_filename('geometries.db')
	
	# Create surface geometry (with atom indexes left->right & top->down)
	slab = ase.build.fcc111('Pd', size=(nx,ny,3), vacuum=10)
	slab.positions = np.array(sorted(slab.positions, key=lambda x:(-x[2],-x[1],x[0])))
	
	# Create adsorbates geometry
	CO_ads = ase.build.molecule('CO')
	CO_ads.positions = -CO_ads.positions
	O_ads = ase.build.molecule('O')
	
	# Store geometries
	mcts.geometries_handler.set_surface(slab, 0)
	mcts.geometries_handler.set_adsorbate('CO', CO_ads, 0)
	mcts.geometries_handler.set_adsorbate('O', O_ads, 0)

	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('O',), ('CO',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('Pd',)]:
			node_label_1 = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label_1, new_partition, parent_node=node_label_0)
			for site_type in [('fcc',), ('hcp',)]:
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label_1)
				for site_number in ([(i,) for i in range(2*nx*ny)[::2]] if 'fcc' in site_type else [(i,) for i in range(2*nx*ny)[1::2]]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in [(None,)]: # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	bond_vectors = {'fcc': (1.375,0.794,2), 'hcp': (2.751,1.588,2)}
	dft_single_body = {(('O',), ('fcc',)): -1.0932, (('O',), ('hcp',)): -0.9302, (('CO',), ('fcc',)): -1.9276, (('CO',), ('hcp',)): -1.9047}
	for final_partition in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
		ads_type, site_atom_type, site_type, site_number, orientation, chirality = sites_partition_tree.get_node_partition(final_partition).properties
		cell_number = site_number[0]//2
		dft_energy = dft_single_body[(ads_type, site_type)]
		print(ads_type, site_type, site_number, dft_energy)
		bond_vector = bond_vectors[site_type[0]]
		sites_partition_tree.add_occupied_site_node(OccupiedSite(cell_number, -1, ads_type[0], 0, -1, -1, bond_vector=bond_vector, theta=None, bond_dihedral=orientation[0], dft_energy=dft_energy), final_partition)

	# Define translation + rotation symmetry
	sites_partition_tree.set_symmetry(custom_symmetry_function)

	# Initialize linear model with single body terms
	mcts.initialize_model()

	#
	# Create target model Hamiltonian
	#
	target_model_handler = copy.deepcopy(mcts.model_handler)

	# Add all O fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2288)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0050)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0421)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('O_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0641)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0142)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_fcc_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0405)

	# Add all O hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2274)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0050)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0425)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('O_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0641)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0144)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_hcp_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0405)

	# Add all O fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2089)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0179)

	# Add all CO fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2849)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0097)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0076)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0306)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0127)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_fcc_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0016)

	# Add all CO hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2795)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0086)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0059)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0344)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0126)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_hcp_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0047)

	# Add all CO fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2142)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0156)

	# Add all CO-O fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_fcc-fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2185)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_fcc_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0063)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	print('2O-CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1644)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (O_site, O_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('2O-CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0810)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	print('O-2CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0713)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (CO_site, CO_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('O-2CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0592)

	# Add all CO-O hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_hcp-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2009)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_hcp-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0834)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	print('2O-CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2350)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (O_site, O_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('2O-CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.1046)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	print('O-2CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2089)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (CO_site, CO_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('O-2CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.1143)

	# Add all O-CO fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('O-CO_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('O-CO_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1677)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0174)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1239)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0425)
	
	return(mcts, target_model_handler)

nb_ads = [int(sys.argv[2])]*int(sys.argv[5]) # [2]*5+[3]*10+[4]*15+[5]*20+[6]*50
nb_steps = nb_select = int(sys.argv[1])
kwargs = dict()
if len(sys.argv) > 6:
#	nb_select = int(sys.argv[6])
#	exact = True
#	c = 0
#else:
#	nb_select = nb_steps
#	exact = False
#	c = 1
	for key, value in zip(sys.argv[6::2],sys.argv[7::2]):
		kwargs[key] = value

def run_RS(nb_ads, mcts, target_model_handler, exploration=50):
	energy_RS = []
	coverage_RS = []
	trace_rs = []
	trace_rs_best = []
	mcts.random = True
	nb_steps = mcts.nb_pre_exploration_steps
	mcts.nb_pre_exploration_steps = mcts.nb_selection
#	mcts.exhaustive_search = True
	# Run RS enabled
	for nb_adsorbates in nb_ads:
		while True:
			new_deviation = mcts.single_run(nb_adsorbates=nb_adsorbates, target_model_handler=target_model_handler)
			if new_deviation:
				break
		new_count = np.count_nonzero(np.array(mcts.model_handler.active_indexes) < 56)
		energy_RS.append(new_deviation)
		coverage_RS.append(new_count)
		
		active_filter = np.array(mcts.model_handler.active_indexes) < 56
		X = mcts.model.observations[:,active_filter][:,4:]
		eig = np.linalg.eigvals(np.dot(X.T,X))
		trace = abs((1/eig).sum())
		
		best_filter = np.in1d(mcts.model_handler.active_indexes, np.array(target_model_handler.active_indexes)[np.argsort(np.abs(target_model_handler.model.parameters))[:-8][-12:]])
		X_best = mcts.model.observations[:,best_filter]
		eig_best = np.linalg.eigvals(np.dot(X_best.T,X_best))
		trace_best = abs((1/eig_best).sum())
		
		trace_rs.append(trace)
		trace_rs_best.append(trace_best)
		
		print(len(energy_RS), '\t', '{:8.4g}'.format(np.mean(energy_RS)), '\t', '{:8.4g}'.format(new_deviation), '\t', new_count, '\t', len(mcts.model_handler.active_indexes), '\t', mcts.model.nb_core_obs, '\t', '{:8.4g}'.format(trace), '\t', '{:8.4g}'.format(trace_best))
		mcts.exploration_quality += 0
		if len(energy_RS) == exploration:
			mcts.random = False
#			mcts.random_rollout = True
			mcts.nb_pre_exploration_steps = nb_steps
#			mcts.exhaustive_search = False
	mean_RS = sum([energy for energy in energy_RS if energy])/len([energy for energy in energy_RS if energy])

	print(X.sum(), X_best.sum())

#	RS_values = dict()
#	for index, value in zip(mcts.model_handler.active_indexes, mcts.model_handler.model.contributions):
#		RS_values[index] = value

#	target_values = dict()
#	for index, value in zip(target_model_handler.active_indexes, target_model_handler.model.contributions):
#		target_values[index] = value

#	err_RS = 0
#	for index in set(RS_values).union(target_values):
#		err_RS += (RS_values.get(index,0) - target_values.get(index,0))**2

#	active_filter = np.array(mcts.model_handler.active_indexes) < 56
#	X = np.dot(mcts.model.dupl_config_set, mcts.model.core_config_set)[:,active_filter][4:,4:]
#	eig = np.linalg.svd(np.dot(X.T,X))[1]
#	rs_trace = (1/eig[eig > 1e-5]).sum() # mcts.model.norm_empiric_variance[active_filter][:,active_filter][4:,4:].trace()

##	active_filter = np.array(target_model_handler.active_indexes) < 56
##	target_trace = target_model_handler.model.norm_empiric_variance[active_filter][:,active_filter][4:,4:].trace()
#	
#	best_filter = np.argsort(abs(mcts.model.contributions[active_filter][4:]))[-20:] 
#	X_best = X[:,best_filter] 
#	eig_best = np.linalg.svd(np.dot(X_best.T,X_best))[1] 
#	rs_trace_best = (1/eig_best[eig_best > 1e-5]).sum()
#	
	mcts.random = True
	score_deviation = 0
	for i in range(100):
		print(i,'\r',end='')
		res = []
		while not res:
			res = mcts.pre_exploration(nb_steps=1)
		config_dict = mcts.tree.nodes[res[0][0]]['storage'].node_config
		score_deviation += (mcts.model_handler.predict_deviation(config_dict)-target_model_handler.predict_deviation(config_dict))**2
	score_deviation = np.sqrt(score_deviation)
	
	return((mean_RS, score_deviation, trace, trace_best), (trace_rs, trace_rs_best))


def run_random(nb_ads, mcts, target_model_handler):
	# Run random
	mcts.random = True
	mcts.nb_pre_exploration_steps = mcts.nb_selection # 1
#	mcts.model_handler.active_indexes = mcts.model_handler.active_indexes[:4]
	energy_rand = []
	coverage_rand = []
	trace_rand = []
	trace_rand_best = []
	for nb_adsorbates in nb_ads:
		while True:
			new_deviation = mcts.single_run(nb_adsorbates=nb_adsorbates, target_model_handler=target_model_handler)
			if new_deviation:
				break
	#		print(new_deviation)
		new_count = np.count_nonzero(np.array(mcts.model_handler.active_indexes) < 56)
		energy_rand.append(new_deviation)
		coverage_rand.append(new_count)
		
		active_filter = np.array(mcts.model_handler.active_indexes) < 56
		X = mcts.model.observations[:,active_filter][:,4:]
		eig = np.linalg.eigvals(np.dot(X.T,X))
		trace = abs((1/eig).sum())
		
		best_filter = np.in1d(mcts.model_handler.active_indexes, np.array(target_model_handler.active_indexes)[np.argsort(np.abs(target_model_handler.model.parameters))[:-8][-12:]])
		X_best = mcts.model.observations[:,best_filter]
		eig_best = np.linalg.eigvals(np.dot(X_best.T,X_best))
		trace_best = abs((1/eig_best).sum())
		
		trace_rand.append(trace)
		trace_rand_best.append(trace_best)
		
		print(len(energy_rand), '\t', '{:8.4g}'.format(np.mean(energy_rand)), '\t', '{:8.4g}'.format(new_deviation), '\t', new_count, '\t', len(mcts.model_handler.active_indexes), '\t', mcts.model.nb_core_obs, '\t', '{:8.4g}'.format(trace), '\t', '{:8.4g}'.format(trace_best))
	mean_rand = sum([energy for energy in energy_rand if energy])/len([energy for energy in energy_rand if energy])

	print(X.sum(), X_best.sum())

#	rand_values = dict()
#	for index, value in zip(mcts.model_handler.active_indexes, mcts.model_handler.model.contributions):
#		if index < 1000:
#			rand_values[index] = value

#	target_values = dict()
#	for index, value in zip(target_model_handler.active_indexes, target_model_handler.model.contributions):
#		target_values[index] = value

#	err_rand = 0
#	for index in set(rand_values).union(target_values):
#		err_rand += (rand_values.get(index,0) - target_values.get(index,0))**2

#	active_filter = np.array(mcts.model_handler.active_indexes) < 56
#	X = np.dot(mcts.model.dupl_config_set, mcts.model.core_config_set)[:,active_filter][4:,4:]
#	eig = np.linalg.svd(np.dot(X.T,X))[1]
#	rand_trace = (1/eig[eig > 1e-5]).sum() # np.linalg.inv(np.dot(X,X.T)).trace() # mcts.model.norm_empiric_variance[active_filter][:,active_filter][4:,4:].trace()
#	
#	best_filter = np.argsort(abs(mcts.model.contributions[active_filter][4:]))[-20:] 
#	X_best = X[:,best_filter] 
#	eig_best = np.linalg.svd(np.dot(X_best.T,X_best))[1] 
#	rand_trace_best = (1/eig_best[eig_best > 1e-5]).sum()
	
	mcts.random = True
	score_deviation = 0
	for i in range(100):
		print(i,'\r',end='')
		res = []
		while not res:
			res = mcts.pre_exploration(nb_steps=1)
		config_dict = mcts.tree.nodes[res[0][0]]['storage'].node_config
		score_deviation += (mcts.model_handler.predict_deviation(config_dict)-target_model_handler.predict_deviation(config_dict))**2
	score_deviation = np.sqrt(score_deviation)
	
	return((mean_rand, score_deviation, trace, trace_best), (trace_rand, trace_rand_best))

nb_restart = int(sys.argv[4])
exploration = int(sys.argv[3])

values_RS = []
traces_RS = []
for i in range(nb_restart):
	RS4CEH.np.random.seed(i)
	mcts_rs, target_model_handler = initialize_mcts(nb_steps=nb_steps, nb_select=nb_select, kwargs=kwargs)
	stat, trace = run_RS(nb_ads, mcts_rs, target_model_handler, exploration=exploration)
	values_RS.append(stat)
	traces_RS.append(trace)
mean_RS, err_RS, rs_trace, rs_trace_best = np.mean(values_RS, axis=0)
mean_RS_std, err_RS_std, rs_trace_std, rs_trace_best_std = np.std(values_RS, axis=0)

values_rand = []
traces_rand = []
for i in range(nb_restart):
	RS4CEH.np.random.seed(i)
	mcts, target_model_handler = initialize_mcts(nb_steps=nb_steps, nb_select=nb_select, kwargs=kwargs)
	stat, trace = run_random(nb_ads, mcts, target_model_handler)
	values_rand.append(stat)
	traces_rand.append(trace)
mean_rand, err_rand, rand_trace, rand_trace_best = np.mean(values_rand, axis=0)
mean_rand_std, err_rand_std, rand_trace_std, rand_trace_best_std = np.std(values_rand, axis=0)

print(mean_RS, err_RS, rs_trace, rs_trace_best)
print(mean_rand, err_rand, rand_trace, rand_trace_best)

#print(mean_RS,mean_rand)
#print(err_RS, err_rand)
print(rs_trace, stats.t.interval(0.95, nb_restart-1, loc=rs_trace, scale=rs_trace_std))
print(rs_trace_best, stats.t.interval(0.95, nb_restart-1, loc=rs_trace_best, scale=rs_trace_best_std))
print(np.array(values_RS)[:,-2:])
print(rand_trace, stats.t.interval(0.95, nb_restart-1, loc=rand_trace, scale=rand_trace_std))
print(rand_trace_best, stats.t.interval(0.95, nb_restart-1, loc=rand_trace_best, scale=rand_trace_best_std))
print(np.array(values_rand)[:,-2:])

# Get nb of computations gain
def get_diff(list1, list_ref):
	if list1[-1] < list_ref[-1]:
		list_best = list1
		list_worse = list_ref
		sign = 1
	else:
		list_best = list_ref
		list_worse = list1
		sign = -1
	for i, val in enumerate(list_best[::-1]):
		if val >= list_worse[-1]:
			return(sign*(i - (val != list_worse[-1])))
nb_diff = []
for trace_rs, trace_rand in zip(traces_RS, traces_rand):
	nb_diff.append(tuple(get_diff(tr_rs, tr_rand) for tr_rs, tr_rand in zip(trace_rs, trace_rand)))
mean_diff, mean_diff_best = np.mean(nb_diff, axis=0)
mean_diff_std, mean_diff_best_std = np.std(nb_diff, axis=0)
print(mean_diff, stats.t.interval(0.95, nb_restart-1, loc=mean_diff, scale=mean_diff_std))
print(mean_diff_best, stats.t.interval(0.95, nb_restart-1, loc=mean_diff_best, scale=mean_diff_best_std))
print(np.array(nb_diff)[:,-2:])
