#!/usr/bin/env python3

import copy
import sys
import itertools
import scipy.stats as stats
import scipy.integrate as scint
import numpy as np
from RS4CEH import MCTS, AbstractBindingSite, Partition, OccupiedSite, AbstractPath, Pattern, DEBUG_MODE
import RS4CEH
import ase.build
import argparse
import json
import sqlite3

TEST_DB_FILENAME = './tests/tests_results_CO_ox_Pd.db'
TEST_ALL_POSITIVE = False

# Define surface cell numbers
nx, ny = 4, 3
unit_cell_numbers = [[y*nx+x for x in range(nx)] for y in range(ny)]

# Define translation + rotation symmetry
def custom_symmetry_function(configuration_object):
	# Define configuration data
	partition_tree = configuration_object.partition_tree
	full_partition_tree = configuration_object.sites_partition_tree
	cur_node = configuration_object.current_node
	occupied_sites = configuration_object.occupied_sites
	tree_root = configuration_object.partition_tree_object.tree_root
	site_number_root = configuration_object.partition_tree_object.site_number_root

	# All unit cells are equivalent for first adsorbate
	if cur_node == tree_root and len(occupied_sites) == 0:
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep first unit cell (i.e. site_number = (0,) or (1,))
			if configuration_object.get_node_partition(site_number_node).site_number[0] > 1:
				# Delete subtree
				configuration_object.delete_partition_subtree(site_number_node)

	# Only some cells are not equivalent for second adsorbate
	if cur_node == tree_root and len(occupied_sites) == 1:
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep cells that equal their minimum image convention with respect first cell
			if ((configuration_object.get_node_partition(site_number_node).site_number[0]//2) % nx > nx//2 or
			    (configuration_object.get_node_partition(site_number_node).site_number[0]//2) // nx > ny//2):
				# Delete subtree
				configuration_object.delete_partition_subtree(site_number_node)
	
	# Prevent close fcc-hcp clashes
	if cur_node == tree_root and len(occupied_sites) >= 1:
		# Retrieve already occupied site numbers
		site_numbers = iter(full_partition_tree.get_site_partition(site).site_number for site in occupied_sites)
		occupied_site_numbers = set(itertools.chain.from_iterable(site_numbers))
		
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep sites that not close from occupied site
			site_number = configuration_object.get_node_partition(site_number_node).site_number[0] # Assume unidentate
			cell_number = site_number//2
			x, y = cell_number%nx, cell_number//nx
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			# Check if fcc site
			if site_number%2 == 0:
				if (site_number+1 in occupied_site_numbers or
				    2*n_W+1 in occupied_site_numbers or
				    2*n_S+1 in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
			else:
				if (site_number-1 in occupied_site_numbers or
				    2*n_E in occupied_site_numbers or
				    2*n_N in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)

def initialize_mcts(nb_steps=10, seed=0, kwargs=dict()):
	RS4CEH.DEBUG_MODE = False
	RS4CEH.chemcat.VERBOSE = False

	# Create MCTS instance
	mcts = MCTS(body_term_order=3, max_length_cutoff=3, max_sites_cutoff=3, nb_pre_exploration_steps=nb_steps, seed=seed)
	mcts.min_range = 0.05 # 0.5 # 0.05
	mcts.c = 0 # c
#	mcts.population_bias = False

	# Add config
	for key,value in kwargs.items():
#		formatted_value = int(float(value)) if int(float(value)) == float(value) else float(value)
		mcts.__setattr__(key,value)#formatted_value)

	# Define adsorbate binding sites types
	O_site = AbstractBindingSite('O', None, '+', mcts.equivalency_storage)
	O_site.set_equivalent_to(AbstractBindingSite('O', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	CO_site = AbstractBindingSite('CO', None, '+', mcts.equivalency_storage)
	CO_site.set_equivalent_to(AbstractBindingSite('CO', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)

	# Define surface sites types
	#fcc_site = AbstractBindingSite('Pd_fcc', 0, '+', mcts.equivalency_storage)
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 120, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 240, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 180, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 60, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 300, '-', mcts.equivalency_storage))
	#hcp_site = AbstractBindingSite('Pd_hcp', 0, '+', mcts.equivalency_storage)
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 120, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 240, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 180, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 60, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 300, '-', mcts.equivalency_storage))
	# Zacros do not consider oriented surface sites
	fcc_site = AbstractBindingSite('Pd_fcc', None, '+', mcts.equivalency_storage)
	fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	hcp_site = AbstractBindingSite('Pd_hcp', None, '+', mcts.equivalency_storage)
	hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)

	# Define surface sites positions and connections (Set arbitrary position to →)
	for y in range(ny):
		for x in range(nx):
			n = unit_cell_numbers[y][x]
			mcts.surface.add_site(2*n, fcc_site)
			mcts.surface.add_site(2*n+1, hcp_site)
	for y in range(ny):
		for x in range(nx):
			# Defines adjacent unit cells
			n = unit_cell_numbers[y][x]
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			n_NW = unit_cell_numbers[(y-1)%ny][(x-1)%nx]
			n_SE = unit_cell_numbers[(y+1)%ny][(x+1)%nx]
			# Add fcc-fcc connections
			mcts.surface.add_connections(2*n, 2*n_E, 1.0, 0)
			mcts.surface.add_connections(2*n, 2*n_N, 1.0, 60)
			mcts.surface.add_connections(2*n, 2*n_NW, 1.0, 120)
			# Add hcp-hcp connections
			mcts.surface.add_connections(2*n+1, 2*n_E+1, 1.0, 0)
			mcts.surface.add_connections(2*n+1, 2*n_N+1, 1.0, 60)
			mcts.surface.add_connections(2*n+1, 2*n_NW+1, 1.0, 120)
			# Add fcc-hcp connections
			mcts.surface.add_connections(2*n, 2*n+1, 0.58, 30)
			mcts.surface.add_connections(2*n, 2*n_W+1, 0.58, 150)
			mcts.surface.add_connections(2*n, 2*n_S+1, 0.58, -90)

	#
	# Setup ASE geometry handler
	#
	mcts.geometries_handler.set_database_filename('geometries.db')
	
	# Create surface geometry (with atom indexes left->right & top->down)
	slab = ase.build.fcc111('Pd', size=(nx,ny,3), vacuum=10)
	slab.positions = np.array(sorted(slab.positions, key=lambda x:(-x[2],-x[1],x[0])))
	
	# Create adsorbates geometry
	CO_ads = ase.build.molecule('CO')
	CO_ads.positions = -CO_ads.positions
	O_ads = ase.build.molecule('O')
	
	# Store geometries
	mcts.geometries_handler.set_surface(slab, 0)
	mcts.geometries_handler.set_adsorbate('CO', CO_ads, 0)
	mcts.geometries_handler.set_adsorbate('O', O_ads, 0)

	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('O',), ('CO',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('Pd',)]:
			node_label_1 = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label_1, new_partition, parent_node=node_label_0)
			for site_type in [('fcc',), ('hcp',)]:
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label_1)
				for site_number in ([(i,) for i in range(2*nx*ny)[::2]] if 'fcc' in site_type else [(i,) for i in range(2*nx*ny)[1::2]]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in [(None,)]: # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	bond_vectors = {'fcc': (1.375,0.794,2), 'hcp': (2.751,1.588,2)}
	dft_single_body = {(('O',), ('fcc',)): -1.0932, (('O',), ('hcp',)): -0.9302, (('CO',), ('fcc',)): -1.9276, (('CO',), ('hcp',)): -1.9047}
	for final_partition in sorted(sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root)):
		ads_type, site_atom_type, site_type, site_number, orientation, chirality = sites_partition_tree.get_node_partition(final_partition).properties
		cell_number = site_number[0]//2
		dft_energy = dft_single_body[(ads_type, site_type)]
		print(ads_type, site_type, site_number, dft_energy)
		bond_vector = bond_vectors[site_type[0]]
		sites_partition_tree.add_occupied_site_node(OccupiedSite(cell_number, -1, ads_type[0], 0, -1, -1, bond_vector=bond_vector, theta=None, bond_dihedral=orientation[0], dft_energy=dft_energy), final_partition)

	# Define translation + rotation symmetry
	sites_partition_tree.set_symmetry(custom_symmetry_function)

	# Initialize linear model with single body terms
	mcts.initialize_model()
	mcts.model.eps = 1e-9

	#
	# Create target model Hamiltonian
	#
	target_model_handler = copy.deepcopy(mcts.model_handler)

	# Add all O fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2288)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0050*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0421)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('O_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0641*(-1 if TEST_ALL_POSITIVE else 1))

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0142*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_fcc_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0405)

	# Add all O hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2274)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0050*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0425)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('O_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0641*(-1 if TEST_ALL_POSITIVE else 1))

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0144*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site, O_site), (path1, path2)).get_index()
	print('O_triplet_hcp_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0405)

	# Add all O fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2089)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0179)

	# Add all CO fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2849)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0097)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0076*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0306)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0127)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_fcc_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0016)

	# Add all CO hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2795)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0086)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0059*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_target, multiplicity=3)
	print('CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0344)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0126)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site, CO_site), (path1, path2)).get_index()
	print('CO_triplet_hcp_bent', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0047)

	# Add all CO fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2142)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0156)

	# Add all CO-O fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_fcc-fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2185)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_fcc_fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0063)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	print('2O-CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1644)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (O_site, O_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('2O-CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0810)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[0][2]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	print('O-2CO_triplet_fcc_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0713)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                                2*unit_cell_numbers[0][1]], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1], 
		                                2*unit_cell_numbers[1][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (CO_site, CO_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('O-2CO_triplet_fcc_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.0592*(-1 if TEST_ALL_POSITIVE else 1))

	# Add all CO-O hcp-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_hcp-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2009)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                               2*unit_cell_numbers[0][1]+1, 
		                               2*unit_cell_numbers[1][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('CO-O_pair_hcp-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0834)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	print('2O-CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2350)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site, O_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (O_site, O_site, CO_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('2O-CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.1046*(-1 if TEST_ALL_POSITIVE else 1))

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[0][2]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	print('O-2CO_triplet_hcp_linear', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2089)

	path1 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0]+1, 
		                                2*unit_cell_numbers[0][1]+1], mcts.surface)
	path2 = AbstractPath.from_surface_path([2*unit_cell_numbers[0][1]+1, 
		                                2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site, CO_site), (path1, path2)).get_index()
	# Add pattern equivalency
	index_bis = Pattern(mcts.equivalency_storage, (CO_site, CO_site, O_site), (path1, path2)).get_index()
	mcts.equivalency_storage.set_pattern_multiplicity(index_bis, multiplicity=2)
	index_target = mcts.equivalency_storage.merge_indexes(index_bis, index_target)
	print('O-2CO_triplet_hcp_triangle', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, -0.1143*(-1 if TEST_ALL_POSITIVE else 1))

	# Add all O-CO fcc-hcp lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('O-CO_pair_fcc-hcp_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('O-CO_pair_fcc-hcp_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1677)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, CO_site), (path,)).get_index()
	print('CO_pair_fcc-hcp_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0174)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[1][0]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.9999)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[1][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_2NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.1239)

	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1], 
		                               2*unit_cell_numbers[0][1]+1], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (CO_site, O_site), (path,)).get_index()
	print('O-CO_pair_hcp-fcc_3NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.0425)
	
	return(mcts, target_model_handler)


def run_RS(nb_ads, mcts_DoE, target_model_handler, nb_runs, nb_test_set, nb_rand=None, nb_max_patterns=None, nb_ads_test_set=None):
	global mcts_rs, X, Y, test_set_exact_energies, truncation_levels
	# Run random+DoE-based initial exploration
	mcts_DoE.random = True
	print('Run random+DoE initial sampling')
	if nb_rand is None:
		assert(nb_max_patterns > 0)
		# Sample until all patterns are included at least once
		nb_rand = 0
		while len(mcts_DoE.model_handler.active_indexes) < nb_max_patterns and mcts_DoE.tree.nodes['root']['storage'].nb_deviation < nb_runs:
			mcts_DoE.single_run(nb_adsorbates=nb_ads, target_model_handler=target_model_handler)
			nb_rand += 1
			print(mcts_DoE.stats_report())
		assert(mcts_DoE.tree.nodes['root']['storage'].nb_deviation == nb_rand)
	else:
		# Sample requested number of random+DoE initial configurations
		for config_index in range(nb_rand):
			mcts_DoE.single_run(nb_adsorbates=nb_ads, target_model_handler=target_model_handler)
			print(mcts_DoE.stats_report())
	
	# Save pre-trained mcts instance copy (but keep Pattern equivalency storage in common, to ensure same pattern indexes)
	mcts_rs = copy.deepcopy(mcts_DoE)
	mcts_rs.equivalency_storage = mcts_DoE.equivalency_storage
	
	# Save model
	model_handler_ini = copy.deepcopy(mcts_DoE.model_handler)
	
	# Run remaining of random+DoE-based exploration
	print('\nRun random+DoE exploration\n')
	for config_index in range(nb_rand, nb_runs):
		mcts_DoE.single_run(nb_adsorbates=nb_ads, target_model_handler=target_model_handler)
		print(mcts_DoE.stats_report())
	
	# Run RS-based sampling
	RS_configs = []
	mcts_rs.random = False
	print('\nRun RS-based exploration\n')
	for config_index in range(nb_rand, nb_runs):
		selected_config = mcts_rs.single_run(nb_adsorbates=nb_ads, target_model_handler=target_model_handler)
		print(mcts_rs.stats_report())
		RS_configs.append(selected_config)
	
	assert(mcts_DoE.model_handler.model.nb_obs == mcts_rs.model_handler.model.nb_obs == nb_runs + mcts_DoE.model_handler.model_single.nb_obs)
#	assert(mcts.model_handler.model.nb_regressors == mcts_rs.model_handler.model.nb_regressors == model_handler_ini.model_handler.model.nb_regressors)
	
	# Extract test set from remaining configurations
	print('\nExtracting test set\n')
	mcts_DoE.nb_adsorbates = nb_ads_test_set or nb_ads
	test_set_patterns_dicts = []
	for config_index in range(nb_test_set):
		selected_config = None
		# Select new configuration at random
		while selected_config is None or any(all(site1.properties == site2.properties for site1, site2 in zip(selected_config.occupied_sites, config.occupied_sites)) for config in RS_configs if len(config.occupied_sites) == len(selected_config.occupied_sites)): # Check selected configuration is not redundant with RS-based selection
			selected_node, selected_config, _ = mcts_DoE.pre_exploration(nb_steps=1)[0]
		# Store patterns found
		test_set_patterns_dicts.append(mcts_DoE.tree.nodes[selected_node]['storage'].node_patterns)
	
	# Compute exact energies of test set configurations
	test_set_exact_energies = np.array([target_model_handler.predict_deviation(patterns_dict)+target_model_handler.single_body_energy(patterns_dict) for patterns_dict in test_set_patterns_dicts])
	
	# Estimate quality of both models (with history)
	target_parameters_dict = dict(zip(target_model_handler.active_indexes, target_model_handler.model.parameters))
	sorted_abs_lateral_params = np.sort(np.abs(target_model_handler.model.parameters[target_model_handler.model.parameters < 0.99][4:]))
	truncation_levels = sorted_abs_lateral_params/sorted_abs_lateral_params[-1]
	RS_quality, DoE_quality = [], []
	RS_trace_profile, DoE_trace_profile = [], []
	RS_basic_stats, DoE_basic_stats = [], []
	for mcts, quality_storage, trace_profile, basic_stats in ((mcts_DoE, DoE_quality, DoE_trace_profile, DoE_basic_stats), (mcts_rs, RS_quality, RS_trace_profile, RS_basic_stats)):
		# Extract truncated model observations
		relevant_regressors_filter = np.in1d(mcts.model_handler.active_indexes, target_model_handler.active_indexes) # Filter on active indexes also present in target_model_handler
		active_indexes = np.array(mcts.model_handler.active_indexes)[relevant_regressors_filter]
		single_body_active_indexes_filter = active_indexes < 4
		reordered_target_params = np.array([target_parameters_dict[index] for index in active_indexes])
		X = mcts.model_handler.model.observations[:-(nb_runs-nb_rand),relevant_regressors_filter]
		Y = mcts.model_handler.model.target_values[:-(nb_runs-nb_rand)]
		abs_deviations = np.abs(Y[-nb_rand:] - np.dot(X[-nb_rand:, single_body_active_indexes_filter], reordered_target_params[single_body_active_indexes_filter]))/mcts.nb_adsorbates
		
		# Estimate history of model quality
		for observation, energy in zip(mcts.model_handler.model.observations[-(nb_runs-nb_rand):], mcts.model_handler.model.target_values[-(nb_runs-nb_rand):]):
			# Add configuration to truncated model
			X = np.vstack((X, observation[relevant_regressors_filter]))
			Y = np.hstack((Y, energy))
			b, var_noise, rank, _ = np.linalg.lstsq(X, Y)
			
			# Compute RMSE on model parameters
			param_RMSE = np.sqrt(np.mean((b - reordered_target_params)**2)) # sum((param - target_parameters_dict[index])**2 for index, param in zip(active_indexes, b))
		
			# Compute RMSE on test set
			predicted_energies = np.array([sum(param*patterns_dict.get(index, 0) for index, param in zip(active_indexes, b)) for patterns_dict in test_set_patterns_dicts])
			RMSE = np.sqrt(np.mean((predicted_energies-test_set_exact_energies)**2))
		
			# Compute Tr(covariance_matrix) on relevant subspace
			eig = np.linalg.eigvalsh(np.dot(X.T,X))
			trace = (1/eig).sum()
			
			# Compute Area Under Curve of Tr(NormVar) = f(truncation_level), where truncation_level = min_abs_accepted/max_abs_param_value
			traces = []
			min_RMSE = (RMSE, 0)
			for truncation_value in sorted_abs_lateral_params:
				sub_X = X[:, np.abs(reordered_target_params) >= truncation_value]
				traces.append((1/np.linalg.eigvalsh(np.dot(sub_X.T,sub_X))).sum())
				# Compute RMSE with sub model
				sub_b, _, _, _ = np.linalg.lstsq(sub_X, Y)
				sub_active_indexes = active_indexes[np.abs(reordered_target_params) >= truncation_value]
				sub_pred_ener = np.array([sum(param*patterns_dict.get(index, 0) for index, param in zip(sub_active_indexes, sub_b)) for patterns_dict in test_set_patterns_dicts])
				sub_RMSE = np.sqrt(np.mean((sub_pred_ener-test_set_exact_energies)**2))
				if sub_RMSE < min_RMSE[0]:
					min_RMSE = (sub_RMSE, truncation_value)
			traces = np.array(traces)
			trace_AUC = sum(traces*np.diff(np.hstack((0,truncation_levels)))) # Step function, not scint.trapz(traces, x=truncation_levels)
			
			# Store quality estimations
			quality_storage.append((RMSE, param_RMSE, (var_noise[0]/(Y.size-rank) if var_noise.size > 0 else float('NaN')), trace, trace_AUC, *min_RMSE))
			# Store basic stats
			abs_deviations = np.hstack((abs_deviations, abs(Y[-1] - np.dot(X[-1, single_body_active_indexes_filter], reordered_target_params[single_body_active_indexes_filter]))/mcts.nb_adsorbates))
			basic_stats.append((abs_deviations.mean(), abs_deviations.std(), abs_deviations.min(), abs_deviations.max(), int(len(Y)), int(np.count_nonzero(X.sum(axis=0))), int(rank)))
		# Display best RMSE on sub-system
		print('RMSE relevant: {}, best truncated RMSE {} (truncation_level = {})'.format(RMSE, *min_RMSE))
		print('mean(abs_dev), std(abs_dev), min(abs_dev), max(abs_dev), nb_dev, nb_patterns, rank:', basic_stats[-1])
		trace_profile += list(traces)
	
	# Compute gain in number of computations
	worst_AUC = max(RS_quality[-1][4], DoE_quality[-1][4])
	for DoE_index, quality in enumerate(DoE_quality): # Find index with AUC <= worst_AUC for DoE
		if quality[4] <= worst_AUC: break
	for RS_index, quality in enumerate(RS_quality): # Find index with AUC <= worst_AUC for RS
		if quality[4] <= worst_AUC: break
	nb_computation_gain = DoE_index - RS_index
	
	# Print info
	print('nb computations gain:')
	print(nb_computation_gain)
	print('Final DoE quality (RMSE, param_RMSE, var_noise, Tr(Norm_Var(params)), AUC(idem = f(trunc)), min_trunc_RMSE, trunc_value)')
	print(DoE_quality[-1])
	print('Final RS quality (RMSE, param_RMSE, var_noise, Tr(Norm_Var(params)), AUC(idem = f(trunc)), min_trunc_RMSE, trunc_value)')
	print(RS_quality[-1])
	
	return(nb_computation_gain, RS_quality, DoE_quality, RS_trace_profile, DoE_trace_profile, RS_basic_stats, DoE_basic_stats)


if __name__ == '__main__':
	# Parse command line arguments
	parser = argparse.ArgumentParser(description="Testing tool for RS4CEH on CO@Pd oxydation.")
	parser.add_argument('nb_ads', type=int, help="Number of adsorbates to place on the surface")
#	parser.add_argument('train_set_size', type=int, help="Number of configurations to generate for the training set")
	parser.add_argument('pre_exp_steps', type=int, help="Number of pre-explored configurations to consider during each DoE-based rollout")
	parser.add_argument('nb_repeat', type=int, help="Number of times to repeat the test with a different seed")
	parser.add_argument('nb_test_set', type=int, help="Number of random configurations to include in test set for RMSE evaluation")
	parser.add_argument('--kwargs', type=json.loads, help="Additional options (as a JSON-encoded dict)")
	parser.add_argument('--max-size', type=int, default=100, help="Maximum training set size")
	parser.add_argument('--nb_ads_test_set', type=int, help="Number of adsorbates for test set", default=None)
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument('--nb_random_ini_sample', type=int, help="Number of DoE-optimized random configurations for the initialization", default=None)
	group.add_argument('--nb_patterns_ini_sample', type=int, help="Number of patterns that must be included during the initial DoE-optimized random sampling", default=None)
	args = parser.parse_args()
	
	TEST_ALL_POSITIVE = args.kwargs.get('positive_lateral_interactions', False)
	
	try:
		# Connect to database
		conn = sqlite3.connect(TEST_DB_FILENAME)
		conn.execute("""CREATE TABLE IF NOT EXISTS `results` (
		  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
		  `kwargs` varchar(65535) DEFAULT NULL,
		  `nb_ads` int(8) NOT NULL,
		  `nb_ads_test_set` int(8) DEFAULT NULL,
		  `pre_exp_steps` int(8) NOT NULL,
		  `nb_random_ini_sample` int(8) DEFAULT NULL,
		  `nb_patterns_ini_sample` int(8) DEFAULT NULL,
		  `nb_test_set` int(8) NOT NULL,
		  `max_size` int(8) NOT NULL,
		  `seed` int(8) NOT NULL,
		  `nb_computation_gain` int(8) NOT NULL,
		  `RS_quality` varchar(65535) NOT NULL,
		  `DoE_quality` varchar(65535) NOT NULL,
		  `RS_trace_profile` varchar(65535) NOT NULL,
		  `DoE_trace_profile` varchar(65535) NOT NULL,
		  `RS_basic_stats` varchar(65535) NOT NULL,
		  `DoE_basic_stats` varchar(65535) NOT NULL
		);""")
#		conn.execute("""CREATE TABLE IF NOT EXISTS `max_patterns` (
#		  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
#		  `nb_ads` int(8) NOT NULL,
#		  `max_patterns` int(8) NOT NULL,
#		  `nb_steps` int(8) NOT NULL
#		);""")
		
		# Compute average gain (and quality)
		RS_results = []
		DoE_results = []
		RS_profiles = []
		DoE_profiles = []
		RS_stats = []
		DoE_stats = []
		nb_gains = []
		for seed in range(args.nb_repeat):
			skip_calculation = False
			
			# Search database for already computed result
			for row in conn.execute("""SELECT `nb_computation_gain`, `RS_quality`, `DoE_quality`, `RS_trace_profile`, `DoE_trace_profile`, `RS_basic_stats`, `DoE_basic_stats` FROM `results`
			  WHERE `kwargs`=?
			    AND `nb_ads`=?
			    AND `pre_exp_steps`=?
			    AND `nb_random_ini_sample`"""+("IS NULL" if args.nb_random_ini_sample is None else "=?")+"""
			    AND `nb_patterns_ini_sample`"""+("IS NULL" if args.nb_patterns_ini_sample is None else "=?")+"""
			    AND `nb_test_set`=?
			    AND `max_size`=?
			    AND `seed`=?
			    AND `nb_ads_test_set`"""+("IS NULL" if args.nb_ads_test_set is None else "=?")+"""
			;""", (json.dumps(args.kwargs, sort_keys=True),
			       args.nb_ads,
			       args.pre_exp_steps,
			       args.nb_random_ini_sample if args.nb_patterns_ini_sample is None else args.nb_patterns_ini_sample,
			       args.nb_test_set,
			       args.max_size,
			       seed
			      ) + ((args.nb_ads_test_set,) if args.nb_ads_test_set is not None else tuple())):
				# Use precomputed results and skip actual computation
				print('Found already computed results in database, skipping computations')
				nb_gain, RS_quality, DoE_quality, RS_trace_profile, DoE_trace_profile, RS_basic_stats, DoE_basic_stats = row
				RS_quality = json.loads(RS_quality)
				DoE_quality = json.loads(DoE_quality)
				RS_trace_profile = json.loads(RS_trace_profile)
				DoE_trace_profile = json.loads(DoE_trace_profile)
				RS_basic_stats = json.loads(RS_basic_stats)
				DoE_basic_stats = json.loads(DoE_basic_stats)
				skip_calculation = True
				break
			
			# Perform computation
			if not skip_calculation:
				mcts, target_model_handler = initialize_mcts(nb_steps=args.pre_exp_steps, seed=seed, kwargs=args.kwargs)
				nb_gain, RS_quality, DoE_quality, RS_trace_profile, DoE_trace_profile, RS_basic_stats, DoE_basic_stats = run_RS(args.nb_ads, mcts, target_model_handler, nb_runs=args.max_size, nb_test_set=args.nb_test_set, nb_rand=args.nb_random_ini_sample, nb_max_patterns=args.nb_patterns_ini_sample, nb_ads_test_set=args.nb_ads_test_set)
			# Save results
			RS_results.append(RS_quality)
			DoE_results.append(DoE_quality)
			RS_profiles.append(RS_trace_profile)
			DoE_profiles.append(DoE_trace_profile)
			RS_stats.append(RS_basic_stats)
			DoE_stats.append(DoE_basic_stats)
			nb_gains.append(nb_gain)
			
			# Save results into database
			if not skip_calculation:
				conn.execute("""INSERT INTO `results`
				  VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
				;""", (json.dumps(args.kwargs, sort_keys=True),
				      args.nb_ads,
				      args.nb_ads_test_set,
				      args.pre_exp_steps,
				      args.nb_random_ini_sample,
				      args.nb_patterns_ini_sample,
				      args.nb_test_set,
				      args.max_size,
				      seed,
				      nb_gain,
				      json.dumps(RS_quality),
				      json.dumps(DoE_quality),
				      json.dumps(RS_trace_profile),
				      json.dumps(DoE_trace_profile),
				      json.dumps(RS_basic_stats),
				      json.dumps(DoE_basic_stats)
				     ))
		
		# Compute statistics
		mean_gain = np.mean(nb_gains, axis=0)
		std_gain = np.std(nb_gains, axis=0)
		RS_results_final = [result[-1] for result in RS_results]
		mean_RS_RMSE, mean_RS_param_RMSE, mean_RS_noise, mean_RS_trace_norm, mean_RS_AUC, mean_RS_min_trunc_RMSE, mean_RS_argmin_trunc = np.mean(RS_results_final, axis=0)
		std_RS_RMSE, std_RS_param_RMSE, std_RS_noise, std_RS_trace_norm, std_RS_AUC, std_RS_min_trunc_RMSE, std_RS_argmin_trunc = np.std(RS_results_final, axis=0)
		DoE_results_final = [result[-1] for result in DoE_results]
		mean_DoE_RMSE, mean_DoE_param_RMSE, mean_DoE_noise, mean_DoE_trace_norm, mean_DoE_AUC, mean_DoE_min_trunc_RMSE, mean_DoE_argmin_trunc = np.mean(DoE_results_final, axis=0)
		std_DoE_RMSE, std_DoE_param_RMSE, std_DoE_noise, std_DoE_trace_norm, std_DoE_AUC, std_DoE_min_trunc_RMSE, std_DoE_argmin_trunc = np.std(DoE_results_final, axis=0)
		RS_stats_final = [result[-1] for result in RS_stats]
		mean_RS_mean_abs_dev, mean_RS_std_abs_dev, mean_RS_min_abs_dev, mean_RS_max_abs_dev = np.mean(RS_stats_final, axis=0)[:4]
		std_RS_mean_abs_dev, std_RS_std_abs_dev, std_RS_min_abs_dev, std_RS_max_abs_dev = np.std(RS_stats_final, axis=0)[:4]
		DoE_stats_final = [result[-1] for result in DoE_stats]
		mean_DoE_mean_abs_dev, mean_DoE_std_abs_dev, mean_DoE_min_abs_dev, mean_DoE_max_abs_dev = np.mean(DoE_stats_final, axis=0)[:4]
		std_DoE_mean_abs_dev, std_DoE_std_abs_dev, std_DoE_min_abs_dev, std_DoE_max_abs_dev = np.std(DoE_stats_final, axis=0)[:4]
		
		print('\nAverage nb computations gain:')
		print(mean_gain, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_gain, scale=std_gain))
		print('RS quality (RMSE(E), RMSE(b), Var(noise), tr(NormVar(b)), AUC(idem = f(trunc%)), min_trunc(RMSE(E)) and argmin):')
		print(mean_RS_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_RMSE, scale=std_RS_RMSE))
		print(mean_RS_param_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_param_RMSE, scale=std_RS_param_RMSE))
		print(mean_RS_noise, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_noise, scale=std_RS_noise))
		print(mean_RS_trace_norm, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_trace_norm, scale=std_RS_trace_norm))
		print(mean_RS_AUC, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_AUC, scale=std_RS_AUC))
		print(mean_RS_min_trunc_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_min_trunc_RMSE, scale=std_RS_min_trunc_RMSE))
		print(mean_RS_argmin_trunc, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_argmin_trunc, scale=std_RS_argmin_trunc))
		print('RS stats (mean(abs_dev), std(abs_dev), min(abs_dev), max(abs_dev)):')
		print(mean_RS_mean_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_mean_abs_dev, scale=std_RS_mean_abs_dev))
		print(mean_RS_std_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_std_abs_dev, scale=std_RS_std_abs_dev))
		print(mean_RS_min_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_min_abs_dev, scale=std_RS_min_abs_dev))
		print(mean_RS_max_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_RS_max_abs_dev, scale=std_RS_max_abs_dev))
		print('DoE quality (RMSE(E), RMSE(b), Var(noise), tr(NormVar(b)), AUC(idem = f(trunc%)), min_trunc(RMSE(E)) and argmin):')
		print(mean_DoE_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_RMSE, scale=std_DoE_RMSE))
		print(mean_DoE_param_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_param_RMSE, scale=std_DoE_param_RMSE))
		print(mean_DoE_noise, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_noise, scale=std_DoE_noise))
		print(mean_DoE_trace_norm, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_trace_norm, scale=std_DoE_trace_norm))
		print(mean_DoE_AUC, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_AUC, scale=std_DoE_AUC))
		print(mean_DoE_min_trunc_RMSE, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_min_trunc_RMSE, scale=std_DoE_min_trunc_RMSE))
		print(mean_DoE_argmin_trunc, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_argmin_trunc, scale=std_DoE_argmin_trunc))
		print('DoE stats (mean(abs_dev), std(abs_dev), min(abs_dev), max(abs_dev)):')
		print(mean_DoE_mean_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_mean_abs_dev, scale=std_DoE_mean_abs_dev))
		print(mean_DoE_std_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_std_abs_dev, scale=std_DoE_std_abs_dev))
		print(mean_DoE_min_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_min_abs_dev, scale=std_DoE_min_abs_dev))
		print(mean_DoE_max_abs_dev, stats.t.interval(0.95, args.nb_repeat-1, loc=mean_DoE_max_abs_dev, scale=std_DoE_max_abs_dev))
	finally:
		conn.commit()
		conn.close()
