#!/usr/bin/env python3

import copy
import sys
import itertools
import scipy.stats as stats
import numpy as np
from RS4CEH import MCTS, AbstractBindingSite, Partition, OccupiedSite, AbstractPath, Pattern, DEBUG_MODE
import RS4CEH
import ase.build
from ase.calculators.lj import LennardJones
from ase.optimize import BFGS

# Define surface cell numbers
nx, ny = 4, 3
unit_cell_numbers = [[y*nx+x for x in range(nx)] for y in range(ny)]

# Define translation + rotation symmetry
def custom_symmetry_function(configuration_object):
	# Define configuration data
	partition_tree = configuration_object.partition_tree
	full_partition_tree = configuration_object.sites_partition_tree
	cur_node = configuration_object.current_node
	occupied_sites = configuration_object.occupied_sites
	tree_root = configuration_object.partition_tree_object.tree_root
	site_number_root = configuration_object.partition_tree_object.site_number_root

	# All unit cells are equivalent for first adsorbate
	if cur_node == tree_root and len(occupied_sites) == 0:
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep first unit cell (i.e. site_number = (0,) or (1,))
			if configuration_object.get_node_partition(site_number_node).site_number[0] > 1:
				# Delete subtree
				configuration_object.delete_partition_subtree(site_number_node)

	# Only some cells are not equivalent for second adsorbate
	if cur_node == tree_root and len(occupied_sites) == 1:
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep cells that equal their minimum image convention with respect first cell
			if ((configuration_object.get_node_partition(site_number_node).site_number[0]//2) % nx > nx//2 or
			    (configuration_object.get_node_partition(site_number_node).site_number[0]//2) // nx > ny//2):
				# Delete subtree
				configuration_object.delete_partition_subtree(site_number_node)
	
	# Prevent close fcc-hcp clashes
	if cur_node == tree_root and len(occupied_sites) >= 1:
		# Retrieve already occupied site numbers
		site_numbers = iter(full_partition_tree.get_site_partition(site).site_number for site in occupied_sites)
		occupied_site_numbers = set(itertools.chain.from_iterable(site_numbers))
		
		for site_number_node in tuple(partition_tree.successors(site_number_root)):
			# Only keep sites that not close from occupied site
			site_number = configuration_object.get_node_partition(site_number_node).site_number[0] # Assume unidentate
			cell_number = site_number//2
			x, y = cell_number%nx, cell_number//nx
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			# Check if fcc site
			if site_number%2 == 0:
				if (site_number+1 in occupied_site_numbers or
				    2*n_W+1 in occupied_site_numbers or
				    2*n_S+1 in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
			else:
				if (site_number-1 in occupied_site_numbers or
				    2*n_E in occupied_site_numbers or
				    2*n_N in occupied_site_numbers):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)

# Define precomputation function
def lj_geo_opt(atoms):
	return()
	atoms.set_calculator(LennardJones())
	dyn = BFGS(atoms)
	dyn.run()
#	return(atoms) # Can be omitted! (if None, treat as in-place function)

def initialize_mcts(nb_steps=10, nb_select=10, kwargs=dict()):
	RS4CEH.DEBUG_MODE = False
	RS4CEH.chemcat.VERBOSE = False

	# Create MCTS instance
#	mcts = MCTS(body_term_order=4, max_length_cutoff=4, max_sites_cutoff=4, nb_pre_exploration_steps=nb_steps, nb_selection=nb_select, exact_no_keep=exact)
	mcts = MCTS(body_term_order=3, max_length_cutoff=3, max_sites_cutoff=3, nb_pre_exploration_steps=nb_steps, nb_selection=nb_select, exact_no_keep=kwargs.get('exact_no_keep', False))
	mcts.min_range = 0.05 # 0.5 # 0.05
	mcts.c = 0 # c
#	mcts.population_bias = False

	# Add config
	for key,value in kwargs.items():
		formatted_value = int(float(value)) if int(float(value)) == float(value) else float(value)
		mcts.__setattr__(key,formatted_value)

	# Define adsorbate binding sites types
	O_site = AbstractBindingSite('O', None, '+', mcts.equivalency_storage)
	O_site.set_equivalent_to(AbstractBindingSite('O', None, '-', mcts.equivalency_storage))
	CO_site = AbstractBindingSite('CO', None, '+', mcts.equivalency_storage)
	CO_site.set_equivalent_to(AbstractBindingSite('CO', None, '-', mcts.equivalency_storage))

	# Define surface sites types
	#fcc_site = AbstractBindingSite('Pd_fcc', 0, '+', mcts.equivalency_storage)
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 120, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 240, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 180, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 60, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 300, '-', mcts.equivalency_storage))
	#hcp_site = AbstractBindingSite('Pd_hcp', 0, '+', mcts.equivalency_storage)
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 120, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 240, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 180, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 60, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 300, '-', mcts.equivalency_storage))
	# Zacros do not consider oriented surface sites
	fcc_site = AbstractBindingSite('Pd_fcc', None, '+', mcts.equivalency_storage)
	fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', None, '-', mcts.equivalency_storage))
	hcp_site = AbstractBindingSite('Pd_hcp', None, '+', mcts.equivalency_storage)
	hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', None, '-', mcts.equivalency_storage))

	# Define surface sites positions and connections (Set arbitrary position to →)
	for y in range(ny):
		for x in range(nx):
			n = unit_cell_numbers[y][x]
			mcts.surface.add_site(2*n, fcc_site)
			mcts.surface.add_site(2*n+1, hcp_site)
	for y in range(ny):
		for x in range(nx):
			# Defines adjacent unit cells
			n = unit_cell_numbers[y][x]
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			n_NW = unit_cell_numbers[(y-1)%ny][(x-1)%nx]
			n_SE = unit_cell_numbers[(y+1)%ny][(x+1)%nx]
			# Add fcc-fcc connections
			mcts.surface.add_connections(2*n, 2*n_E, 1.0, 0)
			mcts.surface.add_connections(2*n, 2*n_N, 1.0, 60)
			mcts.surface.add_connections(2*n, 2*n_NW, 1.0, 120)
			# Add hcp-hcp connections
			mcts.surface.add_connections(2*n+1, 2*n_E+1, 1.0, 0)
			mcts.surface.add_connections(2*n+1, 2*n_N+1, 1.0, 60)
			mcts.surface.add_connections(2*n+1, 2*n_NW+1, 1.0, 120)
			# Add fcc-hcp connections
			mcts.surface.add_connections(2*n, 2*n+1, 0.58, 30)
			mcts.surface.add_connections(2*n, 2*n_W+1, 0.58, 150)
			mcts.surface.add_connections(2*n, 2*n_S+1, 0.58, -90)

	#
	# Setup ASE geometry handler
	#
	mcts.geometries_handler.set_database_filename('geometries.db')
	
	# Create surface geometry (with atom indexes left->right & top->down)
	slab = ase.build.fcc111('Pd', size=(nx,ny,3), vacuum=10)
	slab.positions = np.array(sorted(slab.positions, key=lambda x:(-x[2],-x[1],x[0])))
	lj_geo_opt(slab)
	
	# Create adsorbates geometry
	CO_ads = ase.build.molecule('CO')
	CO_ads.positions = -CO_ads.positions
	lj_geo_opt(CO_ads)
	O_ads = ase.build.molecule('O')
	lj_geo_opt(O_ads)
	
	# Setup calculator
	mcts.geometries_handler.set_calculator(LennardJones())
	mcts.geometries_handler.set_precomputation(lj_geo_opt)
	
	# Compute reference energies
	slab.set_calculator(LennardJones())
	slab_energy = slab.get_potential_energy()
	O_ads.set_calculator(LennardJones())
	O_energy = O_ads.get_potential_energy()
	CO_ads.set_calculator(LennardJones())
	CO_energy = CO_ads.get_potential_energy()
	
	# Store geometries
	mcts.geometries_handler.set_surface(slab, slab_energy)
	mcts.geometries_handler.set_adsorbate('CO', CO_ads, CO_energy)
	mcts.geometries_handler.set_adsorbate('O', O_ads, O_energy)


	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('O',), ('CO',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('Pd',)]:
			node_label_1 = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label_1, new_partition, parent_node=node_label_0)
			for site_type in [('fcc',), ('hcp',)]:
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label_1)
				for site_number in ([(i,) for i in range(2*nx*ny)[::2]] if 'fcc' in site_type else [(i,) for i in range(2*nx*ny)[1::2]]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in [(None,)]: # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	bond_vectors = {'fcc': (1.375,0.794,2), 'hcp': (2.751,1.588,2)}
	for final_partition in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
		ads_type, site_atom_type, site_type, site_number, orientation, chirality = sites_partition_tree.get_node_partition(final_partition).properties
		cell_number = site_number[0]//2
		print(ads_type, site_type, cell_number)
		bond_vector = bond_vectors[site_type[0]]
		sites_partition_tree.add_occupied_site_node(OccupiedSite(cell_number, -1, ads_type[0], 0, -1, -1, bond_vector=bond_vector, theta=None, bond_dihedral=orientation[0], dft_energy=None), final_partition)

	# Define translation + rotation symmetry
	sites_partition_tree.set_symmetry(custom_symmetry_function)

	# Initialize linear model with single body terms
	mcts.initialize_model()

	return(mcts, None)

nb_ads = [int(sys.argv[2])]*int(sys.argv[5]) # [2]*5+[3]*10+[4]*15+[5]*20+[6]*50
nb_steps = nb_select = int(sys.argv[1])
kwargs = dict()
if len(sys.argv) > 6:
#	nb_select = int(sys.argv[6])
#	exact = True
#	c = 0
#else:
#	nb_select = nb_steps
#	exact = False
#	c = 1
	for key, value in zip(sys.argv[6::2],sys.argv[7::2]):
		kwargs[key] = value

def run_RS(nb_ads, mcts, target_model_handler, exploration=50):
	mcts.random = True
	nb_steps = mcts.nb_pre_exploration_steps
	mcts.nb_pre_exploration_steps = mcts.nb_selection
#	mcts.exhaustive_search = True
	# Run RS enabled
	for nb_adsorbates in nb_ads:
		while True:
			new_deviation = mcts.single_run(nb_adsorbates=nb_adsorbates, target_model_handler=target_model_handler)
			if new_deviation:
				break
		
		print(new_deviation, mcts.stats_report())
		
		mcts.exploration_quality += 0
		if mcts.stats_report()['training_set_size'] == exploration:
			mcts.random = False
#			mcts.random_rollout = True
			mcts.nb_pre_exploration_steps = nb_steps
#			mcts.exhaustive_search = False
	return(mcts)

nb_restart = int(sys.argv[4])
exploration = int(sys.argv[3])

mcts_RS = []
for i in range(nb_restart):
	RS4CEH.np.random.seed(i)
	mcts_rs, target_model_handler = initialize_mcts(nb_steps=nb_steps, nb_select=nb_select, kwargs=kwargs)
	mcts_rs = run_RS(nb_ads, mcts_rs, target_model_handler, exploration=exploration)
	mcts_RS.append(trace)
