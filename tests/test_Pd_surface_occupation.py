#!/usr/bin/env python3

import copy
import sys
import itertools
import scipy.stats as stats
import numpy as np
from RS4CEH import MCTS, AbstractBindingSite, Partition, OccupiedSite, AbstractPath, Pattern, DEBUG_MODE, nx
import RS4CEH


def initialize_mcts(nb_steps=1):
	RS4CEH.DEBUG_MODE = False

	# Create MCTS instance
	mcts = MCTS(body_term_order=2, max_length_cutoff=2, max_sites_cutoff=2, nb_pre_exploration_steps=nb_steps)
	#mcts.min_range = 5

	# Define adsorbate binding sites types
	O_site = AbstractBindingSite('O', None, '+', mcts.equivalency_storage)
#	O_site.set_equivalent_to(AbstractBindingSite('O', None, '-', mcts.equivalency_storage))
#	CO_site = AbstractBindingSite('CO', None, '+', mcts.equivalency_storage)
#	CO_site.set_equivalent_to(AbstractBindingSite('CO', None, '-', mcts.equivalency_storage))

	# Define surface sites types
	#fcc_site = AbstractBindingSite('Pd_fcc', 0, '+', mcts.equivalency_storage)
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 120, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 240, '+', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 180, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 60, '-', mcts.equivalency_storage))
	#fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', 300, '-', mcts.equivalency_storage))
	#hcp_site = AbstractBindingSite('Pd_hcp', 0, '+', mcts.equivalency_storage)
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 120, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 240, '+', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 180, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 60, '-', mcts.equivalency_storage))
	#hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', 300, '-', mcts.equivalency_storage))
	# Zacros do not consider oriented surface sites
	fcc_site = AbstractBindingSite('Pd_fcc', None, '+', mcts.equivalency_storage)
#	fcc_site.set_equivalent_to(AbstractBindingSite('Pd_fcc', None, '-', mcts.equivalency_storage))
#	hcp_site = AbstractBindingSite('Pd_hcp', None, '+', mcts.equivalency_storage)
#	hcp_site.set_equivalent_to(AbstractBindingSite('Pd_hcp', None, '-', mcts.equivalency_storage))

	# Define surface sites positions and connections (Set arbitrary position to →)
	nx, ny = 4, 3
	unit_cell_numbers = [[y*nx+x for x in range(nx)] for y in range(ny)]
	for y in range(ny):
		for x in range(nx):
			n = unit_cell_numbers[y][x]
			mcts.surface.add_site(2*n, fcc_site)
			mcts.surface.add_site(2*n+1, fcc_site)
	for y in range(ny):
		for x in range(nx):
			# Defines adjacent unit cells
			n = unit_cell_numbers[y][x]
			n_E = unit_cell_numbers[y][(x+1)%nx]
			n_W = unit_cell_numbers[y][(x-1)%nx]
			n_N = unit_cell_numbers[(y-1)%ny][x]
			n_S = unit_cell_numbers[(y+1)%ny][x]
			n_NW = unit_cell_numbers[(y-1)%ny][(x-1)%nx]
			n_SE = unit_cell_numbers[(y+1)%ny][(x+1)%nx]
			# Add fcc-fcc connections
			mcts.surface.add_connections(2*n, 2*n_E, 1.0, 0)
			mcts.surface.add_connections(2*n, 2*n_N, 1.0, 60)
			mcts.surface.add_connections(2*n, 2*n_NW, 1.0, 120)
			# Add hcp-hcp connections
			mcts.surface.add_connections(2*n+1, 2*n_E+1, 1.0, 0)
			mcts.surface.add_connections(2*n+1, 2*n_N+1, 1.0, 60)
			mcts.surface.add_connections(2*n+1, 2*n_NW+1, 1.0, 120)
			# Add fcc-hcp connections
			mcts.surface.add_connections(2*n, 2*n+1, 0.58, 30)
			mcts.surface.add_connections(2*n, 2*n_W+1, 0.58, 150)
			mcts.surface.add_connections(2*n, 2*n_S+1, 0.58, -90)

	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('O',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('Pd',)]:
			node_label_1 = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label_1, new_partition, parent_node=node_label_0)
			for site_type in [('fcc',)]:
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label_1)
				for site_number in ([(i,) for i in range(2*nx*ny)[:]]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in [(None,)]: # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	dft_single_body = {(('O',), ('fcc',)): -1.0932}
	for final_partition in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
		ads_type, site_atom_type, site_type, site_number, orientation, chirality = sites_partition_tree.get_node_partition(final_partition).properties
		dft_energy = dft_single_body[(ads_type, site_type)]
		print(ads_type, site_type, site_number, dft_energy)
		sites_partition_tree.add_occupied_site_node(OccupiedSite(site_number[0], 1, ads_type, theta=180, bond_dihedral=orientation, dft_energy=dft_energy), final_partition)

	# Define translation + rotation symmetry
	def custom_symmetry_function(configuration_object):
		# Define configuration data
		partition_tree = configuration_object.partition_tree
		full_partition_tree = configuration_object.sites_partition_tree
		cur_node = configuration_object.current_node
		occupied_sites = configuration_object.occupied_sites
		tree_root = configuration_object.partition_tree_object.tree_root
		site_number_root = configuration_object.partition_tree_object.site_number_root
	
		# All unit cells are equivalent for first adsorbate
		if cur_node == tree_root and len(occupied_sites) == 0:
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep first unit cell (i.e. site_number = (0,) or (1,))
				if configuration_object.get_node_partition(site_number_node).site_number[0] > 1:
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
	
		# Only some cells are not equivalent for second adsorbate
		if cur_node == tree_root and len(occupied_sites) == 1:
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep cells that equal their minimum image convention with respect first cell
				if ((configuration_object.get_node_partition(site_number_node).site_number[0]//2) % nx > nx//2 or
				    (configuration_object.get_node_partition(site_number_node).site_number[0]//2) // nx > ny//2):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
		
		# Prevent close fcc-hcp clashes
		if cur_node == tree_root and len(occupied_sites) >= 1:
			# Retrieve already occupied site numbers
			site_numbers = iter(full_partition_tree.get_site_partition(site).site_number for site in occupied_sites)
			occupied_site_numbers = set(itertools.chain.from_iterable(site_numbers))
			
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep sites that not close from occupied site
				site_number = configuration_object.get_node_partition(site_number_node).site_number[0] # Assume unidentate
				cell_number = site_number//2
				x, y = cell_number%nx, cell_number//nx
				n_E = unit_cell_numbers[y][(x+1)%nx]
				n_W = unit_cell_numbers[y][(x-1)%nx]
				n_N = unit_cell_numbers[(y-1)%ny][x]
				n_S = unit_cell_numbers[(y+1)%ny][x]
				# Check if fcc site
				if site_number%2 == 0:
					if (site_number+1 in occupied_site_numbers or
					    2*n_W+1 in occupied_site_numbers or
					    2*n_S+1 in occupied_site_numbers):
						# Delete subtree
						configuration_object.delete_partition_subtree(site_number_node)
				else:
					if (site_number-1 in occupied_site_numbers or
					    2*n_E in occupied_site_numbers or
					    2*n_N in occupied_site_numbers):
						# Delete subtree
						configuration_object.delete_partition_subtree(site_number_node)
	sites_partition_tree.set_symmetry(custom_symmetry_function)

	# Initialize linear model with single body terms
	mcts.initialize_model()

	#
	# Create target model Hamiltonian
	#
	target_model_handler = copy.deepcopy(mcts.model_handler)

	# Add all O fcc-fcc lateral interaction terms
	path = AbstractPath.from_surface_path([2*unit_cell_numbers[0][0], 
		                               2*unit_cell_numbers[0][1]], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (O_site, O_site), (path,)).get_index()
	print('O_pair_fcc_1NN', index_target)
	target_model_handler.add_config_to_model({index_target: 1}, 0.2288)
	
	return(mcts, target_model_handler)

def custom_single_run(mcts, nb_adsorbates=None, target_model_handler=None):
	# Store desired number of adsorbates
	assert(mcts.nb_adsorbates or nb_adsorbates)
	if nb_adsorbates:
		mcts.nb_adsorbates = nb_adsorbates
	
	# Pre-exploration step
	pre_explored_nodes = mcts.pre_exploration(nb_steps=mcts.nb_pre_exploration_steps)
	if not pre_explored_nodes:
		return(None)
	best_node = pre_explored_nodes[0][0]
	
	# Compute energy + deviation
	real_deviation = 1
	
	# Backpropagation
	mcts.tree.nodes[best_node]['storage'].add_deviation(real_deviation, nb_adsorbates=mcts.nb_adsorbates, set_node_deviation=True, exploration_penality=mcts.exploration_quality)
	for node in nx.ancestors(mcts.tree, best_node):
		mcts.tree.nodes[node]['storage'].add_deviation(real_deviation, nb_adsorbates=mcts.nb_adsorbates, exploration_penality=mcts.exploration_quality)
	mcts.set_unsearchable(best_node)

nb_ads = int(sys.argv[1])

RS4CEH.np.random.seed(0)
mcts_rs, target_model_handler = initialize_mcts()
mcts_rs.random = True
i = 0
while not mcts_rs.tree.nodes['root']['unsearchable']:
	custom_single_run(mcts_rs, nb_adsorbates=nb_ads, target_model_handler=target_model_handler)
	if not i % 100:
		print(i)
	i+=1

print(mcts_rs.tree.nodes['root']['storage'].nb_deviation, i)

# 4: 8564
# 5: 28800
# 6: 103207
# 7: 180677
# 8: 375136
# 9: 192626
# 10: 
