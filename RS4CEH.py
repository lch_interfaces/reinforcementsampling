#!/usr/bin/env python3

# Import librairies
import numpy as np
import networkx as nx
#import scipy.linalg as lg
import scipy.optimize as scopt
from scipy.special import binom
import math
import itertools
import warnings
from abc import ABCMeta, abstractmethod
import copy
#import random as rd
from rank_greville import RecursiveModelOrthogonal
import ase
import ase.db
import chemcat
import pickle
from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import mean_squared_error

DEBUG_MODE = True

#
# Geometries handler
#
class GeometriesHandler(object):
	"""Handler for geometries (structure input-output, calculations, ...)"""
	
	def __init__(self, database_filename=None):
		self.surface = dict()
		self.adsorbates = dict()
		self.database_filename = database_filename
		self.calculator = None
		self.precomputation = None
	
	def set_surface(self, surface_geometry, surface_energy, **read_options):
		"""Add geometry + energy for the surface
		
		Parameters:
			surface_geometry (ase.Atoms or string): Geometry of the surface (if a string, it is treated as a filename and read with ASE)
			
			surface_energy (float): Energy of the surface taken alone (i.e. reference energy for adsorption energy calculation)
			
			read_options (optional): Options for ase.io.read to read the geometry file.
		"""
		# Store energy
		self.surface['energy'] = surface_energy
		
		# Store geometry
		self.surface['geometry'] = self._read_geometry(surface_geometry, **read_options)
	
	def set_adsorbate(self, adsorbate_label, adsorbate_geometry, adsorbate_energy, **read_options):
		"""Set an adsorbate geometry + energy
		
		Parameters:
			adsorbate_label (str): Label of the adsorbate (as used in `OccupiedSite` objects)
			
			adsorbate_geometry (ase.Atoms or str): Geometry of the adsorbate (if a string, it is treated as a filename and read with ASE)
			
			read_options (optional): Options for ase.io.read to read the geometry file.
		"""
		# Reset adsorbate
		self.adsorbates[adsorbate_label] = dict()
		
		# Store energy
		self.adsorbates[adsorbate_label]['energy'] = adsorbate_energy
		
		# Store geometry
		self.adsorbates[adsorbate_label]['geometry'] = self._read_geometry(adsorbate_geometry, **read_options)
	
	def set_calculator(self, calculator):
		self.calculator = calculator
	
	def set_precomputation(self, precomputation_function):
		self.precomputation = precomputation_function
	
	def set_database_filename(self, database_filename):
		self.database_filename = database_filename
	
	def build_occupied_geometry(self, occupied_sites):
		# Initialize energy sum of isolated fragments
		surface = self.surface['geometry']
		sum_energy = self.surface['energy']
		
		# Loop over all occupied sites (each representing an adsorbate)
		adsorbates = []
		for occupied_site in occupied_sites:
			# Add energy alone to energy sum
			sum_energy += self.adsorbates[occupied_site.adsorbate_type]['energy']
			
			# Position related adsorbate accordingly
			adsorbate = self.adsorbates[occupied_site.adsorbate_type]['geometry'].copy()
			chemcat.transform_adsorbate(adsorbate,
			                            surface,
			                            atom1_mol=adsorbate[occupied_site.adsorbate_ref_atom_1],
			                            atom2_mol=adsorbate[occupied_site.adsorbate_ref_atom_2],
			                            atom3_mol=adsorbate[occupied_site.adsorbate_ref_atom_3],
			                            atom1_surf=surface[occupied_site.site_ref_atom_1],
			                            atom2_surf=surface[occupied_site.site_ref_atom_2],
			                            bond_vector=occupied_site.bond_vector,
			                            bond_angle_target=occupied_site.theta,
			                            dihedral_angle_target=occupied_site.bond_dihedral,
			                            mol_dihedral_angle_target=occupied_site.mol_dihedral)
			adsorbates.append(adsorbate)
		
		# Build occupied surface (i.e. merge all adsorbates + surface)
		occupied_surface = sum(adsorbates, surface)
		
		return(occupied_surface, sum_energy)
	
	def compute_adsorption_energy(self, occupied_surface, sum_energy):
		# Apply precomputation if defined (geometry optimization, ...)
		if self.precomputation:
			result = self.precomputation(occupied_surface)
			if result:
				occupied_surface = result
		
		# Run energy calculation
		if self.calculator is not None:
			occupied_surface.set_calculator(self.calculator)
			potential_energy = occupied_surface.get_potential_energy()
			adsorption_energy = potential_energy - sum_energy
		else:
			# Ask for manual input
			print("No calculator set, please perform manually an energy run for the selected occupied surface")
			geometry_filename = input("Filename for writing selected geometry: ")
			print("Writing structure")
			occupied_surface.write(geometry_filename)
			print("Structure successfully written as {}".format(geometry_filename))
			adsorption_energy = float(input('Enter associated adsorption energy: '))
		
		return(adsorption_energy)
	
	def write_to_database(self, mcts_object, occupied_surface='', adsorption_energy=0, occupied_sites=None, database_filename=None):
		# Retrieve filename of database
		if database_filename is None:
			database_filename = self.database_filename
		
		# Do nothing if no database filename is set
		if not database_filename:
			return(None)
		
		# Create associated config for archive
		config = Configuration(None, with_partition_tree=False)
		if occupied_sites:
			config.occupied_sites = occupied_sites
		config_pkl = pickle.dumps(config, protocol=0).decode()
		
		# Archive data
		database = ase.db.connect(database_filename)
		database.write(occupied_surface,
		               adsorption_energy=adsorption_energy,
		               config=config_pkl,
		               **self.convert_archivable_mcts(mcts_object))
	
	def query_database_configs(self, database_filename=None, query='', with_energy=False):
		# Retrieve filename of database
		if database_filename is None:
			database_filename = self.database_filename
		
		# Run query
		database = ase.db.connect(database_filename)
		rows = tuple(database.select(query))
		
		# Extract associated configurations
		configs = [pickle.loads(row.config.encode()) for row in rows]
		if not with_energy:
			return(configs)
		
		# Extract associated adsorption energy
		adsorption_energies = [row.adsorption_energy for row in rows]
		return(configs, adsorption_energies)
	
	def _read_geometry(self, geometry, **read_options):
		# Use ASE to read geometry file if filename is given
		if isinstance(geometry, str):
			return(ase.io.read(geometry, **read_options))
		
		# By default, assume geometry has compatible type
		return(geometry)
	
	@staticmethod
	def convert_archivable_mcts(mcts_object):
		# Create dict of MCTS attributes
		mcts_attributes = dict()
		
		# Populate attributes
		for key, value in mcts_object.__dict__.items():
			# Convert value to archivable type, if required
			try:
				ase.db.core.check({key: value})
			except ValueError:
				value = str(value)
			
			# Add attribute
			mcts_attributes[key] = value
		
		# Add number of training-set material configurations in MCTS tree (i.e. nb selected so far)
		mcts_attributes['training_set_config_index'] = mcts_object.tree.nodes['root']['storage'].nb_deviation
		
		return(mcts_attributes)

#
# Configuration partition
#
class OccupiedSite(object):
	"""Occupied site object containing all parameters required for absorbing a given adsorbate at a given site"""
	
	def __init__(self, site_ref_atom_1, site_ref_atom_2, adsorbate_type, adsorbate_ref_atom_1=0, adsorbate_ref_atom_2=1, adsorbate_ref_atom_3=2, bond_vector=[0,0,2], theta=120, bond_dihedral=0, dihedral=0, dft_energy=0):
		"""Create occupied site
		
		Parameters:
			self (OccupiedSite object): object to create
			
			site_ref_atom_1 (int): index of surface atom to bond the adsorbate onto
			
			site_ref_atom_2 (int): index of surface atom to reference for defining dihedral angle (see `dihedral`)
			
			adsorbate_type (string): type of the adsorbate
			
			adsorbate_ref_atom_1 (int): index of adsorbate atom to bond the surface
			
			adsorbate_ref_atom_2 (int): index of surface atom to reference for defining theta and dihedral angle (see `theta` and `bond_dihedral`)
			
			adsorbate_ref_atom_3 (int): index of surface atom to reference for defining new dihedral angle (see `dihedral`)
			
			bond_vector (size 3 array): vector setting position of `adsorbate_ref_atom_1` with respect to `site_ref_atom_1`
				Default: [0, 0, 2] (i.e. 2 angstrom above (Z direction) surface atom indexed by `site_ref_atom_1`)
			
			theta (float): angle between (`site_ref_atom_1`, `adsorbate_ref_atom_1`, `adsorbate_ref_atom_2`)
				Default: 120
			
			bond_dihedral (float): dihedral angle between (`site_ref_atom_1`, `site_ref_atom_2`, `adsorbate_ref_atom_1`, `adsorbate_ref_atom_2`)
			
			mol_dihedral (float): dihedral angle between (`site_ref_atom_2`, `adsorbate_ref_atom_1`, `adsorbate_ref_atom_2`, `adsorbate_ref_atom_3`)
			
			dft_energy (float, negative): DFT adsorption energy of occupied site alone
		"""
		self.site_ref_atom_1 = site_ref_atom_1
		self.site_ref_atom_2 = site_ref_atom_2
		self.adsorbate_type = adsorbate_type
		self.adsorbate_ref_atom_1 = adsorbate_ref_atom_1
		self.adsorbate_ref_atom_2 = adsorbate_ref_atom_2
		self.adsorbate_ref_atom_3 = adsorbate_ref_atom_3
		self.bond_vector = bond_vector
		self.theta = theta
		self.bond_dihedral = bond_dihedral
		self.mol_dihedral = dihedral
		self.dft_energy = dft_energy
	
	@property
	def properties(self):
		return((self.site_ref_atom_1,
		        self.site_ref_atom_2,
		        self.adsorbate_type,
		        self.adsorbate_ref_atom_1,
		        self.adsorbate_ref_atom_2,
		        self.adsorbate_ref_atom_3,
		        self.bond_vector,
		        self.theta,
		        self.bond_dihedral,
		        self.mol_dihedral))
	
	def __repr__(self):
		return('{}_{}({},{})@{}({})[{},{},{},{}]'.format(self.adsorbate_type,
		                                           self.adsorbate_ref_atom_1,
		                                           self.adsorbate_ref_atom_2,
		                                           self.adsorbate_ref_atom_3,
		                                           self.site_ref_atom_1,
		                                           self.site_ref_atom_2,
		                                           self.bond_vector,
		                                           self.theta,
		                                           self.bond_dihedral,
		                                           self.mol_dihedral))
	
	def __lt__(self, other):
		return(self.__repr__() < other.__repr__())

class Partition(object):
	"""Partition object"""
	properties_names = ('ads_type', 'site_atom_type', 'site_type', 'site_number', 'orientation', 'chirality')
	
	def __init__(self, ads_type=None, site_atom_type=None, site_type=None, site_number=None, orientation=None, chirality=None):
		# Store partition properties
		self.ads_type = ads_type
		self.site_atom_type = site_atom_type
		self.site_type = site_type
		self.site_number = site_number
		self.orientation = orientation
		self.chirality = chirality
		self._properties = None
	
	@property
	def properties(self):
		# Compute and store value if not already computed
		if self._properties is None:
			self._properties = (self.ads_type, self.site_atom_type, self.site_type, self.site_number, self.orientation, self.chirality)
		
		return(self._properties)
	
	def get_new_properties(self, other):
		"""Return names of all properties added in this partition, compared to another"""
		# Get properties
		own_properties = self.properties
		if other is None:
			other_properties = (None,)*len(own_properties)
		else:
			other_properties = other.properties
		
		# Check properties are not removed, and common properties are equal
		assert(all([own_property == other_property for own_property, other_property in zip(own_properties, other_properties) if other_property is not None]))
		
		# Return new properties names
		return(property_name for property_name, own_property, other_property in zip(self.properties_names, own_properties, other_properties) if other_property is None and own_property is not None)
	
	def __lt__(self, other):
		# Depth-two lexicographic order on properties
		for own_prop, other_prop in zip(self.properties, other.properties):
			if own_prop != other_prop:
				if None in (own_prop, other_prop):
					# None is larger than any property
					return(other_prop is None)
				# Compare tuple-based property values (for multidentate support)
				for own_prop_item, other_prop_item in zip(own_prop, other_prop):
					if own_prop_item != other_prop_item:
						try:
							return(own_prop_item < other_prop_item)
						except TypeError:
							# None is larger than any property
							return(other_prop_item is None)
				# With identical prefixes, shorter tuples are considered lower
				own_prop_length, other_prop_length = len(own_prop), len(other_prop)
				if own_prop_length != other_prop_length:
					return(own_prop_length < other_prop_length)
		return(False)
	
	def __eq__(self, other):
		return(self.properties == other.properties)

class SitesPartitionTree(object):
	def __init__(self):
		# Initialize common partition tree
		self.tree = nx.DiGraph()
		self.nb_leaves = 0
		
		# Define types names
		self.roots_type = 'roots'
		self.partition_type = 'partition'
		self.occupied_site_type = 'occupied_site'
		
		# Define roots shortcuts labels
		self.tree_root = 'root'
		self.final_partition_root = 'final_partition'
		self.occupied_site_root = 'occupied_site'
		self.ads_type_root = 'ads_type'
		self.site_atom_type_root = 'site_atom_type'
		self.site_type_root = 'site_type'
		self.site_number_root = 'site_number'
		self.orientation_root = 'orientation'
		self.chirality_root = 'chirality'
		self.suitable_shortcut = {'ads_type':self.ads_type_root, 'site_atom_type':self.site_atom_type_root, 'site_type':self.site_type_root, 'site_number':self.site_number_root, 'orientation':self.orientation_root, 'chirality':self.chirality_root}
		
		# Define roots shortcuts nodes (parents of shallowest nodes fixing given property)
		self.tree.add_node(self.tree_root, type=self.roots_type, nb_lexicographic=dict())
		self.tree.add_node(self.ads_type_root, type=self.roots_type)
		self.tree.add_node(self.site_atom_type_root, type=self.roots_type)
		self.tree.add_node(self.site_type_root, type=self.roots_type)
		self.tree.add_node(self.site_number_root, type=self.roots_type)
		self.tree.add_node(self.orientation_root, type=self.roots_type)
		self.tree.add_node(self.chirality_root, type=self.roots_type)
		self.tree.add_node(self.final_partition_root, type=self.roots_type)
	
	def add_partition_node(self, partition_node, partition, parent_node=None):
		# Default parent node is root
		if parent_node is None:
			parent_node = self.tree_root
		
		# Create partition node
		self.tree.add_node(partition_node, type=self.partition_type, partition=partition, nb_lexicographic={'original': True})
		self.tree.add_edge(parent_node, partition_node)
		
		# Retrieve new properties introduced, with respect with parent
		try:
			parent_partition = self.tree.nodes[parent_node]['partition']
		except KeyError:
			parent_partition = None
		new_properties_names = partition.get_new_properties(parent_partition)
		
		# Add corresponding shortcuts links
		for new_property_name in new_properties_names:
			# Retrieve corresponding shortcut node
			shortcut_root_node = self.suitable_shortcut[new_property_name]
			# Add shortcut link
			self.tree.add_edge(shortcut_root_node, partition_node)
		
		# Add shortcut link if complete partition
		if None not in partition.properties:
			self.tree.add_edge(self.final_partition_root, partition_node)
	
	def add_occupied_site_node(self, occupied_site, parent_partition_node):
		# Check that parent partition is complete
		assert(self.final_partition_root in self.tree.predecessors(parent_partition_node))
		
		# Add occupied site node
		self.tree.add_node(occupied_site, type=self.occupied_site_type)
		self.tree.add_edge(parent_partition_node, occupied_site)
		self.tree.add_edge(self.occupied_site_root, occupied_site)
		
		# Update number of leaves
		self.nb_leaves += 1
	
	def get_site_partition(self, site):
		"""Get final partition info (partition node dict) of an occupied site"""
		# Retrieve parent nodes of occupied site in partition tree
		parent_nodes = self.tree.predecessors(site)
		
		# Select partition node with maximum info only
		parent_nodes = tuple(node for node in parent_nodes if self.final_partition_root in self.tree.predecessors(node))
		assert(len(parent_nodes) == 1)
		
		return(self.tree.nodes[parent_nodes[0]]['partition'])
	
	def set_symmetry(self, symmetry_function):
		"""Define custom symmetry function"""
		self.apply_symmetries = symmetry_function
	
	@staticmethod
	def apply_symmetries(configuration_object):
		"""Define tree deletions/operations applying symmetries"""
		warnings.warn('No symmetries defined, use `SitesPartitionTree().set_symmetry` to define symmetries')
		return(None)
	
	def update_lexicographic_descendants(self, maximum_exploration_depth):
		"""Compute and update lexicographically ordered number of descendants for up to a given exploration depth
		
		Computational details:
			A strict lexicographic order filtering is used for the exploration, leading to branches being effectively unbalanced (only lex ordered sequences of final partitions are considered). This function is computing the number of acceptable descendants (lexicographically ordered sequences) of all nodes, up to given exploration depth.
		"""
		
		# Sort all final partitions nodes, in decreasing order, with respect to Partition order
		sorted_final_partitions_nodes = [self.tree.nodes[node] for node in self.tree.successors(self.final_partition_root)]
		sorted_final_partitions_nodes.sort(key=lambda node:node['partition'], reverse=True)
		
		# Sort all partition node labels in reverse topological order
		reverse_topo_ordered_node_labels = tuple(nx.dfs_postorder_nodes(self.tree, self.tree_root))
		
		# Compute and update all numbers of lexicographic filtered descendants up to given maximum depth (i.e. number of ordered sequences of size up to maximum depth)
		for depth in range(maximum_exploration_depth):
			# Check if data at current depth was already computed
			if all(depth in node['nb_lexicographic'] for node in sorted_final_partitions_nodes):
				continue
			
			# Compute and store number of lexicographic filtered descendants (final partitions nodes type) of final partition nodes, up to exploration depth
			# Assuming N possible values and a k-length sequence, the number of strictly ordered sequences of size k is binomial(N, k)
			for index, node in enumerate(sorted_final_partitions_nodes):
				# Number of sequences with size up to depth = number of sequences of size depth + number of sequences with size up to depth-1
				node['nb_lexicographic'][depth] = binom(index, depth) + node['nb_lexicographic'].get(depth-1, 0)
			
			# Update ancestors nodes, in reverse topological sort order to ensure descendants have been treated before
			for node_label in reverse_topo_ordered_node_labels:
				node = self.tree.nodes[node_label]
				
				# Retrieve children
				children = tuple(self.tree.nodes[child] for child in self.tree.successors(node_label))
				
				# Check that node and its children all have lexicographic storage
				if 'nb_lexicographic' not in node:
					continue
				if not children or any('nb_lexicographic' not in child for child in children):
					continue
				
				# Update number of ordered sequences from partition node from its successors
				node['nb_lexicographic'][depth] = sum(child['nb_lexicographic'][depth] for child in children)
	
	def get_max_child_descendants(self, parent_partition_node_label, exploration_depth):
		"""Compute the maximum number of descendants among the children of the node."""
		
		# Get statistics on the number of descendants of children partitions
		depth_remain = exploration_depth-1
		max_child_descendants = 0
		for child_label in self.tree.successors(parent_partition_node_label):
			child_node = self.tree.nodes[child_label]
			if 'nb_lexicographic' in child_node:
				max_child_descendants = max(max_child_descendants, child_node['nb_lexicographic'][depth_remain])
		
		# Return the maximum number of descendants among children
		return(max_child_descendants)
	
	def get_bias_ratio(self, partition_node_label, exploration_depth, max_sibling_descendants):
		"""Compute bias ratio to apply for cancelling population bias between branches of a same parent.
		
		Computational details:
			Due to 1-body uneven repartition and lexicographically order filtering, branches of a same parent can display large population unbalance. This unbalance could be taken into account when considering how much a branch was explored. A simple way to do so is to rescale the real number of explorations by the ratio : maximum_number_of_descendants / number_of_descendants. The intuitive idea is to give more weight to exploration on branches leading to fewer nodes, having a uniform effective 'exploration ratio' of unbalanced branches.
		"""
		
		# Get self partition exploration statistics
		partition_node = self.tree.nodes[partition_node_label]
		depth_remain = exploration_depth-1
		nb_self_descendants = partition_node['nb_lexicographic'][depth_remain]
		
		# Return population bias ratio to apply for correcting population unbalance
		return(max_sibling_descendants/nb_self_descendants)
	
	def get_node_partition(self, node_label):
		# Get node
		node = self.tree.nodes[node_label]
		
		# Retrieve partition, or return empty partition
		if not 'partition' in node:
			return(Partition())
		else:
			return(node['partition'])
	
	def copy(self):
		new_sites_partition_tree = copy.copy(self)
		new_sites_partition_tree.suitable_shortcut = copy.deepcopy(self.suitable_shortcut)
		new_sites_partition_tree.tree = self.tree.copy()
#		new_tree = new_sites_partition_tree.tree
#		for node in new_tree:
#			if 'nb_lexicographic' in new_tree.nodes[node]:
#				new_tree.nodes[node]['nb_lexicographic'] = copy.deepcopy(new_tree.nodes[node]['nb_lexicographic'])
		return(new_sites_partition_tree)
	
	@staticmethod
	def get_lexicographic_for_update(node):
		nb_lexicographic = node['nb_lexicographic']
		if not nb_lexicographic.get('original', False):
			return(nb_lexicographic)
		
		# On-the-fly copy of original before modification
		nb_lexicographic = copy.deepcopy(nb_lexicographic)
		nb_lexicographic.pop('original')
		node['nb_lexicographic'] = nb_lexicographic
		
		return(nb_lexicographic)

class Configuration(object):
	"""Molecular configuration object"""
	
	def __init__(self, sites_partition_tree, with_partition_tree=True):
		self.sites_partition_tree = sites_partition_tree
		self.partition_tree_object = None
		self.partition_tree = None
		self.current_node = None
		self.occupied_sites = []
		
		# Create partition tree if requested (default)
		if with_partition_tree:
			# Initialize partition tree
			self.partition_tree_object = sites_partition_tree.copy()
			self.partition_tree = self.partition_tree_object.tree
			self.current_node = self.partition_tree_object.tree_root
			
			# Apply symmetries
			self.partition_tree_object.apply_symmetries(self)
	
	def get_choices(self):
		"""Retrieve current choices available"""
		# Check that current node was not deleted during symmetry application
		if self.current_node not in self.partition_tree:
			return([])
		
		# Get list of next available partition labels
		return(list(self.partition_tree.successors(self.current_node)))
	
	def choose_partition(self, partition_node):
		"""Advance in partition tree by choosing a partition and apply symmetry filtering"""
		# Check if reaching final partition
		if self.partition_tree_object.final_partition_root in self.partition_tree.predecessors(partition_node):
			# Add new occupied site
			self.occupied_sites.append(tuple(self.partition_tree.successors(partition_node))[0])
			# Reset partition tree
			self.reset_partition_tree()
		else:
			# Advance in partition tree
			self.current_node = partition_node
		
		# Apply symmetries
		self.partition_tree_object.apply_symmetries(self)
	
	def reset_partition_tree(self):
		"""Reset partition tree, excluding sites already filled, and ensuring increasing occupied site partition properties (lexicographic order)"""
		# Reset partition tree and current node
		self.partition_tree_object = self.sites_partition_tree.copy()
		self.partition_tree = self.partition_tree_object.tree
		self.current_node = self.partition_tree_object.tree_root
		
		# Retrieve already occupied sites numbers
		sites_numbers = iter(self.sites_partition_tree.get_site_partition(site).site_number for site in self.occupied_sites)
		occupied_sites_numbers = set(itertools.chain.from_iterable(sites_numbers))
		
		# Iterate over all site number partition nodes
		for site_number_node in tuple(self.partition_tree.successors(self.partition_tree_object.site_number_root)):
			# Retrieve associated partition
			site_number_node_partition = self.get_node_partition(site_number_node)
			# Remove partitions using site numbers already occupied
			if occupied_sites_numbers.intersection(site_number_node_partition.site_number):
				# Delete subtree
				self.delete_partition_subtree(site_number_node)
		
		# Retrieve partition of last occupied site
		last_occupied_site_partition = self.sites_partition_tree.get_site_partition(self.occupied_sites[-1])
		
		# Iterate over all partition nodes in BFS order (assuming python>=3.6)
		for node in nx.traversal.bfs_tree(self.partition_tree, self.partition_tree_object.tree_root):
			# Check node was not already deleted
			if node not in self.partition_tree:
				continue
			# Retrieve associated partition
			try:
				node_partition = self.get_node_partition(node)
			except KeyError:
				continue
			# Remove smaller partitions (with lexicographic order defined)
			if node_partition < last_occupied_site_partition:
				# Delete subtree
				self.delete_partition_subtree(node)
	
	def delete_partition_subtree(self, node):
		"""Easy deletion of partition subtree and pruning of above empty branches"""
		# Retrieve branches above, excluding initial node
		branches_above = tuple(nx.dfs_preorder_nodes(nx.reverse_view(self.partition_tree), node))[1:]
		
		# Retrieve lexicographic descendants data
		try:
			nb_lexicographic_node = self.partition_tree.nodes[node]['nb_lexicographic']
		except IndexError:
			nb_lexicographic_node = dict()
		
		# Delete subtree below
		subtree = nx.descendants(self.partition_tree, node)
		self.partition_tree.remove_nodes_from(subtree)
		
		# Delete node
		self.partition_tree.remove_node(node)
		
		# Delete empty nodes in branch above
		for ancestor_node_label in branches_above:
			ancestor_node = self.partition_tree.nodes[ancestor_node_label]
			
			# Remove now empty ancestors above but keep shortcuts roots
			if self.partition_tree.out_degree(ancestor_node_label) == 0 and ancestor_node['type'] == self.partition_tree_object.partition_type:
				self.partition_tree.remove_node(ancestor_node_label)
				continue
			
			# Update lexicographic descendants data
			if 'nb_lexicographic' in ancestor_node:
				nb_lexicographic_ancestor = self.partition_tree_object.get_lexicographic_for_update(ancestor_node)
				for depth in nb_lexicographic_ancestor:
					nb_lexicographic_ancestor[depth] -= nb_lexicographic_node.get(depth, 0)
	
	def get_node_partition(self, node):
		return(self.partition_tree_object.get_node_partition(node))

#
# Abstract pattern manipulation
#
def angles_diff(angle1, angle2, mirrored=False):
		"""Computes uniquely defined difference between two angles (in degrees)"""
		# Treat case where angles are not both defined
		if angle1 is None or angle2 is None:
			return(None)
		
		# Compute raw relative angle
		diff_angle = angle2 - angle1
		
		# Apply mirror state
		if mirrored:
			diff_angle = -diff_angle
		
		# Compute relative angle between [-180, 180)
		diff_angle = (diff_angle + 180)%360 - 180
		
		# Round difference, since modulo introduces numerical entropy
		diff_angle = round(diff_angle, 2)
		
		return(diff_angle)

class AbstractConnection(metaclass=ABCMeta):
	@abstractmethod
	def get_outermost_orientation(self, in_angle=True, reverse_format=False):
		pass
	
	@staticmethod
	def _get_connection_connection_angles(abstract_connections, reverse_format=False, mirror_format=False):
		# Define read order
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		# Compute in_connection to out_connection angles
		connection_connection_angles = []
		for in_connection, out_connection in nx.utils.misc.pairwise(abstract_connections[slicing]):
			# Retrieve absolute orientations
			in_connection_orientation = in_connection.get_outermost_orientation(in_angle=True, reverse_format=reverse_format)
			out_connection_orientation = out_connection.get_outermost_orientation(in_angle=False, reverse_format=reverse_format)
			
			# Compute and store relative angle
			in_out_angle = angles_diff(in_connection_orientation, out_connection_orientation, mirrored=mirror_format)
			connection_connection_angles.append(in_out_angle)
		
		# Return angles
		return(connection_connection_angles)
	
	@staticmethod
	def _get_connection_sites_angles(extended_abstract_connections, abstract_sites_sequence, reverse_format=False, mirror_format=False, reverse_extended=False):
		# Define read order
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		# Do not use default direction (in) for extended connections, if inversion requested
		default_direction = not reverse_extended
		
		# Compute in_connection to sites angles
		connection_sites_angles = []
		for in_connection, sites in zip(extended_abstract_connections[slicing][:-1], abstract_sites_sequence[slicing]):
			# Retrieve absolute orientation of in_connection
			in_connection_orientation = in_connection.get_outermost_orientation(in_angle=default_direction, reverse_format=reverse_format)
			sites_orientations = (site.orientation for site in sites)
			
			# Compute and store relative angles
			in_sites_angles = (angles_diff(in_connection_orientation, site_orientation, mirrored=mirror_format) for site_orientation in sites_orientations)
			connection_sites_angles.append(tuple(in_sites_angles))
			
			# Use default (in) direction after extended connection was treated (last extended connection is never treated)
			default_direction = True
		
		# Return angles
		return(connection_sites_angles)

class EquivalencyStorage(object):
	def __init__(self):
		# Abstract binding sites equivalency storage
		self.equivalent_sites = dict()
		
		# Patterns equivalency storage
		self.nb_patterns_indexes = 0
		self.nb_redundant_indexes = 0
		self.pattern_index = dict()
		self.internal_multiplicities = []
		self.user_defined_multiplicities = []
		self.pattern_mergers = dict()
#		self.pattern_constructors = dict() # debug patterns generation
	
	def get_new_pattern_index(self, multiplicity=1):
		# Get new unused pattern index
		new_index = self.nb_patterns_indexes
		
		# Set new multiplicity
		assert(len(self.user_defined_multiplicities) == self.nb_patterns_indexes)
		self.user_defined_multiplicities.append(multiplicity)
		self.internal_multiplicities.append(1)
		
		# Update pattern indexes count
		self.nb_patterns_indexes += 1
		
		return(new_index)
	
	def merge_indexes(self, index1, index2):
		assert (index1 < self.nb_patterns_indexes and index2 < self.nb_patterns_indexes), "Both indexes must correspond to already seen patterns"
		
		if self.get_representant(index1) == self.get_representant(index2):
			return()
		
		# Retrieve equivalent indexes
		index1_equivalents = self.pattern_mergers.get(index1, set((index1,)))
		index2_equivalents = self.pattern_mergers.get(index2, set((index2,)))
		combined_equivalents = index1_equivalents.union(index2_equivalents)
		
		# Update equivalents (merges)
		for index in combined_equivalents:
			self.pattern_mergers[index] = combined_equivalents
		
		self.nb_redundant_indexes += 1
		
		return(self.get_representant(index1))
	
	def set_pattern_multiplicity(self, original_index, multiplicity=1, automorphism=False):
		if automorphism:
			self.internal_multiplicities[original_index] = multiplicity
		else:
			self.user_defined_multiplicities[original_index] = multiplicity
	
	def get_patterns_from_index(self, index_target, original=False):
		return(tuple(pattern for pattern, index in self.pattern_index.items() if index_target == (index if original else self.get_representant(index))))
	
	def get_index(self, pattern, original=False):
		original_index = self.pattern_index[pattern]
		
		if original:
			return(original_index)
		
		return(self.get_representant(original_index))
	
	def get_representant(self, index):
		if index not in self.pattern_mergers:
			return(index)
		
		return(min(self.pattern_mergers[index]))
	
	def get_multiplicity(self, pattern):
		# Get all equivalent pattern indexes
		pattern_index = pattern.get_index(original=True)
		equivalent_indexes = self.pattern_mergers.get(pattern_index, (pattern_index,))
		
		# Compute total multiplicity
		total_multiplicity = 0
		for index in equivalent_indexes:
			total_multiplicity += self.internal_multiplicities[index] * self.user_defined_multiplicities[index]
		
		return(total_multiplicity)
	
	@property
	def nb_distinct_patterns(self):
		return(self.nb_patterns_indexes - self.nb_redundant_indexes)

class AbstractBindingSite(object):
	"""Abstract representation of a binding site (on surface or adsorbate)"""
	
	def __init__(self, chemical_type, orientation, chirality, equivalency_storage):
		"""Create an abstract binding site
		
		Parameters:
			self (AbstractBindingSite): abstract binding site to create
			
			chemical_type (string): chemical type of site (e.g. Pt_top, en_N1, ...)
			
			orientation (float): orientation in degrees with respect to a reference axis
			
			chirality (string): chirality state of the site
				Possible values: '+' or '-'
		"""
		self.chemical_type = chemical_type
		self.orientation = orientation
		self.chirality = chirality
		
		# Store itself in common repertory of sites
		equivalent_sites = equivalency_storage.equivalent_sites
		if self not in equivalent_sites:
			equivalent_sites[self] = {self}
	
	@classmethod
	def from_partitioned_occupied_site(cls, occupied_site, sites_partition_tree, equivalency_storage):
		# Get associated final partition
		site_partition = sites_partition_tree.get_site_partition(occupied_site)
		
		# Iterate over molecular binding sites of the adsorbate
		adsorbate_binding_sites = []
		for ads_type, orientation, chirality in zip(site_partition.ads_type, site_partition.orientation, site_partition.chirality):
			adsorbate_binding_sites.append(cls(ads_type, orientation, chirality, equivalency_storage))
		
		# Retrieve surface binding sites numbers
		surface_binding_sites_numbers = site_partition.site_number
		return(adsorbate_binding_sites, surface_binding_sites_numbers)
	
	def get_chirality(self, mirrored=False):
		if not mirrored:
			return(self.chirality)
		
		if self.chirality == '+':
			return('-')
		else:
			return('+')
	
	def get_equivalents(self, equivalency_storage):
		equivalent_sites = equivalency_storage.equivalent_sites
		return(equivalent_sites[self])
	
	def set_equivalent_to(self, other, equivalency_storage):
		equivalent_sites = equivalency_storage.equivalent_sites
		assert(other in equivalent_sites)
		# Retrieve equivalents
		own_equivalents = equivalent_sites[self]
		other_equivalents = equivalent_sites[other]
		
		# Add other equivalents to own equivalents
		own_equivalents.update(other_equivalents)
		
		# Update equivalents of other equivalents 
		for other_equivalent in other_equivalents:
			equivalent_sites[other_equivalent] = own_equivalents
	
	def __hash__(self):
		return((self.chemical_type, self.orientation, self.chirality).__hash__())
	
	def __eq__(self, other):
		return((self.chemical_type, self.orientation, self.chirality) == (other.chemical_type, other.orientation, other.chirality))
	
	def __repr__(self):
		if self.orientation is not None:
			str_self = '{}{}({:.0f}°)'.format(self.chirality, self.chemical_type, self.orientation)
		else:
			str_self = '{}{}(None)'.format(self.chirality, self.chemical_type)
		return(str_self)
	
	@staticmethod
	def _get_generic_sites(abstract_sites, reverse_format=False, mirror_format=False):
		# Define read order slicing
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		# Keep only chemical type and chirality, in order
		return((abstract_site.chemical_type, abstract_site.get_chirality(mirrored=mirror_format)) for abstract_site in abstract_sites[slicing])

class AbstractSurfaceSiteConnection(AbstractConnection):
	"""Straight connection object between two abstract surface sites"""
	def __init__(self, length, angle_site1):
		self.length = length
		# Orientations are defined directed to other adsorbate: @-->####<--@
		self.angle_site1 = angle_site1
		# Assumes non curvy connections
		self.angle_site2 = angle_site1 + (180 if angle_site1 < 0 else -180)
	
	def reversed(self):
		return(self.__class__(self.length, self.angle_site2))
	
	def get_outermost_orientation(self, in_angle=True, reverse_format=False):
		# Get effective direction
		effective_in_angle = in_angle ^ reverse_format
		
		if effective_in_angle:
			return(self.angle_site2)
		else:
			return(self.angle_site1)
	
	@staticmethod
	def _get_edge_lengths(connections, reverse_format=False, precision=2):
		# Define read order slicing
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		# Keep only chemical type and chirality, in order
		return(round(connection.length, precision) for connection in connections[slicing])
	
	def __repr__(self):
		return('{}_{}_{}'.format(self.angle_site1, self.length, self.angle_site2))

class AbstractSurface(object):
	"""Surface object"""
	def __init__(self):
		# Defines graph of abstract surface binding sites
		self.graph = nx.DiGraph()
		# Example of nodes/edges
#		self.graph.add_node(site_number, abstract_site=AbstractBindingSite('Pt_top', None, '+'))
#		self.graph.add_edge(site1_number, site2_number, abstract_connection=AbstractSurfaceSiteConnection(2.0, site1_angle_with_arbitrary_direction, -90))
	
	@classmethod
	def from_structure(cls):
		pass # XXX
	
	def add_site(self, site_number, abstract_site):
		# Create new surface site node
		self.graph.add_node(site_number, abstract_site=abstract_site)
	
	def add_connections(self, site1_number, site2_number, length, site1_out_angle):
		# Create abstract surface sites connection
		connection = AbstractSurfaceSiteConnection(length, site1_out_angle)
		
		# Create associated surface sites edges 
		self.graph.add_edge(site1_number, site2_number, abstract_connection=connection)
		self.graph.add_edge(site2_number, site1_number, abstract_connection=connection.reversed())
	
	def get_shortest_paths(self, site1, site2, max_length=None):
		"""Get shortest paths from two occupied sites on a surface.
		
		Parameters:
			self (Surface object): surface onto which paths must be found
			
			site1 (int): occupied site number from/to which paths must be found
			
			site2 (int): occupied site number from/to which paths must be found
			
			max_length (int): maximum acceptable length (number of nodes) of shortest paths. Path longer are discarded.
				Default: None (i.e. all lengths accepted)
		"""
		# Get shortest paths
		shortest_paths = tuple(nx.all_shortest_paths(self.graph, site1, site2))
		if max_length is not None:
			shortest_paths = tuple(path for path in shortest_paths if len(path) <= max_length)
		
		# Build tuple of AbstractPath objects from shortest paths
		abstract_paths = tuple(AbstractPath.from_surface_path(path, self) for path in shortest_paths)
		return(abstract_paths, shortest_paths)
	
class AbstractPath(AbstractConnection):
	"""Abstract path object, representing a connection type between different occupied sites, as a shortest path on an AbstractSurface."""
	
	def __init__(self, sites, edges):
		self.sites = sites
		self.edges = edges
		self._generic_format = None
		self._reverse_format = None
		self._mirror_format = None
		self._mirror_reverse_format = None
	
	@classmethod
	def from_surface_path(cls, path, surface):
		"""Create abstract path from list of nodes on a Surface object"""
		assert(len(path) > 1)
		sites = tuple(surface.graph.nodes[node]['abstract_site'] for node in path)
		edges = tuple(surface.graph.edges[u, v]['abstract_connection'] for u, v in nx.utils.misc.pairwise(path))
		return(cls(sites, edges))
	
	def get_outermost_orientation(self, in_angle=True, reverse_format=False):
		# Define read order slicing
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		if in_angle:
			return(self.edges[slicing][-1].get_outermost_orientation(in_angle=True, reverse_format=reverse_format))
		else:
			return(self.edges[slicing][0].get_outermost_orientation(in_angle=False, reverse_format=reverse_format))
	
	def get_equivalents(self, equivalency_storage):
		# Generate equivalents for each site
		equivalents_of_sites = (site.get_equivalents(equivalency_storage) for site in self.sites)
		
		# Generate equivalent paths, as combinations of all equivalents sites (cartesian product)
		equivalent_paths = []
		for sites in itertools.product(*equivalents_of_sites):
			# Create equivalent path
			new_path = AbstractPath(sites, self.edges)
			# Store path
			equivalent_paths.append(new_path)
		return(equivalent_paths)
	
	def compute_generic_format(self, reverse_format=False, mirror_format=False):
		# Compute generic format for sites in path (excluding first and last surface sites, (only compute the connection))
		generic_sites = self._get_generic_sites(reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Compute edge lengths
		edge_lengths = self._get_edge_lengths(reverse_format=reverse_format)
		
		# Compute in_edge to out_edge angles
		edge_edge_angles = self._get_edge_edge_angles(reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Compute in_edge to surface site angles
		edge_site_angles = self._get_edge_site_angles(reverse_format=reverse_format, mirror_format=mirror_format)
		
		return((generic_sites, edge_lengths, edge_edge_angles, edge_site_angles))
	
	def _get_generic_sites(self, reverse_format=False, mirror_format=False):
		return(tuple(AbstractBindingSite._get_generic_sites(self.sites[1:-1], reverse_format=reverse_format, mirror_format=mirror_format)))
	
	def _get_edge_lengths(self, reverse_format=False):
		return(tuple(AbstractSurfaceSiteConnection._get_edge_lengths(self.edges, reverse_format=reverse_format)))
	
	def _get_edge_edge_angles(self, reverse_format=False, mirror_format=False):
		return(tuple(AbstractConnection._get_connection_connection_angles(self.edges, reverse_format=reverse_format, mirror_format=mirror_format)))
	
	def _get_edge_site_angles(self, reverse_format=False, mirror_format=False):
		# Get abstract sites sequence
		abstract_sites_sequence = tuple((site,) for site in self.sites[1:-1])
		return(tuple(AbstractConnection._get_connection_sites_angles(self.edges, abstract_sites_sequence, reverse_format=reverse_format, mirror_format=mirror_format)))
	
	@property
	def generic_format(self):
		# Compute if not available
		if self._generic_format is None:
			self._generic_format = self.compute_generic_format()
		
		return(self._generic_format)
	
	@property
	def reverse_format(self):
		# Compute if not available
		if self._reverse_format is None:
			self._reverse_format = self.compute_generic_format(reverse_format=True)
		
		return(self._reverse_format)
	
	@property
	def mirror_format(self):
		# Compute if not available
		if self._mirror_format is None:
			self._mirror_format = self.compute_generic_format(mirror_format=True)
		
		return(self._mirror_format)
	
	@property
	def mirror_reverse_format(self):
		# Compute if not available
		if self._mirror_reverse_format is None:
			self._mirror_reverse_format = self.compute_generic_format(reverse_format=True, mirror_format=True)
		
		return(self._mirror_reverse_format)
	
	@property
	def nb_edges(self):
		return(len(self.edges))
	
class AbstractOccupiedSurface(object):
	"""Abstract occupied surface, representing abstract occupied sites and abstract paths between them"""
	
	def __init__(self, configuration, surface, equivalency_storage, max_length=None):
		# Store equivalency storage
		self.equivalency_storage = equivalency_storage
		
		# Initiate abstract graph, nodes are adsorbate binding sites, edges are abstract paths
		self.graph = nx.DiGraph()
		
		binding_sites_couples = []
		# Get all couples (ads_bind_site, surface_site_number)
		for occupied_site in configuration.occupied_sites:
			ads_sites, surf_sites_numbers = AbstractBindingSite.from_partitioned_occupied_site(occupied_site, configuration.sites_partition_tree, equivalency_storage)
			binding_sites_couples.extend(zip(ads_sites, surf_sites_numbers))
		
		# Create abstract graph nodes (adsorbate binding sites)
		for node_index, (ads_site, surf_site_number) in enumerate(binding_sites_couples):
			surface_abstract_site = surface.graph.nodes[surf_site_number]['abstract_site']
			self.graph.add_node(node_index, abstract_site=ads_site, surface_abstract_site=surface_abstract_site, site_number=surf_site_number)
		
		# Create abstract edges (sequences of abstract paths between adsorbate binding sites)
		# TODO Note that only one-way paths could be considered (i.e. undirected graph), as return paths could be obtained by reversing them on the fly
		for data_site1, data_site2 in itertools.combinations(enumerate(binding_sites_couples), 2):
			# Get surface binding sites info
			node1, (_, surf_site1_number) = data_site1
			node2, (_, surf_site2_number) = data_site2
			
			# Retrieve all shortest paths between binding sites on surface (possible lateral interactions)
			abstract_paths, site_number_paths = surface.get_shortest_paths(surf_site1_number, surf_site2_number, max_length=max_length)
			
			# Store abstract paths in new edge, if sites are close enough on surface
			if abstract_paths:
				self.graph.add_edge(node1, node2, abstract_paths=abstract_paths, site_number_paths=site_number_paths)
			
			# Idem for reverse paths
			abstract_paths, site_number_paths = surface.get_shortest_paths(surf_site2_number, surf_site1_number, max_length=max_length)
			if abstract_paths:
				self.graph.add_edge(node2, node1, abstract_paths=abstract_paths, site_number_paths=site_number_paths)
	
	def get_patterns(self, max_order=None, max_sites=None):
		assert(max_sites is None or max_sites > 0)
		assert(max_order is None or max_order > 0)
		# Convert max order into max path length (nb of edges) cutoff
		try:
			cutoff = max_order-1
		except TypeError:
			cutoff = None
		# Convert max sites into max edges
		try:
			max_edges = max_sites-1
		except TypeError:
			max_edges = None
		
		found_patterns = []
		
		# Generate all 1-body-term patterns
		for node in self.graph.nodes:
			node_sites = (self.graph.nodes[node]['abstract_site'],)
			found_patterns.append(Pattern(self.equivalency_storage, node_sites, tuple(), self.graph.nodes[node]['surface_abstract_site']))
		
		patterns_by_label = dict()
		
		# Generate all 2+-body-terms patterns as simple paths on the abstract occupied surface, with at most max_order-1 edges
		for pair_index, (node1, node2) in enumerate(itertools.combinations(range(self.graph.number_of_nodes()), 2)):
			if DEBUG_MODE: print('\rExtracting patterns... {}%. Total distincts patterns: {}'.format(100*pair_index/binom(self.graph.number_of_nodes(), 2), self.equivalency_storage.nb_distinct_patterns), end='')
			# Generate all simple paths between (each) two nodes
			for node_path in nx.all_simple_paths(self.graph, node1, node2, cutoff=cutoff):
				# Retrieve adsorbate abstract sites seen on path, in order
				node_sites = tuple(self.graph.nodes[node]['abstract_site'] for node in node_path)
				# Retrieve occupied site numbers
				occ_sites_numbers = tuple(self.graph.nodes[node]['site_number'] for node in node_path)
				# Retrieve tuples of possible (abstract_path + site_number_path) seen along the path, in order (e.g. ((A-B_path1, A-B_path2), (B-C_path1,), ...) = (all_A-B_paths, all_B-C_paths, ...))
				abstract_paths_path = tuple(tuple(zip(self.graph.edges[u, v]['abstract_paths'], self.graph.edges[u, v]['site_number_paths'])) for u, v in nx.utils.misc.pairwise(node_path))
				# Generate all combinations of abstract path sequences 
				for path_data_sequence in itertools.product(*abstract_paths_path):
					# Extract sequence of abstract paths and sequence of site numbers from sequence of (abstract_path + site_number_path)
					abstract_path_sequence, site_number_path_sequence = tuple(zip(*path_data_sequence))
					# Exclude patterns with more total edges than max_edges
					nb_edges = sum(path.nb_edges for path in abstract_path_sequence)
					if max_edges and nb_edges > max_edges:
						continue
					# Add associated acceptable pattern
					pattern = Pattern(self.equivalency_storage, node_sites, abstract_path_sequence)
					found_patterns.append(pattern)
					# Get unicity label for unique graph on surface (ordered occupied_site_numbers + ordered set of ordered edges)
					pattern_label = (tuple(sorted(occ_sites_numbers)), tuple(sorted(set(tuple(sorted((u,v))) for site_number_path in site_number_path_sequence for u, v in nx.utils.misc.pairwise(site_number_path)))))
					# Store automorphism-based equivalent patterns (inter and intra pattern indexes)
					original_index = pattern.get_index(original=True)
					if pattern_label not in patterns_by_label:
						patterns_by_label[pattern_label] = dict()
					if original_index not in patterns_by_label[pattern_label]:
						patterns_by_label[pattern_label][original_index] = 0
					patterns_by_label[pattern_label][original_index] += 1
		
		# Set equivalency between patterns with same unique labels
		for equivalent_patterns_by_index in patterns_by_label.values():
			# Set multiplicities occurring from automorphism
			for original_index in equivalent_patterns_by_index:
				self.equivalency_storage.set_pattern_multiplicity(original_index, multiplicity=equivalent_patterns_by_index[original_index], automorphism=True)
			
			# Merge all equivalent indexes
			reference_index = original_index
			for original_index in equivalent_patterns_by_index:
				self.equivalency_storage.merge_indexes(original_index, reference_index)
		
		return(found_patterns)
	
class Pattern(object):
	"""Pattern object, representing a lateral interaction as a generic path on an AbstractOccupiedSurface object"""
	
	def __init__(self, equivalency_storage, abstract_sites, abstract_paths, abstract_surface_site=None, search_update_equivalents=True, reverse_format=False, mirror_format=False):
		# Store equivalency storage
		self.equivalency_storage = equivalency_storage
		
		# Build ordered generic sites (i.e. chemical type and chirality)
		self.generic_sites = self._get_generic_sites(abstract_sites, abstract_paths, abstract_surface_site, reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Build ordered generic paths (i.e. AbstractPath objects in generic format)
		self.generic_paths = self._get_generic_paths(abstract_paths, reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Build ordered path_to_path angles (i.e. out_path orientation with respect to in_path)
		self.path_path_angles = self._get_path_path_angles(abstract_paths, reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Build ordered path_to_sites angles (i.e. sites orientation with respect to in_path)
		self.path_sites_angles = self._get_path_sites_angles(abstract_sites, abstract_paths, abstract_surface_site, reverse_format=reverse_format, mirror_format=mirror_format)
		
		# Search and update equivalent patterns if requested
		if search_update_equivalents:
			assert(equivalency_storage is not None)
			self.search_update_equivalents(abstract_sites, abstract_paths, abstract_surface_site)
	
	@staticmethod
	def _get_generic_sites(abstract_sites, abstract_paths, abstract_surface_site, reverse_format=False, mirror_format=False):
		# Retrieve surface sites
		if not abstract_paths:
			# If no path (i.e. single-term pattern), surface site must be stored in abstract_surface_site
			assert(abstract_surface_site is not None)
			assert(len(abstract_sites) == 1)
			abstract_surface_sites = (abstract_surface_site,)
		else:
			abstract_surface_sites = [path.sites[0] for path in abstract_paths] + [abstract_paths[-1].sites[-1]] # Select all different paths endpoints in order
		
		# Keep only chemical type and chirality, in order
		generic_adsorbate_sites = AbstractBindingSite._get_generic_sites(abstract_sites, reverse_format=reverse_format, mirror_format=mirror_format)
		generic_surface_sites = AbstractBindingSite._get_generic_sites(abstract_surface_sites, reverse_format=reverse_format, mirror_format=mirror_format)
		
		return(tuple(zip(generic_adsorbate_sites, generic_surface_sites)))
	
	@staticmethod
	def _get_generic_paths(abstract_paths, reverse_format=False, mirror_format=False):
		# Define read order slicing
		if reverse_format:
			slicing = slice(None, None, -1)
		else:
			slicing = slice(None, None, 1)
		
		# Keep only paths in generic format, in order
		if reverse_format and mirror_format:
			return(tuple(path.mirror_reverse_format for path in abstract_paths[slicing]))
		
		if reverse_format:
			return(tuple(path.reverse_format for path in abstract_paths[slicing]))
		
		if mirror_format:
			return(tuple(path.mirror_format for path in abstract_paths[slicing]))
		
		return(tuple(path.generic_format for path in abstract_paths[slicing]))
	
	@staticmethod
	def _get_path_path_angles(abstract_paths, reverse_format=False, mirror_format=False):
		# Compute in_path to out_path angles
		return(tuple(AbstractConnection._get_connection_connection_angles(abstract_paths, reverse_format=reverse_format, mirror_format=mirror_format)))
	
	@staticmethod
	def _get_path_sites_angles(abstract_sites, abstract_paths, abstract_surface_site, reverse_format=False, mirror_format=False):
		# If no path (i.e. single-term pattern), choose in_path orientation as surf_site orientation (or ads_site orientation if not available)
		if not abstract_paths:
			# Retrieve absolute orientations
			assert(abstract_surface_site is not None)
			assert(len(abstract_sites) == 1)
			adsorbate_site_orientation = abstract_sites[0].orientation
			surface_site_orientation = abstract_surface_site.orientation
			if surface_site_orientation is not None:
				in_path_orientation = surface_site_orientation
			else:
				in_path_orientation = adsorbate_site_orientation
			
			# Compute and return relative orientations
			path_ads_site_angle = angles_diff(in_path_orientation, adsorbate_site_orientation, mirrored=mirror_format)
			path_surf_site_angle = angles_diff(in_path_orientation, surface_site_orientation, mirrored=mirror_format)
			return(((path_ads_site_angle, path_surf_site_angle),))
		
		# Retrieve adsorbate and surface abstract sites
		surface_sites = [path.sites[0] for path in abstract_paths] + [abstract_paths[-1].sites[-1]] # Select all different paths endpoints in order
		abstract_sites_sequence = tuple(zip(abstract_sites, surface_sites))
		
		# First/last sites have no in/out paths. Using their out/in paths as in/out paths in paths extension
		# Retrieve paths extended with outermost connections to be considered reversed
		extended_abstract_paths = (abstract_paths[0],) + abstract_paths + (abstract_paths[-1],)
		
		# Compute paths sites angles and return them in a hashable way
		return(tuple(AbstractConnection._get_connection_sites_angles(extended_abstract_paths, abstract_sites_sequence, reverse_format=reverse_format, mirror_format=mirror_format, reverse_extended=True)))
	
	def search_update_equivalents(self, abstract_sites, abstract_paths, abstract_surface_site):
		# Retrieve pattern index lookup table
		pattern_index = self.equivalency_storage.pattern_index
		
		# Check if pattern has already been treated
		if self in pattern_index:
			return # Nothing to do
		
		# Get new pattern index
		new_pattern_index = self.equivalency_storage.get_new_pattern_index()
		
#		self.equivalency_storage.pattern_constructors[new_pattern_index] = (abstract_sites, abstract_paths, abstract_surface_site) # debug patterns generation
		
		# Generate and store all equivalent patterns (sites equivalence + reversibility + enantiomerism)
		# First, generate all equivalents for each abstract adsorbate site
		equivalents_of_abstract_sites = (site.get_equivalents(self.equivalency_storage) for site in abstract_sites)
		# Iterate over all equivalent abstract sites sequences
		for equivalent_abstract_sites in itertools.product(*equivalents_of_abstract_sites):
			# Generate all equivalents for each abstract path
			equivalents_of_abstract_paths = (path.get_equivalents(self.equivalency_storage) for path in abstract_paths)
			# Iterate over all equivalent abstract paths sequences
			for equivalent_abstract_paths in itertools.product(*equivalents_of_abstract_paths):
				# Generate all abstract surface site equivalents
				if abstract_surface_site is None:
					abstract_surface_site_equivalents = (None,)
				else:
					abstract_surface_site_equivalents = abstract_surface_site.get_equivalents(self.equivalency_storage)
				# Iterate over all equivalent abstract surface sites
				for equivalent_abstract_surface_site in abstract_surface_site_equivalents:
					# Generate equivalent pattern
					equivalent_pattern = Pattern(self.equivalency_storage, equivalent_abstract_sites, equivalent_abstract_paths, equivalent_abstract_surface_site, search_update_equivalents=False)
					# Memorize with same new index
					pattern_index[equivalent_pattern] = new_pattern_index
					
					# Generate equivalent pattern reversed
					equivalent_pattern = Pattern(self.equivalency_storage, equivalent_abstract_sites, equivalent_abstract_paths, equivalent_abstract_surface_site, search_update_equivalents=False, reverse_format=True)
					# Memorize with same new index
					pattern_index[equivalent_pattern] = new_pattern_index
					
					# Generate equivalent pattern mirrored
					equivalent_pattern = Pattern(self.equivalency_storage, equivalent_abstract_sites, equivalent_abstract_paths, equivalent_abstract_surface_site, search_update_equivalents=False, mirror_format=True)
					# Memorize with same new index
					pattern_index[equivalent_pattern] = new_pattern_index
					
					# Generate equivalent pattern mirrored and reversed
					equivalent_pattern = Pattern(self.equivalency_storage, equivalent_abstract_sites, equivalent_abstract_paths, equivalent_abstract_surface_site, search_update_equivalents=False, reverse_format=True, mirror_format=True)
					# Memorize with same new index
					pattern_index[equivalent_pattern] = new_pattern_index
	
	def get_index(self, original=False):
		return(self.equivalency_storage.get_index(self, original=original))
	
	def get_multiplicity(self):
		return(self.equivalency_storage.get_multiplicity(self))
	
	def __hash__(self):
		return((self.generic_sites, self.generic_paths, self.path_path_angles, self.path_sites_angles).__hash__())
	
	def __eq__(self, other):
		return((self.generic_sites, self.generic_paths, self.path_path_angles, self.path_sites_angles, id(self.equivalency_storage)) == (other.generic_sites, other.generic_paths, other.path_path_angles, other.path_sites_angles, id(other.equivalency_storage)))
	
	def __repr__(self):
		return('occupied sites: {}\nabstract paths (intermediary sites, distances, angles, path-surf angles): {}\npath-path angles: {}\npath-sites angles (path-ads, path-surf): {})'.format(self.generic_sites, self.generic_paths, self.path_path_angles, self.path_sites_angles))

class ModelHandler(object):
	def __init__(self, model, model_single=None):
		self.model = model
		self.model_single = model_single if model_single is not None else RecursiveModelOrthogonal(covariance_update=True)
		self.active_indexes = []
	
	def add_config_to_model(self, patterns_dict, energy, new_contrib_guess=0, new_variances_guess=np.nan, initialize=False, duplicates=False):
		# Update active variables and retrieve number of new variables to add to the model, if any
		nb_new_var = self.update_active_variables(patterns_dict)
		
		# Add new variables to the model, if required
		if nb_new_var > 0:
			if np.isscalar(new_variances_guess):
				new_variances_guess = np.eye(nb_new_var)*new_variances_guess
			self.model.add_new_regressors(nb_new_var, new_param_guess=new_contrib_guess, new_variances_guess=new_variances_guess)
			if initialize:
				self.model_single.add_new_regressors(nb_new_var, new_param_guess=new_contrib_guess, new_variances_guess=new_variances_guess)
		elif duplicates:
			# If dealing with potential duplicates, only store if unseen variables (redundant config are discarded)
			return(0, (0, 0), 0) #XXX Instead, one could check that parameter update is null before discarding
		
		# Save (opposite of) previous covariance matrices
		guess_variance_change, norm_empiric_variance_change = np.negative(self.model.guess_variance), np.negative(self.model.norm_empiric_variance)
		
		# Convert config into active regressors for the model
		regressors = self._extract_active_space(patterns_dict)
		
		# Replace dummy energy value by arbitrary value
		if energy is None:
			energy = 0
		
		# Update model
		pred_contrib_change = self.model.add_observation(regressors, energy)
		if initialize:
			self.model_single.add_observation(regressors, energy)
		
		# Compute covariance changes
		guess_variance_change += self.model.guess_variance
		norm_empiric_variance_change += self.model.norm_empiric_variance
		
		return(pred_contrib_change, (guess_variance_change, norm_empiric_variance_change), nb_new_var)
	
	def update_active_variables(self, patterns_dict):
		# Retrieve not yet active indexes in config
		new_indexes = set(patterns_dict).difference(self.active_indexes)
		
		# Add new indexes to active indexes
		self.active_indexes.extend(new_indexes)
		
		# Return number of new indexes
		return(len(new_indexes))
	
	def predict_deviation(self, patterns_dict):
		# Retrieve active model regressors
		config = self._extract_active_space(patterns_dict)
		
		# Return predicted energy deviation
		return(np.dot(self.model_deviation_contributions, config))
	
	def single_body_energy(self, patterns_dict):
		# Retrieve active single-body model regressors (assumes same regressors order as full model)
		config = self._extract_active_space(patterns_dict, nb_params=self.model_single.nb_regressors)
		
		# Return single-body energy deviation
		return(np.dot(self.model_single.parameters, config))
	
	def get_deviation_contributions(self, patterns_dict):
		# Retrieve active model regressors
		config = self._extract_active_space(patterns_dict)
		
		# Return energy contributions
		return(np.multiply(config, self.model_deviation_contributions))
	
	def run_cross_validation(self, method=None, metric=None, **metric_kwargs):
		"""Cross-validation based estimator
		
		Parameters:
			method (CV splitter, optional): Cross-validation generator to be used
				Note: If None, uses sklearn.model_selection.LeaveOneOut() splitter
				Default: None (LOO method)
			
			metric (function, optional): Metric to use for assessing the score on a test set
				Note: If None, uses sklearn.metrics.mean_squared_error, with squared=False
				Default: None (RMSE metric)
			
			metric_kwargs (dict, optional): Additional arguments to be given to the metric function
		
		Returns:
			Score average
		
		Note:
			By default, this returns the average prediction error for each observation with training on all other observations
		"""
		
		# Retrieve method by default if requested
		if method is None:
			method = LeaveOneOut()
		
		# Retrieve metric by default if requested
		if metric is None:
			metric = mean_squared_error
			metric_kwargs['squared'] = False
		
		# Retrieve observations and target values
		X = self.model.observations
		y = self.model.target_values
		
		# Compute all scores
		scores = []
		for train, test in method.split(X):
			# Train model
			model = self.model.__class__()
			model.add_observations(X[train], y[train])
			
			# Predict test set
			y_pred = np.dot(X[test], model.parameters)
			
			# Compute score
			scores.append(metric(y[test], y_pred, **metric_kwargs))
		
		# Return average score
		return(sum(scores)/len(scores))
	
	def cook_distances(self):
		"""Compute Cook's distance for each observation
		
		Returns: Cook's distances (in order)
		
		Note:
			Cook's distance for observation Γ_i is D(i) = sum_j((y_j - y'_j)²)/ms²,
			where y'_j is the predicted value of Γ_j using a model trained without Γ_i, m is the number of regressors and s²=tr(err).err/(n-k) (with err the residuals, n the number of observations and k the rank of the observations) is the estimator of the variance of the norm (or also called regression MSE)
		"""
		
		# Retrieve counters
		n = self.model.nb_obs
		k = self.model.nb_core_obs
		m = self.model.nb_regressors
		
		# Retrieve observations
		X = self.model.observations
		y = self.model.target_values
		
		# Compute predictions, residuals and variance of the noise
		y_pred = np.dot(X, self.model.parameters)
		residuals = y_pred - y
		s2 = ((y_pred - y)**2).sum()/(n-k) if n > k else np.nan
		
		# Iterate over all observations
		cook_distances = []
		for i, observation in enumerate(X):
			# Create partially trained model
			train = (np.arange(n) != i) # mask to select all but index i
			model = self.model.__class__()
			model.add_observations(X[train], y[train])
			
			# Predict all target values with both models
			y_partial_pred = np.dot(X, model.parameters)
			
			# Compute and store Cook's distance
			cook_distances.append(((y_pred-y_partial_pred)**2).sum()/(m*s2))
		
		return(cook_distances)
	
	@property
	def model_deviation_contributions(self):
		# Copy full model Hamiltonian
		deviation_contributions = self.model.parameters.copy()
		
		# Substract single-body model (assumes same order for the regressors)
		deviation_contributions[:self.model_single.nb_regressors] -= self.model_single.parameters
		
		return(deviation_contributions)
	
	def _extract_active_space(self, patterns_dict, nb_params=None):
		"""Convert the pattern multiplicities dictionnary of a configuration into a vector of active regressors for the model Hamiltonian"""
		return(np.array(tuple(patterns_dict.get(index, 0) for index in self.active_indexes[:nb_params])))

class PartitionStorage(object):
	def __init__(self):
		# Real energy deviation
		self.nb_deviation = 0
		self.sum_abs_deviation = 0
		self.min_abs_deviation = math.inf
		self.max_abs_deviation = 0
		self.node_abs_deviation = None
		self.quality_deviation = 1
		# Patterns
		self.node_patterns = dict()
	
	@property
	def mean_abs_deviation(self):
		if self.nb_deviation <= 0:
			return(math.nan)
		
		return(self.sum_abs_deviation/self.nb_deviation)
	
	def add_deviation(self, deviation, nb_adsorbates=1, set_node_deviation=False, exploration_quality=1):
		# Use absolute energy deviation per adsorbate
		abs_deviation = abs(deviation/nb_adsorbates)
		
		# Set current node absolute energy deviation, if requested
		if set_node_deviation:
			self.node_abs_deviation = abs_deviation
		
		# Update sum
		self.sum_abs_deviation += abs_deviation
		
		# Update min/max
		self.min_abs_deviation = min(self.min_abs_deviation, abs_deviation)
		self.max_abs_deviation = max(self.max_abs_deviation, abs_deviation)
		
		# Increase counter
		self.nb_deviation += 1
		
		# Compute new exploration quality
		self.quality_deviation = ((self.nb_deviation-1)*self.quality_deviation + exploration_quality) / self.nb_deviation
	
	def estimate_extrema_abs_deviation(self, min_range=0):
		# Return estimated distribution extrema
		return(self._estimate_extrema_positive(self.min_abs_deviation, self.max_abs_deviation, self.nb_deviation, min_range=min_range))
	
	@staticmethod
	def KL_UCB_score(mean, dist_min, dist_max, nb_values, bias_prefactor):
		# UCB of maximum possible reward is itself
		if mean == dist_max:
			return(dist_max)
		
		# Convert to [0,1] range
		mean_normalized = (mean - dist_min)/(dist_max - dist_min)
		
		# Compute KL-UCB score
		exploration_bias = bias_prefactor/nb_values
		normalized_KL_UCB = scopt.brentq(__class__._kl_div_diff, mean_normalized, 1, args=(mean_normalized, exploration_bias))
		
		# Convert back to dist range
		KL_UCB = normalized_KL_UCB*(dist_max - dist_min) + dist_min
		return(KL_UCB)
	
	@staticmethod
	def UCB1_score(mean, dist_min, dist_max, nb_values, bias_prefactor, confidence_level=math.sqrt(2)):
		# UCB of maximum possible reward is itself
		if mean == dist_max:
			return(dist_max)
		
		# Convert to [0,1] range
		mean_normalized = (mean - dist_min)/(dist_max - dist_min)
		
		# Compute UCB1 score
		exploration_bias = bias_prefactor/nb_values
		normalized_UCB1 = mean_normalized + confidence_level*math.sqrt(exploration_bias)
		
		# Convert back to dist range
		UCB1 = normalized_UCB1*(dist_max - dist_min) + dist_min
		return(UCB1)
	
	@staticmethod
	def _kl_div_diff(q, p, bias):
		"""# Define difference between exploration bias and Kullback-Leibler divergence"""
		
		kl_div = 0
		
		if p != 0:
			if q == 0:
				kl_div += math.inf
			else:
				kl_div += p*math.log(p/q)
		
		if p != 1:
			if q == 1:
				kl_div += math.inf
			else:
				kl_div += (1-p)*math.log((1-p)/(1-q))
		
		return(kl_div - bias)
	
	@staticmethod
	def _estimate_extrema_positive(min_positive_value, max_positive_value, nb_values, min_range=0):
		"""Estimate distribution extrema from sample extrema, assuming uniform distribution of a positive variable
		
		Computational details:
			Assuming a uniform distribution, one have:
				emp_min = (dist_min*nb_values + dist_max)/(nb_values+1)
				emp_max = (dist_min + dist_max*nb_values)/(nb_values+1)
		"""
		# Return arbitrary large value if not enough data
		if nb_values < 2:
			return(0, max(2*max_positive_value, min_range))
		
		dist_min = (min_positive_value*nb_values - max_positive_value)/(nb_values-1)
		dist_max = (max_positive_value*nb_values - min_positive_value)/(nb_values-1)
		
		range_add = max(min_range - (dist_max-dist_min), 0)
		
		dist_min -= range_add/2
		dist_max += range_add/2
		
		# Make sure dist_min >= 0
		if dist_min < 0:
#			dist_max -= dist_min
			dist_min = 0
		
		return(dist_min, dist_max)
	
	@staticmethod
	def _estimate_extrema_fixed_min(dist_min, max_positive_value, nb_values, min_range=0):
		"""Estimate distribution extrema from sample maximum and distribution minimum, assuming uniform distribution of a positive variable"""
		
		dist_max = (max_positive_value*(nb_values+1) - dist_min) / nb_values
		
		range_add = max(min_range - (dist_max-dist_min), 0)
		dist_max += range_add
		
		return(dist_min, dist_max)
	
	@staticmethod
	def convert_patterns(patterns):
		"""Convert raw patterns into a pattern multiplicities dictionnary"""
		patterns_dict = dict()
		for pattern in patterns:
			pattern_index = pattern.get_index()
			patterns_dict[pattern_index] = patterns_dict.get(pattern_index, 0) + 1/pattern.get_multiplicity()
		return(patterns_dict)
	
	def __repr__(self):
		return(str(self.__dict__))

class MCTS(object):
	"""Revisited MCTS object"""
	
	def __init__(self, body_term_order=2, max_length_cutoff=3, max_sites_cutoff=5, nb_pre_exploration_steps=100, seed=0):
		"""Create a revisited MCTS object.
	
		Parameters:
			body_term_order (int): maximal body term order considered for the cluster expansion model.
				Default: use a 2-body order cluster expansion model.
		
			max_length_cutoff (int): maximal path length (number of surface sites in path, endpoints included) between two adsorbates, for a lateral interaction to be considered.
				Default: explore up to next-to-nearest neighbors. (Example: A-?-B-?-C is accepted, but not A-?-?-B)
			
			max_sites_cutoff (int): maximal number of surface sites in pattern for a lateral interaction to be considered.
				Default: maximum 5 surface sites involved per pattern.
		"""
		#
		# MCTS-related attributes
		#
		# Set random seed, for reproducibility purposes
		self.seed = seed
		np.random.seed(self.seed)
		# Initialize tree with root node
		self.tree = nx.DiGraph()
		self.tree.add_node('root', partition_label='root', storage=PartitionStorage(), unsearchable=set())
		self.max_index = 0
		self.min_range = 0.5 # Minimum range considered for energies, when estimating bounds (i.e. assume E in [a, b], b-a will not be considered less that min_range)
		self.model = RecursiveModelOrthogonal(covariance_update=True)
		self.model_handler = ModelHandler(self.model, model_single=RecursiveModelOrthogonal(covariance_update=True))
		self.surface = AbstractSurface()
		self._nb_adsorbates = 0
		self.body_term_order = body_term_order
		self.max_length_cutoff = min(max_length_cutoff, max_sites_cutoff)
		self.max_sites_cutoff = max_sites_cutoff
		self.sites_partition_tree = SitesPartitionTree()
		self.equivalency_storage = EquivalencyStorage()
		self.nb_pre_exploration_steps = nb_pre_exploration_steps
		self.population_bias = True
		self.guess_variance = 10
		self.random = False
		self.exhaustive_search = False
		self.exploration_quality = 1
		self._manual = False
		self.target_config = None
		self.target_config_energy = None
		self.geometries_handler = GeometriesHandler('geometries.db')
		# Test only related parameters
		self.dev_ratio_err_std = 0
		self.const_err_std = 0
		self.tot_ratio_err_std = 0
		self.each_ratio_err_std = 0
	
	#########################
	#    MCTS defintions    #
	#########################
	
	# TODO: Idea: Double-step procedure: first, a virtual run is done with MCTS algorithm (V configurations are chosen and predicted). Among these configurations, only the "best" (defined by highest a priori change to model) is chosen for real computation. (the corresponding "virtual branch" is included in the tree as a real branch, and the other "virtual branches" are either deleted or kept as "virtual" or "ghost" branches to avoid redundant computation of branches (but with a space cost, and recognition cost...))
	# Notes: The deletion of some virtual branches is not straightforward as a deletion should not delete a node of a kept virtual branch. Or at least leave the tree as if the corresponding virtual run was not performed, which means that nodes should be deleted or their scores downgraded... This feature would require a new attribute 'keep' which should be updated on the nodes (and their direct children) belonging to the branches of corresponding virtual_run to keep. I believe this is too big a complication for a feature that is likely to be unused at the end... (But research here is about testing, so let's do it!)
	# TODO: Idea: The "virtual" branches that are kept can be selected for further exploration, and fully contribute to the upper-bound evaluation (a variant is to define a factor 'alpha' to rescale the contribution of virtual predictions in the tree. This factor could even be changed afterward, but this would double the space complexity of the tree, since real and virtual scores must be stored. Or to use a size hack, by using np.uint16). Therefore, is a 'virtual' attribute required? (a priori not, since predicted structures have the same impact as others to the tree). Nevertheless, a 'searchable' attribute is required to prevent nodes that are composed of already computed structures to be investigated. This attribute should only be updated when real computations are performed.
	# TODO: Idea: a time-space compromise can be used to save space by storing score for only odd depth nodes... (careful: need to ensure that score of full_configuration is always stored...)
	
	# Node attributes
	# node['type'] = type of the node in ['full_configuration' (representing a fully complete configuration), 'root', ??? to define (configuration, ...)]
	# node['depth'] = depth of the node (currently unused, for space/time compromise)
	# node['searchable'] = boolean(unexplored branches from nodes exists) = boolean(node has unexplored children)
	# node['keep'] = boolean(should node be kept (space/time compromise))
	# node['sum_virt_config'] = vector of sum of each virtual config component (over successors leaves) = [sum_{virt_config_i}(_{1}_{i}), ...]
	# node['nb_virt_config'] = number of virtual configurations stored/represented by all successors leaves
	# node['max_virt_config'] = vector of max of each virtual config component (over all seen successors leaves) = [max_{virt_config_i}(x_{1}_{i}), ...]
	# idem with real_config = idem, except that reprojected/reconstructed real configurations are used instead of virtual configurations
	
	def single_run(self, nb_adsorbates=None, target_model_handler=None):
		# Store desired number of adsorbates
		assert(self.nb_adsorbates or nb_adsorbates)
		if nb_adsorbates:
			self.nb_adsorbates = nb_adsorbates
		
		# Pre-exploration step
		pre_explored_nodes = self.pre_exploration(nb_steps=self.nb_pre_exploration_steps)
		
		# Choose most promising leaf
		best_node, best_config = self.choose_most_promising(pre_explored_nodes)
		if best_node is None:
			return(None)
		if DEBUG_MODE: print('most promising adsorbates configuration', best_config.occupied_sites)
		
		# Compute energy + deviation
		if DEBUG_MODE: print('most promising config', self.tree.nodes[best_node]['storage'].node_patterns)
		if self.target_config_energy:
			real_energy = self.target_config_energy
		elif target_model_handler: # XXX : intended for test purposes only
			node_patterns = self.tree.nodes[best_node]['storage'].node_patterns
			real_energy = (sum(sum(param*np.random.normal(loc=1, scale=self.each_ratio_err_std) for _ in range(round(node_patterns.get(target_model_handler.active_indexes[index], 0)))) for index, param in enumerate(target_model_handler.model.parameters)) * np.random.normal(loc=1, scale=self.tot_ratio_err_std)
			               + target_model_handler.predict_deviation(node_patterns)*np.random.normal(loc=0, scale=self.dev_ratio_err_std)
			               + np.random.normal(loc=0, scale=self.const_err_std))
			print(sum(node_patterns.get(target_model_handler.active_indexes[index], 0)*param for index, param in enumerate(target_model_handler.model.parameters)), target_model_handler.predict_deviation(node_patterns)+target_model_handler.single_body_energy(node_patterns), real_energy)
			assert(np.isclose(target_model_handler.predict_deviation(node_patterns)+target_model_handler.single_body_energy(node_patterns), sum(sum(param for _ in range(round(node_patterns.get(target_model_handler.active_indexes[index], 0)))) for index, param in enumerate(target_model_handler.model.parameters))))
			occupied_surface, _ = self.geometries_handler.build_occupied_geometry(best_config.occupied_sites)
			self.geometries_handler.write_to_database(self, occupied_surface, adsorption_energy=real_energy, occupied_sites=best_config.occupied_sites)
		else:
			real_energy = self.compute_adsorption_energy(best_config)
		
		# Compute deviation
		node_patterns = self.tree.nodes[best_node]['storage'].node_patterns
		real_deviation = real_energy - self.model_handler.single_body_energy(node_patterns)
		
		# Backpropagation
		self.tree.nodes[best_node]['storage'].add_deviation(real_deviation, nb_adsorbates=self.nb_adsorbates, set_node_deviation=True, exploration_quality=self.exploration_quality)
		for node in nx.ancestors(self.tree, best_node):
			self.tree.nodes[node]['storage'].add_deviation(real_deviation, nb_adsorbates=self.nb_adsorbates, exploration_quality=self.exploration_quality)
		self.set_unsearchable(best_node)
		
		# Update model
		patterns_dict = self.tree.nodes[best_node]['storage'].node_patterns
		if hasattr(self, 'cheating_max_index'):
			for index in tuple(patterns_dict.keys()):
				if index > self.cheating_max_index:
					del patterns_dict[index]
		self.model_handler.add_config_to_model(patterns_dict, real_energy, new_variances_guess=self.guess_variance)
		
		return(best_config) # XXX test debug
	
	def compute_adsorption_energy(self, config):
		# Build occupied surface
		occupied_sites = config.occupied_sites
		occupied_surface, sum_energy = self.geometries_handler.build_occupied_geometry(occupied_sites)
		
		# Compute adsorption energy
		adsorption_energy = self.geometries_handler.compute_adsorption_energy(occupied_surface, sum_energy)
		
		# Store/Archive results
		self.geometries_handler.write_to_database(self, occupied_surface, adsorption_energy=adsorption_energy, occupied_sites=occupied_sites)
		
		return(adsorption_energy)
	
	def pre_exploration(self, nb_steps=None):
		if nb_steps is None:
			nb_steps = self.nb_pre_exploration_steps
		pre_explored_nodes = []
		
		try:
			# Perform nb_steps pre-exploration steps
			while len(pre_explored_nodes) < nb_steps:
				# Select leaf using best-first search on UCB1 score
				chosen_node, chosen_config = self.select_configuration()
			
				# Pass and set non-searchable status if no searchable child was found
				if chosen_config is None:
					self.set_unsearchable(chosen_node)
					# Stop if whole tree is unsearchable
					if chosen_node == 'root':
						break
					continue
			
				# Extract config patterns multiplicities
				config_aos = AbstractOccupiedSurface(chosen_config, self.surface, self.equivalency_storage, max_length=self.max_length_cutoff)
				patterns = config_aos.get_patterns(max_order=self.body_term_order, max_sites=self.max_sites_cutoff)
				new_patterns_dict = PartitionStorage.convert_patterns(patterns)
			
				# Compute average many-body patterns path length
				mean_pattern_path = np.mean(tuple(len(path[0]) for pattern in patterns if len(pattern.generic_sites) > 1 for path in pattern.generic_paths)) + 1
			
				# Store pattern multiplicities in the node
				self.tree.nodes[chosen_node]['storage'].node_patterns = new_patterns_dict
			
				# Store chosen node
				pre_explored_nodes.append((chosen_node, chosen_config, mean_pattern_path))
			
				# Temporarily set unsearchable status on chosen node (to avoid selecting it twice)
				self.set_unsearchable(chosen_node)
		finally:
			# Remove temporary unsearchable status on pre-explored nodes (now than pre-exploration is over)
			for (chosen_node, _, _) in pre_explored_nodes:
				self.unset_unsearchable(chosen_node)
		
		return(pre_explored_nodes)
	
	def select_configuration(self):
		"""Perform selection process, by best-first approach until a configuration is found.
		
		Parameters:
			self (Model object): Model object on which tree to execute the selection step.
		
		Returns:
			selected_node (int): index of the selected node in the MCTS tree.
			
			selected_config (Configuration object): selected configuration.
		
		Computational details:
			The MCTS tree is explored by a best-first search approach until a leaf is found (i.e. a full_configuration node, a node that represents a full_configuration).
			Starting from the root node, the next node to be explored is the child node with the highest score, recursively.
			
			The choice of the best child is performed by the `self.get_best_child` method.
			
			If a non-terminal node (i.e. a non full_configuration node) explored has no children, its children are created and attached to the tree, using the `self.create_children` method.
			Therefore, this ensures that the search will finish on a terminal node, leading if necessary to the creation and attachement of the corresponding full branch (and their immediate children) to the tree.
		"""
		cur_node = 'root'
		cur_config = Configuration(self.sites_partition_tree)
		
		# Build configuration until reaching requested number of adsorbates on surface
		while len(cur_config.occupied_sites) < self.nb_adsorbates:
			# Create children if empty node
			if self.tree.out_degree(cur_node) == 0:
				self.create_children(cur_node, cur_config)
				if DEBUG_MODE: print('children created')
			
			if DEBUG_MODE: print('current config node:', cur_config.current_node, 'tree node:', self.tree.nodes[cur_node]['partition_label'], 'nb of ads placed:', len(cur_config.occupied_sites))
			if DEBUG_MODE: print('current node storage', self.tree.nodes[cur_node]['storage'])
			# Select best child
			if self.target_config:
				best_child = self.get_target_child(cur_node, cur_config)
			elif self.manual:
				best_child = self.get_manual_child(cur_node, cur_config)
			else:
				best_child = self.get_best_child(cur_node, cur_config)
			
			# If exploration cannot continue, stop branch and exit
			if not best_child:
				return(cur_node, None)
			
			# Move to best child
			cur_node = best_child
			cur_config.choose_partition(self.tree.nodes[best_child]['partition_label'])
		
		return(cur_node, cur_config)
	
	def create_children(self, parent_node, cur_config):
		"""Create available children according to possible partitions reachable from a current configuration object
		
		Parameters:
			self (Model object): Model object concerned.
			
			parent_node (int): index of the parent node.
			
			cur_config (Configuration object): current configuration state.
		
		Returns:
			None
		"""
		# Get possible partition labels
		partition_labels_avail = sorted(cur_config.get_choices())
		
		# Create associated children
		for partition_label in partition_labels_avail:
			self.max_index += 1
			child_node = self.max_index
			self.tree.add_node(child_node,
			                   partition_label=partition_label,
			                   storage=PartitionStorage(),
			                   unsearchable=set())
			self.tree.add_edge(parent_node, child_node)
	
	def get_best_child(self, parent_node, configuration):
		"""Return child with the highest corresponding score
		
		Parameters:
			self (Model object): Model object concerned.
			
			parent_node (int): index of the parent node.
			
			cur_nb_adsorbates (int, optional): number of adsorbates currently already placed on the surface.
			
			configuration (Configuration object): current configuration being constructed.
		
		Returns:
			best_child (int/None): index of the searchable node with the highest score amongst the provided nodes (i.e. `children`).
				Notes: If `children` contains no searchable nodes, return None.
		
		Computational details:
			Some nodes have already been fully explored (i.e. exhausted), and are therefore not searchable anymore.
			This function loops over the children nodes and selects the searchable node with the maximum score, using the `self.get_score` method.
			If the unsearchable tag is properly set, this function should never be called on the children of a non-searchable node. If `children` contains no searchable nodes, then returns None.
			This function precomputes some elements (the sqrt(log(T)) term) for increased computational efficiency and negligible memory cost.
		"""
		# Check if node is searcheable
		if self.nb_adsorbates in self.tree.nodes[parent_node]['unsearchable']:
			return(None)
		
		# Get children
		children = sorted(self.tree.successors(parent_node))
		if DEBUG_MODE: print('children', [self.tree.nodes[child]['partition_label'] for child in children])
		
		# Check if node has children
		if not children:
			return(None)
		
		# Get number of remaining adsorbates to select
		cur_nb_adsorbates = len(configuration.occupied_sites)
		nb_ads_remain = self.nb_adsorbates - cur_nb_adsorbates
		
		# Get best child
		best_score, best_children = -np.inf, []
		UCB_factors = self.precompute_UCB_prefactors(parent_node, children, nb_ads_remain, configuration) # Precompute only once the log(T) term and maximum number of descendants (since it is independant of the child)
		for cur_child in children:
			child = self.tree.nodes[cur_child]
			# Check that child is searcheable for required exploration depth
			if self.nb_adsorbates in child['unsearchable']:
				continue
			cur_score = self.get_score(child, *UCB_factors, nb_ads_remain, configuration.partition_tree_object)
			if DEBUG_MODE: print('score of child', child['partition_label'], cur_score)
			if cur_score > best_score:
				best_children = [cur_child]
				best_score = cur_score
			elif cur_score == best_score:
				best_children.append(cur_child)
		
		# Retrieve (bias-ratio based) weights for selecting a child among best children
		weights = []
		for child in best_children:
			# Retrieve population bias correction term (i.e. 1/number_of_descendants)
			if self.population_bias:
				bias_ratio = configuration.partition_tree_object.get_bias_ratio(self.tree.nodes[child]['partition_label'], nb_ads_remain, 1)
			else:
				bias_ratio = 1
			# Get 1/bias_ratio as weight
			weights.append(1/bias_ratio)
		# Normalize weights to get probability
		weights = np.array(weights)/sum(weights)
		
		# Choose a child at random
		best_child = np.random.choice(best_children, p=weights)
		return(best_child)
	
	def get_manual_child(self, parent_node, configuration):
		"""Return child chosen manually
		
		Parameters:
			self (Model object): Model object concerned.
			
			parent_node (int): index of the parent node.
			
			configuration (Configuration object): current configuration being constructed.
		
		Returns:
			best_child (int/None): index of the searchable node manually chosen among the provided nodes (i.e. `children`).
				Notes: If `children` contains no searchable nodes, return None.
		"""
		# Get children
		children = sorted(self.tree.successors(parent_node))
		if DEBUG_MODE: print('children', [self.tree.nodes[child]['partition_label'] for child in children])
		
		# Check if node has children
		if not children:
			print('\nNo available children were found... Stopping the search. (maybe check the symmetries/lexicographic order)')
			return(None)
		
		# Get number of remaining adsorbates to select
		cur_nb_adsorbates = len(configuration.occupied_sites)
		nb_ads_remain = self.nb_adsorbates - cur_nb_adsorbates
		
		# Print possible children
		possible_choices = []
		UCB_factor_real, max_nb_descendants = self.precompute_UCB_prefactors(parent_node, children, nb_ads_remain, configuration) # Precompute only once the log(T) term and maximum number of descendants (since it is independant of the child)
		print('\nSearchable children:\n')
		for cur_child in children:
			child = self.tree.nodes[cur_child]
			# Check that child is searcheable for required exploration depth
			if self.nb_adsorbates in child['unsearchable']:
				continue
			# Get score
			KL_UCB_real, KL_UCB_pred = self.get_score(child, UCB_factor_real, max_nb_descendants, nb_ads_remain, configuration.partition_tree_object)
			## Get statistics
			# Retrieve population bias correction term
			bias_ratio = 1
			if self.population_bias: bias_ratio = configuration.partition_tree_object.get_bias_ratio(child['partition_label'], nb_ads_remain, max_nb_descendants)
			# Get storage
			child_storage = child['storage']
			## Print info about child
			# Print label
			print('{}) {}'.format(len(possible_choices), child['partition_label']))
			# Print exploration stats
			print('Stats: nb_exp(corrected) = {} ({:.1f})\t\
			       nb_final_config = {:.0f} (estimate)'.format(child_storage.nb_deviation,
			                                              child_storage.nb_deviation*child_storage.quality_deviation*bias_ratio,
			                                              1 / configuration.partition_tree_object.get_bias_ratio(child['partition_label'], nb_ads_remain, 1)))
			# Print DFT deviation stats
			print('Real deviation: mean = {:.3f}\t\
			       min/max(uniform guess) = {:.3f}/{:.3f} ({:.3f}/{:.3f})\t\
			       KL_UCB_score = {:.3f}'.format(child_storage.mean_abs_deviation,
			                               child_storage.min_abs_deviation, child_storage.max_abs_deviation,
			                               *child_storage.estimate_extrema_abs_deviation(min_range=self.min_range),
			                               KL_UCB_real))
			possible_choices.append(cur_child)
		
		# Ask user choice
		while True:
			try:
				chosen_index = int(input('Index choice: '))
				chosen_child = possible_choices[chosen_index]
				break
			except (ValueError, IndexError):
				print('Index invalid, please choose an acceptable index')
		
		return(chosen_child)
	
	def get_target_child(self, parent_node, configuration):
		"""Return child toward a targeted final configuration (in `self.target_config`).
		
		Parameters:
			self (Model object): Model object concerned.
			
			parent_node (int): index of the parent node.
			
			configuration (Configuration object): current configuration being constructed.
		
		Returns:
			targeted_child (int/None): index of the searchable node chosen among the provided nodes (i.e. `children`).
				Notes: If `children` contains no searchable nodes, return None.
		"""
#		# Check that target configuration is well-defined (i.e. current configuration has same occupied sites)
#		assert([occupied_site.properties for occupied_site in self.target_config.occupied_sites[:len(configuration.occupied_sites)]] == [occupied_site.properties for occupied_site in configuration.occupied_sites])
		# Check that target configuration is defined
		assert(self.target_config is not None)
		
		# Get children
		children = sorted(self.tree.successors(parent_node))
		
		# Check if node has children
		if not children:
			return(None)
		
		# Get targeted occupied site
		cur_nb_adsorbates = len(configuration.occupied_sites)
		target_occupied_site = self.target_config.occupied_sites[cur_nb_adsorbates]
		assert target_occupied_site in configuration.partition_tree.successors(configuration.partition_tree_object.occupied_site_root), 'target_config is not compatible with current MCTS instance (i.e. some imported occupied sites are not accessible, this may be due to incompatible filters)'
		
		# Get partition labels ancestors of targeted occupied site in current configuration
		ancestor_partition_labels = tuple(nx.dfs_preorder_nodes(nx.reverse_view(configuration.partition_tree), target_occupied_site))
		
		# Find child leading to targeted configuration
		target_child = None
		for cur_child in children:
			child = self.tree.nodes[cur_child]
			
			# Check that child is searcheable for required exploration depth
			if self.nb_adsorbates in child['unsearchable']:
				continue
			
			# Check if child leads to targeted occupied site
			if child['partition_label'] in ancestor_partition_labels:
				target_child = cur_child
				break
		
		return(target_child)
	
	def precompute_UCB_prefactors(self, parent_node_index, children_indexes, nb_ads_remain, configuration):
		"""Compute UCB prefactor and maximum number of descendants among its children, common to all children of node considered (the log(T) term and nb_max_descendants)
		
		Parameters:
			self (Model object): Model object concerned.
			
			parent_node_index (int): index of the parent node from which the UCB1 prefactor depends.
			
			children_indexes (iterator of int): indexes of the children of parent_node.
			
			nb_ads_remain (int): number of adsorbates that remain to be placed on the surface.
			
			configuration (Configuration object): current configuration being constructed.
		
		Returns:
			UCB_factor (float): the UCB prefactor log(T) where T is the (population bias corrected) number of DFT energy records stored in the node
			
			max_nb_descendants (int): the maximum number of descendants among the children of parent_node (or None if self.population_bias is set to False)
		
		Computational details:
			This function helps to perform an efficient time/memory cost compromise, since the computation of this prefactor is costly and re-used among all children for score computation (and retrieving the best-child).
			
			The population bias corrected exploration numbers are computed by mulitplying real exploration numbers with a population bias correction ratio. More details can be found on #TODO
		"""
		# Get number of explorations
		parent_storage = self.tree.nodes[parent_node_index]['storage']
		if self.population_bias:
			# Get parent-related exploration statistics
			parent_partition_label = configuration.current_node
			max_nb_descendants = configuration.partition_tree_object.get_max_child_descendants(parent_partition_label, nb_ads_remain)
			
			# Get normalized number of (pre-)explorations
			nb_parent_deviation = 0
			for child_index in children_indexes:
				# Get child storages
				child_node = self.tree.nodes[child_index]
				child_label = child_node['partition_label']
				child_storage = child_node['storage']
				# Get normalized number of (pre-)explorations of child
				child_bias_ratio = configuration.partition_tree_object.get_bias_ratio(child_label, nb_ads_remain, max_nb_descendants)
				nb_parent_deviation += child_storage.nb_deviation * child_storage.quality_deviation * child_bias_ratio
		else:
			max_nb_descendants = None
			nb_parent_deviation = parent_storage.nb_deviation * parent_storage.quality_deviation
		
		try:
			UCB_factor = math.log(nb_parent_deviation)
		except ValueError:
			UCB_factor = None
		
		return(UCB_factor, max_nb_descendants)
	
	def get_score(self, node, UCB_factor, max_nb_descendants, nb_ads_remain, partition_tree):
		"""Compute the score of a node
		
		Parameters:
			self (Model object): Model object concerned.
			
			node (Node object, i.e. dict): node whose score is evaluated.
			
			UCB_factor (float): the UCB prefactor log(T) where T is the number of real energy records stored in the node.
			
			max_nb_descendants (int): the maximum number of descendants among the siblings of the node.
			
			nb_ads_remain (int): number of adsorbates that remain to be placed on the surface.
			
			partition_tree (SitesPartitionTree object): partition tree of current configuration being constructed.
		
		Returns:
			UCB_score = KL-UCB(DFT_deviation)
			          or 1/bias_ratio (if random sampling is enabled via the self.random flag)
		
		Computational details:
			None
		"""
		# If random exploration is requested, return constant score
		if self.random:
			return(0)
		
		# Retrieve population bias correction term
		if self.population_bias:
			bias_ratio = partition_tree.get_bias_ratio(node['partition_label'], nb_ads_remain, max_nb_descendants)
		else:
			bias_ratio = 1
		
		node_storage = node['storage']
		
		# Compute KL-UCB score for DFT energy deviation
		# First, retrieve bias_ratio corrected number of adsorption energy data
		nb_deviations = node_storage.nb_deviation*node_storage.quality_deviation*bias_ratio
		
		# If exhaustive exploration is requested, score is the inverse/opposite of the number of explorations (taking into account exploration biases if requested)
		if self.exhaustive_search:
			return(-nb_deviations)
		
		# Second, compute KL-UCB score on DFT deviation
		if nb_deviations == 0:
			KL_UCB = math.inf
		else:
			# Retrieve score average
			mean_deviation = node_storage.mean_abs_deviation
			
			if self.min_range is None:
				# Use global min/max abs deviations
				root_storage = self.tree.nodes['root']['storage']
				extrema_deviation = root_storage.min_abs_deviation, root_storage.max_abs_deviation
				if hasattr(self, 'add_global_margin'):
					extrema_deviation = extrema_deviation[0] - self.add_global_margin, extrema_deviation[1] + self.add_global_margin
			else:
				# Estimate dist extrema from empirical extrema assuming uniform distribution
				extrema_deviation = node_storage.estimate_extrema_abs_deviation(min_range=self.min_range)
				if hasattr(self, 'max_range'):
					empirical_center = (node_storage.min_abs_deviation+node_storage.max_abs_deviation)/2
					extrema_deviation = max(extrema_deviation[0], empirical_center-self.max_range/2), min(extrema_deviation[1], empirical_center+self.max_range/2)
			
			# Compute UCB score on real energy
			KL_UCB = node_storage.KL_UCB_score(mean_deviation, *extrema_deviation, nb_deviations, UCB_factor)
			if hasattr(self, 'score_mean_no_confidence'):
				KL_UCB = mean_deviation
			elif hasattr(self, 'score_max_no_confidence'):
				KL_UCB = node_storage.max_abs_deviation
			elif hasattr(self, 'score_min_no_confidence'):
				KL_UCB = node_storage.min_abs_deviation
			elif hasattr(self, 'score_range_no_confidence'):
				KL_UCB = node_storage.max_abs_deviation - node_storage.min_abs_deviation
			elif hasattr(self, 'score_UCB1'):
				KL_UCB = node_storage.UCB1_score(mean_deviation, *extrema_deviation, nb_deviations, UCB_factor)
		
		if DEBUG_MODE: print('score components', bias_ratio, (mean_deviation, extrema_deviation) if nb_deviations > 0 else (None, None))
		
		return(KL_UCB)
	
	def set_unsearchable(self, node):
		"""Apply unsearchable status to given node, and ancestors, if applicable."""
		# Add current exploration depth to the node unsearchable status
		if DEBUG_MODE: print('set unsearcheable on', node)
		self.tree.nodes[node]['unsearchable'].add(self.nb_adsorbates)
		
		# Update ancestors if necessary, by using a DFS traversal, stopping as soon as possible
		nodes = [node]
		while nodes:
			# Retrieve next node (note: next node cannot have already been treated, since we stop on a branch as soon as we find a searchable branch, and we are on a branch leading to cur_node that used to be searchable)
			cur_node = nodes.pop()
			# If cur_node has at least one searchable child, the backpropagation is stopped for this branch (thanks to the LIFO structure of nodes)
			if all(self.nb_adsorbates in self.tree.nodes[child]['unsearchable'] for child in self.tree.successors(cur_node)):
				# Set node unsearchable
				if DEBUG_MODE: print('unsearchable set on', cur_node)
				self.tree.nodes[cur_node]['unsearchable'].add(self.nb_adsorbates)
				
				# Add predecessors to nodes to explore (potentially non-searchable)
				nodes.extend(self.tree.predecessors(cur_node))
	
	def unset_unsearchable(self, node):
		"""Remove unsearchable status to given node, and ancestors."""
		if DEBUG_MODE: print('unset unsearcheable on', node)
		
		# Update ancestors if necessary, by using a DFS traversal, stopping as soon as possible
		nodes = [node]
		while nodes:
			# Retrieve next (yet untreated) node
			cur_node = nodes.pop()
			
			# Unset unsearchable status if applicable or stop backpropagation for this branch
			if self.nb_adsorbates in self.tree.nodes[cur_node]['unsearchable']:
				self.tree.nodes[cur_node]['unsearchable'].remove(self.nb_adsorbates)
				
				# Add predecessors to nodes to explore (potentially non-searchable)
				nodes.extend(self.tree.predecessors(cur_node))
	
	def choose_most_promising(self, candidates_nodes):
		"""Choose a node based on its estimated benefits on the model Hamiltonian
		
		Parameters:
			candidates_nodes (iterable): list of nodes keys to choose from.
		"""
		# Initialize best score
		best_score, best_node = ((0, 0, -math.inf), -math.inf, -math.inf), (None, None)
		
		# Evaluate requested nodes
		for node, config, mean_pattern_path in candidates_nodes:
			# Copy current model and model handler
			model_handler_copy = copy.deepcopy(self.model_handler)
			
			# Compute proximity score as opposite average many-body patterns path length
			score_proximity = -mean_pattern_path
			
			# Compute energy score as predicted absolute adsorption energy deviation
			patterns_dict = self.tree.nodes[node]['storage'].node_patterns
			if hasattr(self, 'cheating_max_index'):
				for index in tuple(patterns_dict.keys()):
					if index > self.cheating_max_index:
						del patterns_dict[index]
			score_deviation = model_handler_copy.predict_deviation(patterns_dict)
			
			# Update model, and compute estimated model covariance change
			_, (var_change_guess, var_change_emp), nb_new_var = model_handler_copy.add_config_to_model(patterns_dict, None, new_variances_guess=self.guess_variance)
			
			# Compute variance score as opposite of trace of covariance matrix change (# Add bonus for new var discovered)
			score_var = (model_handler_copy.model.nb_core_obs, nb_new_var, -np.trace(var_change_emp))
			
			# Update best node if applicable, using lexicographic ordered score
			score = (score_var, score_deviation, score_proximity)
			if DEBUG_MODE: print('score promising config', config.occupied_sites, patterns_dict, score)
			if score > best_score:
				best_node = (node, config)
				best_score = score
		
#		print(best_score, self.model.nb_core_config) # DEBUG
		
		return(best_node)
	
	def initialize_model(self):
		"""Initialize linear model Hamiltonian with all single body terms (corresponding to all reachable OccupiedSites object)"""
		# Retrieve all OccupiedSite objects defined
		sites_partition_tree = self.sites_partition_tree
		occupied_sites = sites_partition_tree.tree.successors(sites_partition_tree.occupied_site_root)
		
		# Initialize one-body terms
		for occupied_site in sorted(occupied_sites):
			# Create associated abstract configuration
			config = Configuration(self.sites_partition_tree, with_partition_tree=False)
			config.occupied_sites.append(occupied_site)
			
			# Extract config patterns multiplicities
			config_aos = AbstractOccupiedSurface(config, self.surface, self.equivalency_storage, max_length=self.max_length_cutoff)
			patterns = config_aos.get_patterns(max_order=self.body_term_order, max_sites=self.max_sites_cutoff)
			patterns_dict = PartitionStorage.convert_patterns(patterns)
			
			# Discard if single body configuration contains only terms already seen
			if all(np.in1d(tuple(patterns_dict.keys()), self.model_handler.active_indexes)):
				continue
			
			# Update full model and 1-body model (with new pattern described)
			if occupied_site.dft_energy:
				dft_energy = occupied_site.dft_energy
			else:
				dft_energy = self.compute_adsorption_energy(config)
			self.model_handler.add_config_to_model(patterns_dict, dft_energy, new_contrib_guess=0, new_variances_guess=self.guess_variance, initialize=True, duplicates=True)
	
	def find_config_with_pattern(self, pattern_index, original=True):
		"""Use a random-based tree search among minimal configurations until the target pattern is found.
		
		Parameters:
			pattern_index (int): pattern index of the targeted pattern.
				Note: this index must be valid (i.e. correspond to an already defined pattern).
			
			original (bool, optional): whether to search for original pattern index, or merged index.
				Default: True (i.e. search for configurations containing patterns whose original index is `pattern_index`.
		
		Returns:
			config (Configuration object): minimal configuration containing at least one targeted pattern.
		"""
		# Get any associated pattern
		patterns = self.equivalency_storage.get_patterns_from_index(pattern_index, original=original)
		assert patterns, 'Cannot find corresponding pattern to define search parameters: pattern_index must correspond to an assigned index.'
		pattern = patterns[0]
		
		# Get number of adsorbates in pattern (minimal number of adsorbates)
		nb_adsorbates_target = len(pattern.generic_sites)
		
		# Get lengths of abstract paths (in number of intermediary sites)
		path_lengths = [len(path[0]) for path in pattern.generic_paths]
		
		# Get maximal abstract path length (in number of sites, including endpoints)
		max_length = max(path_lengths + [0]) + 2
		
		# Get total number of sites involved
		nb_sites = nb_adsorbates_target + sum(path_lengths)
		
		# Save current number of adsorbates and set new target
		nb_adsorbates_state = self.nb_adsorbates
		self.nb_adsorbates = nb_adsorbates_target
		
		# Save random state and activate random exploration
		random_state = self.random
		self.random = True
		
		# Search configurations until targeted pattern is found
		config = None
		found = False
		while not found:
			# Generate random configuration
			_, config = self.select_configuration()
			
			# Extract patterns
			config_aos = AbstractOccupiedSurface(config, self.surface, self.equivalency_storage, max_length=max_length)
			patterns = config_aos.get_patterns(max_order=nb_adsorbates_target, max_sites=nb_sites)
			
			# Check if targeted pattern is found
			for pattern in patterns:
				if pattern.get_index(original=original) == pattern_index:
					found = True
					break
		
		# Reset MCTS configuration
		self.nb_adsorbates = nb_adsorbates_state
		self.random = random_state
		
		return(config)
	
	def populate_with_external_config(self, external_config, energy=None, target_model_handler=None):
		"""Force the selection and update of the MCTS framework with an external configuration
		
		Parameters:
			external_config (Configuration object): external configuration to include in the current MCTS framework instance.
				Note: this final configuration should be compatible with the current site partition tree (i.e. contain occupied sites with properties matching available occupied sites)
			
			energy (float, optional): adsorption energy corresponding to `external_config`
				Note: if None, the adsorption energy is recomputed
				Default: None
		"""
		# Convert external configuration into current framework (i.e. embed external occupied sites into current site partition tree)
		target_config = Configuration(self.sites_partition_tree, with_partition_tree=False)
		available_occupied_sites = tuple(target_config.sites_partition_tree.tree.successors(target_config.sites_partition_tree.occupied_site_root))
		for occupied_site in external_config.occupied_sites:
			# Populate target_config with occupied site in current sites_partition_tree having same properties
			identical_occupied_sites = [avail_occ_site for avail_occ_site in available_occupied_sites if avail_occ_site.properties == occupied_site.properties]
			assert len(identical_occupied_sites) > 0, 'external_config is not compatible with current MCTS instance (i.e. some imported occupied sites have no equivalents among available occupied sites)'
			target_config.occupied_sites.append(identical_occupied_sites[0])
		
		# Save current number of adsorbates and set new target
		nb_adsorbates_state = self.nb_adsorbates
		nb_pre_exp_state = self.nb_pre_exploration_steps
		self.nb_adsorbates = len(target_config.occupied_sites)
		self.nb_pre_exploration_steps = 1
		
		# Start inclusion
		self.target_config = target_config
		self.target_config_energy = energy
		self.single_run(target_model_handler=target_model_handler)
		
		# Reset MCTS configuration
		self.target_config = None
		self.target_config_energy = None
		self.nb_adsorbates = nb_adsorbates_state
		self.nb_pre_exploration_steps = nb_pre_exp_state
	
	def populate_with_database(self, database_filename=None, query='', verbose=True, target_model_handler=None):
		# Run query and retrieve configs and associated energies
		configs, energies = self.geometries_handler.query_database_configs(database_filename=database_filename, query=query, with_energy=True)
		
		# Add each recorded geometries, one at a time
		for config, energy in zip(configs, energies):
			print('Treating configuration: {}'.format(config))
			self.populate_with_external_config(config, energy=energy, target_model_handler=target_model_handler)
			if verbose: print(self.stats_report())
	
	def stats_report(self):
		# Get number of calculations included
		training_set_size = self.tree.nodes['root']['storage'].nb_deviation
		
		# Get average absolute energy deviation
		mean_abs_deviation_per_ads = self.tree.nodes['root']['storage'].mean_abs_deviation
		
		# Retrieve number of distinct patterns considered in training set
		nb_active_indexes = len(self.model_handler.active_indexes)
		
		# Retrieve number of distinct patterns/exclusions seen so far during exploration/inititialization
		nb_distinct_patterns = self.equivalency_storage.nb_distinct_patterns
		
		# Retrieve number of non-redundant observations
		nb_core_obs = self.model.nb_core_obs
		
		# Retrieve enhanced covariance matrix (unexplored space replaced with guess variance)
		covariance_matrix = self.model.variance_parameters
		
		# Compute trace of enhanced covariance matrix
		covariance_trace = np.trace(covariance_matrix)
		
		# Compute inverse raw normalized covariance matrix eigenvalues
		raw_covariance_eig_inv = np.linalg.eigvalsh(np.dot(self.model.observations.T, self.model.observations))
		
		# Evaluate trace of raw covariance matrix
		raw_covariance_trace = sum(1/raw_covariance_eig_inv)
		
		# Evaluate trace of raw covariance matrix with kernel excluded (restricted to its image subspace)
		raw_covariance_trace_ker_excl = sum(1/np.sort(raw_covariance_eig_inv)[-nb_core_obs:])
		
		# Compute cross-validation estimator of current model
		cross_val = self.model_handler.run_cross_validation()
		
		return({'training_set_size': training_set_size,
		        'mean_abs_deviation_per_adsorbate': mean_abs_deviation_per_ads,
		        'nb_active_patterns': nb_active_indexes,
		        'nb_distinct_patterns': nb_distinct_patterns,
		        'nb_nonredundant_observations': nb_core_obs,
		        'covariance_trace': covariance_trace,
		        'raw_covariance_trace': raw_covariance_trace,
		        'raw_covariance_trace_ker_excl': raw_covariance_trace_ker_excl,
		        'cross_validation': cross_val})
	
	@property
	def nb_adsorbates(self):
		return(self._nb_adsorbates)
	
	@nb_adsorbates.setter
	def nb_adsorbates(self, nb_adsorbates):
		# Pre-compute lexicographic order bias correction ratios for new requested exploration-depth
		self.sites_partition_tree.update_lexicographic_descendants(nb_adsorbates)
		
		# Set new exploration depth
		self._nb_adsorbates = nb_adsorbates
	
	@property
	def manual(self):
		return(self._manual)
	
	@manual.setter
	def manual(self, value):
		self._manual = value
		if value:
			self.nb_pre_exploration_steps = 1
			self.random = False
			self.exhaustive_search = False

class AbstractOccupiedSurfaceZacros(object):
	"""Abstract occupied surface with site numbers, representing abstract occupied sites and abstract paths between them"""
	
	def __init__(self, configuration, surface, max_length=None):
		# Store surface
		self.surface = surface
		
		# Initiate abstract graph, nodes are adsorbate binding sites, edges are abstract paths
		self.graph = nx.DiGraph()
		
		# Create dummy equivalency_storage (Zacros clusters do not rely on automatic symmetry detection)
		dummy_equivalency_storage = EquivalencyStorage()
		
		binding_sites_couples = []
		# Get all couples (ads_bind_site, surface_site_number, dentate_index)
		for occupied_site in configuration.occupied_sites:
			ads_sites, surf_sites_numbers = AbstractBindingSite.from_partitioned_occupied_site(occupied_site, configuration.sites_partition_tree, equivalency_storage=dummy_equivalency_storage)
			binding_sites_couples.extend(zip(ads_sites, surf_sites_numbers, range(len(ads_sites))))
		
		# Create abstract graph nodes (adsorbate binding sites)
		for node_index, (ads_site, surf_site_number, dentate_index) in enumerate(binding_sites_couples):
			surface_abstract_site = surface.graph.nodes[surf_site_number]['abstract_site']
			self.graph.add_node(node_index, abstract_site=ads_site, surface_abstract_site=surface_abstract_site, site_number=surf_site_number, dentate_index=dentate_index)
		
		# Create abstract edges (sequences of abstract paths between adsorbate binding sites)
		# TODO Note that only one-way paths could be considered (i.e. undirected graph), as return paths could be obtained by reversing them on the fly
		for data_site1, data_site2 in itertools.combinations(enumerate(binding_sites_couples), 2):
			# Get surface binding sites info
			node1, (_, surf_site1_number, _) = data_site1
			node2, (_, surf_site2_number, _) = data_site2
			
			# Retrieve all shortest paths between binding sites on surface (possible lateral interactions)
			abstract_paths, site_number_paths = surface.get_shortest_paths(surf_site1_number, surf_site2_number, max_length=max_length)
			
			# Store abstract paths in new edge, if sites are close enough on surface
			if abstract_paths:
				self.graph.add_edge(node1, node2, abstract_paths=abstract_paths, site_number_paths=site_number_paths)
			
			# Idem for reverse paths
			abstract_paths, site_number_paths = surface.get_shortest_paths(surf_site2_number, surf_site1_number, max_length=max_length)
			if abstract_paths:
				self.graph.add_edge(node2, node1, abstract_paths=abstract_paths, site_number_paths=site_number_paths)
	
	def get_clusters(self, max_order, max_sites=None, mirror=True, absl_orientation=False):
		assert(max_sites is None or max_sites > 0)
		assert(max_order > 0)
		
		found_clusters = []
		
		# Generate all 1-body-term clusters
		for node in self.graph.nodes:
			ads_types = (self.graph.nodes[node]['abstract_site'].chemical_type,)
			ads_dentate_indexes = (self.graph.nodes[node]['dentate_index'],)
			surf_types = (self.graph.nodes[node]['surface_abstract_site'].chemical_type,)
			found_clusters.append(ClusterZacros(ads_types=ads_types, ads_dentate_indexes=ads_dentate_indexes, surf_types=surf_types, mirror=mirror, absl_orientation=absl_orientation))
		
		# Generate all 2+-body-terms clusters in a brute force manner
		for body_order in range(2, max_order+1):
			# Generate all body_order-body-terms clusters in a brute force manner, by enumerating all sets of adsorbates with size body_order
			for node_indexes in itertools.combinations(range(self.graph.number_of_nodes()), body_order):
				# Retrieve occupied site numbers
				nodes_by_site_numbers = dict((self.graph.nodes[node_index]['site_number'], self.graph.nodes[node_index]) for node_index in node_indexes)
				# Generate all abstract paths (only surface sites numbers)
				site_numbers_paths_path = (self.graph.edges[u, v]['site_number_paths'] for u, v in itertools.combinations(node_indexes, 2))
				# Generate all possible simple paths to detect minimal signature and automorphism-based graph multiplicity (i.e. number of identical minimal signatures by permutation)
				min_signature = None
				multiplicity = None
				for site_numbers_sequence in itertools.product(*site_numbers_paths_path):
					# Get all site numbers involved
					all_surf_site_numbers = set(itertools.chain.from_iterable(site_numbers_sequence))
					# Exclude patterns with more total distinct sites than max_sites
					if max_sites and len(all_surf_site_numbers) > max_sites:
						continue
					# Try all permutations
					min_cluster = None
					automorphism_multiplicity = 0
					for ordered_sites_numbers in itertools.permutations(all_surf_site_numbers):
						# Filter out some non-minimal signatures (force first site is occupied and first two are adjacent)
						if ordered_sites_numbers[0] not in nodes_by_site_numbers:
							continue
						if tuple(ordered_sites_numbers[0:2]) not in self.surface.graph.edges:
							continue
						# Retrieve adsorbate types in order
						ads_types = [(nodes_by_site_numbers[site_number]['abstract_site'].chemical_type if site_number in nodes_by_site_numbers else None) for site_number in ordered_sites_numbers]
						# TODO: Filter out some non-minimal signatures (force ordered chemical types)
						# Retrieve dentate indexes in order
						ads_dentate_indexes = [(nodes_by_site_numbers[site_number]['dentate_index'] if site_number in nodes_by_site_numbers else None) for site_number in ordered_sites_numbers]
						# Retrieve surface site types in order
						surf_types = [self.surface.graph.nodes[site_number]['abstract_site'].chemical_type for site_number in ordered_sites_numbers]
						# Create Zacros-based cluster (i.e. signature)
						cluster = ClusterZacros(ads_types=ads_types, ads_dentate_indexes=ads_dentate_indexes, surf_types=surf_types, site_numbers=ordered_sites_numbers, surface=self.surface, mirror=mirror, absl_orientation=absl_orientation)
						# Update minimal signature and/or automorphism-based graph multiplicity
						if min_cluster is None or cluster.properties < min_cluster.properties:
							min_cluster = cluster
							automorphism_multiplicity = 1
						elif cluster.properties == min_cluster.properties:
							automorphism_multiplicity += 1
					# Update minimal signature and/or automorphism-based graph multiplicity
					if min_signature is None or min_cluster.properties < min_signature.properties:
						min_signature = min_cluster
						multiplicity = automorphism_multiplicity
				# Add (minimal) detected cluster (and set multiplicity)
				if min_signature is None:
					continue
				min_signature.set_multiplicity(multiplicity)
				found_clusters.append(min_signature)
		
		return(found_clusters)

class ClusterZacros(object):
	def __init__(self, ads_types=None, ads_dentate_indexes=None, surf_types=None, site_numbers=None, surface=None, absl_orientation=False, mirror=True):
		self.ads_types = ads_types
		self.ads_dentate_indexes = ads_dentate_indexes
		self.surf_types = surf_types
		
		# Define adjacency by enumerating and testing all possible edges, sorted
		self.adjacency = set()
		for index1, index2 in itertools.combinations(range(len(self.surf_types)), 2):
			assert(index1 < index2)
			edge = (site_numbers[index1], site_numbers[index2])
			if edge in surface.graph.edges:
				self.adjacency.add((index1, index2))
		self.adjacency = tuple(sorted(self.adjacency))
		
		# Support absolute orientation by storing orientation of first edge
		self.absl_orientation = None
		if absl_orientation:
			index1, index2 = self.adjacency[0]
			first_edge = (site_numbers[index1], site_numbers[index2])
			self.absl_orientation = surface.graph.edges[first_edge]['abstract_connection'].get_outermost_orientation(in_angle=False)
		
		# Compute all angles
		self.angles = set()
		for indexes in itertools.combinations(range(len(self.surf_types)), 3):
			for index1, index2, index3 in itertools.permutations(indexes):
				# Remove duplicates (i.e. reverse angle)
				if index1 > index3:
					continue
				# Get corresponding edges and check that they exist
				edge1, edge2 = (site_numbers[index1], site_numbers[index2]), (site_numbers[index2], site_numbers[index3])
				if (edge1 not in surface.graph.edges) or (edge2 not in surface.graph.edges):
					continue
				# Compute angles
				angle1 = surface.graph.edges[edge1]['abstract_connection'].get_outermost_orientation(in_angle=True)
				angle2 = surface.graph.edges[edge2]['abstract_connection'].get_outermost_orientation(in_angle=False)
				self.angles.add((index1, index2, index3, angles_diff(angle1, angle2)))
		self.angles = tuple(sorted(self.angles))
		
		# Support mirror images (store only one version of angles among : normal or all opposite)
		self.mirror = mirror
		# If mirror images are requested, store only version where first angle is positive
		if self.mirror:
			# Check if first angle is negative
			if self.angles and self.angles[0][-1] < 0:
				# Switch all angles
				self.angles = tuple((u,v,w,-angle) for (u,v,w,angle) in self.angles)
		
		# Defines name
		self.name = None
		
		# Default multiplicity
		self.graph_multiplicity = 1
	
	def set_multiplicity(self, multiplicity):
		self.graph_multiplicity = multiplicity
	
	@property
	def properties(self):
		return(tuple('{}'.format(item) for item in self.ads_types), tuple('{}'.format(item) for item in self.ads_dentate_indexes), tuple('{}'.format(item) for item in self.surf_types), self.adjacency, self.angles, self.mirror, self.absl_orientation, self.graph_multiplicity)
	
	def __hash__(self):
		return(self.properties.__hash__())
	
	def __eq__(self, other):
		return(self.properties == other.properties)
	
	def __repr__(self):
		# Name
		str_version = 'cluster {}\n'.format(self.name)
		# Number of sites
		str_version += '\tsites {}\n'.format(len(self.surf_types))
		# Adjacency
		if self.adjacency:
			neighbors_format = ('{}-{}'.format(u+1, v+1) for u, v in self.adjacency)
			str_version += '\tneighbors {}\n'.format(' '.join(neighbors_format))
		# Occupied sites
		str_version += 'lattice_state\n'
		index = 0
		for ads_type, dentate_index in zip(self.ads_types, self.ads_dentate_indexes):
			if ads_type is None:
				str_version += '\t\t& &  &\n'
			else:
				str_version += '\t\t{} {}  {}\n'.format(index+1, ads_type, dentate_index+1)
				index += 1
		# Site types
		str_version += '\tsite_types\t{}\n'.format(' '.join(self.surf_types))
		# Graph multiplicity
		str_version += '\tgraph_multiplicity\t{}\n'.format(self.graph_multiplicity)
		# Angles
		if self.angles:
			angles_format = ('{}-{}-{}:{:.1f}'.format(u+1, v+1, w+1, angle) for (u, v, w, angle) in self.angles)
			str_version += '\tangles\t{}\n'.format(' '.join(angles_format))
		# Mirror image
		if not self.mirror:
			str_version += '\tno_mirror_images\n'
		# Absolute orientation
		if self.absl_orientation is not None:
			str_version += '\tabsl_orientation\t{}\n'.format(self.absl_orientation)
		# Energy contribution
		str_version += '\tcluster_energy\t???\n'
		str_version += 'end_cluster\n'
		
		return(str_version)

if __name__ == '__main__':
	# Create MCTS instance
	mcts = MCTS(body_term_order=2)
	
	# Define adsorbate binding sites types
	A_site = AbstractBindingSite('A', 0, '+', mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', 120, '+', mcts.equivalency_storage), mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', 240, '+', mcts.equivalency_storage), mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', 0, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', 120, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	A_site.set_equivalent_to(AbstractBindingSite('A', 240, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	B1_site = AbstractBindingSite('B', 0, '+', mcts.equivalency_storage)
	B1_site.set_equivalent_to(AbstractBindingSite('B', 0, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	B2_site = AbstractBindingSite('B', 180, '+', mcts.equivalency_storage)
	B2_site.set_equivalent_to(AbstractBindingSite('B', 180, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	
	# Define surface sites types
	p_site = AbstractBindingSite('p', None, '+', mcts.equivalency_storage)
	p_site.set_equivalent_to(AbstractBindingSite('p', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	n_site = AbstractBindingSite('n', None, '+', mcts.equivalency_storage)
	n_site.set_equivalent_to(AbstractBindingSite('n', None, '-', mcts.equivalency_storage), mcts.equivalency_storage)
	
	# Define surface sites positions and connections
	for i in range(1,26):
		if i%2:
			mcts.surface.add_site(i, p_site)
		else:
			mcts.surface.add_site(i, n_site)
	for i in range(1,26):
		for j in (1,5,4,6):
			if 0 < i+j < 26:
				if j == 1:
					mcts.surface.add_connections(i, i+j, 1.0, -90)
				elif j == 5:
					mcts.surface.add_connections(i, i+j, 1.0, 180)
				elif j == 4:
					mcts.surface.add_connections(i, i+j, 1.4, 135)
				else:
					mcts.surface.add_connections(i, i+j, 1.4, -135)
	
	# Populate sites tree partition of possible occupied sites
	# Define tree structure
	sites_partition_tree = mcts.sites_partition_tree
	for ads_type in [('A',), ('B',)]:
		node_label_0 = '{}'.format(ads_type)
		new_partition = Partition(ads_type=ads_type)
		sites_partition_tree.add_partition_node(node_label_0, new_partition, parent_node=sites_partition_tree.tree_root)
		for site_atom_type in [('p',), ('n',)]:
			node_label_1 = '{}@{}'.format(ads_type, site_atom_type)
			new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type)
			sites_partition_tree.add_partition_node(node_label_1, new_partition, parent_node=node_label_0)
			for site_type in np.random.choice([('top',)], 1):
				node_label_2 = '{}@{}_{}'.format(ads_type, site_atom_type, site_type)
				new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type)
				sites_partition_tree.add_partition_node(node_label_2, new_partition, parent_node=node_label_1)
				for site_number in ([(i,) for i in range(1, 26)[::2]] if 'p' in site_atom_type else [(i,) for i in range(1, 26)[1::2]]):
					node_label_3 = '{}@{}{}_{}'.format(ads_type, site_atom_type, site_number, site_type)
					new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number)
					sites_partition_tree.add_partition_node(node_label_3, new_partition, parent_node=node_label_2)
					for orientation in ([(0,)] if 'A' in ads_type else [(0,),(180,)]): # Orientation is relative to arbitrary axis
						node_label_4 = '{}({}°)@{}{}_{}'.format(ads_type, orientation, site_atom_type, site_number, site_type)
						new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation)
						sites_partition_tree.add_partition_node(node_label_4, new_partition, parent_node=node_label_3)
						for chirality in [('+',)]:
							node_label_5 = '{}{}'.format(chirality, node_label_4)
							new_partition = Partition(ads_type=ads_type, site_atom_type=site_atom_type, site_type=site_type, site_number=site_number, orientation=orientation, chirality=chirality)
							sites_partition_tree.add_partition_node(node_label_5, new_partition, parent_node=node_label_4)
	# Populate tree with OccupiedSite objects
	for final_partition in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
		ads_type, site_atom_type, _, site_number, orientation, _ = sites_partition_tree.get_node_partition(final_partition).properties
		dft_energy = (-3 if 'A' in ads_type else -2) + 1.5*(('A' in ads_type and 'p' in site_atom_type) or ('B' in ads_type and 'n' in site_atom_type))
		print(ads_type, site_atom_type, site_number, dft_energy)
		sites_partition_tree.add_occupied_site_node(OccupiedSite(site_number[0], None, ads_type, None, None, dihedral=orientation, dft_energy=dft_energy), final_partition)
	
	# Define translation + rotation symmetry
	def custom_symmetry_function(configuration_object):
		# Define configuration data
		partition_tree = configuration_object.partition_tree
		cur_node = configuration_object.current_node
		occupied_sites = configuration_object.occupied_sites
		tree_root = configuration_object.partition_tree_object.tree_root
		site_number_root = configuration_object.partition_tree_object.site_number_root
		
		# All sites numbers are equivalent for first adsorbate
		if cur_node == tree_root and len(occupied_sites) == 0:
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep site number 1
				if configuration_object.get_node_partition(site_number_node).site_number != (1,):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
		
		# Only some sites are not equivalent for second adsorbate
		if cur_node == tree_root and len(occupied_sites) == 1:
			for site_number_node in tuple(partition_tree.successors(site_number_root)):
				# Only keep sites number 2, 6 or 7
				if configuration_object.get_node_partition(site_number_node).site_number not in ((2,), (6,), (7,)):
					# Delete subtree
					configuration_object.delete_partition_subtree(site_number_node)
	sites_partition_tree.set_symmetry(custom_symmetry_function)
	
	# Initialize linear model with single body terms
	mcts.initialize_model()
	
	# Create target model Hamiltonian
	path = AbstractPath.from_surface_path([1,7], mcts.surface)
	index_target = Pattern(mcts.equivalency_storage, (B1_site, B2_site), (path,)).get_index()
	target_model_handler = copy.deepcopy(mcts.model_handler)
	target_model_handler.add_config_to_model({index_target: 1}, -3)
	
	# Set exploration depth to 2 adsorbates
	mcts.nb_adsorbates = 2
	
