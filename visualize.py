#!/usr/bin/env python3

# Modules imports
from RS4CEH import Partition
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import networkx as nx
import math
import numpy as np

try:
	import pygraphviz
	from networkx.drawing.nx_agraph import graphviz_layout
except ImportError:
	try:
		import pydot
		from networkx.drawing.nx_pydot import graphviz_layout
	except ImportError:
		raise ImportError("This example requires Graphviz and either PyGraphviz or pydot")

#
# Graph visualization
#

def circular_layout(tree, root='root', aligned_node=None):
	# Compute default circular layout
	all_pos = graphviz_layout(tree, prog='twopi', root=root)
	
	# Return raw layout if no rotation requested
	if aligned_node is None:
		return(all_pos)
	
	# Compute rotation parameters
	root_pos = np.array(all_pos[root])
	ref_vect = np.array(all_pos[aligned_node]) - root_pos
	ref_vect = ref_vect/np.linalg.norm(ref_vect)
	sin_angle, cos_angle = ref_vect
	rot_matrix = np.array([[cos_angle, -sin_angle], [sin_angle, cos_angle]])
	root_displacement = np.dot(rot_matrix, root_pos) - root_pos
	
	# Rotate layout to vertically align specified node with root
	for node, pos in all_pos.items():
		# Apply centered rotation matrix: rot_matrix.(pos-root_pos) + root_pos = rot_matrix.pos - (rot_matrix-1).root_pos = rot_matrix.pos - root_displacement
		all_pos[node] = np.dot(rot_matrix, pos) - root_displacement
	
	return(all_pos)

def dft_node_coloring(tree, cmap, default_color=(0.8,)*4):
	# Retrieve DFT energy deviation stored in nodes, and bounds
	min_deviation, max_deviation = float('+inf'), float('-inf')
	deviations = []
	for node in tree:
		# Retrieve storage data
		node_storage = tree.nodes[node]['storage']
		
		# Check if no energy deviation is available for the node
		if node_storage.node_abs_deviation is None:
			deviations.append(None)
			continue
		
		# Use node DFT energy deviation
		deviation = node_storage.node_abs_deviation
		
		# Store data and update bounds
		min_deviation = min(deviation, min_deviation)
		max_deviation = max(deviation, max_deviation)
		deviations.append(deviation)
	
	# Compute energy deviation range
	range_deviation = max_deviation - min_deviation
	if range_deviation == 0:
		range_deviation = 1
	
	# Compute corresponding node colors
	colors = [(cmap((deviation-min_deviation)/range_deviation) if deviation is not None else default_color) for deviation in deviations]
	return(colors)

def pred_node_coloring(tree, cmap, model_handler, default_color=(0.8,)*4):
	# Retrieve mean predicted energy deviation of nodes, and bounds
	min_deviation, max_deviation = float('+inf'), float('-inf')
	deviations = []
	for node in tree:
		# Retrieve storage data
		node_storage = tree.nodes[node]['storage']
		
		# Check if no energy deviation is available for the node
		if node_storage.nb_config == 0:
			deviations.append(None)
			continue
		
		# Use node mean predicted energy deviation
		deviation = node_storage.get_mean_pred_abs_deviation(model_handler)
		
		# Store data and update bounds
		min_deviation = min(deviation, min_deviation)
		max_deviation = max(deviation, max_deviation)
		deviations.append(deviation)
	
	# Compute energy deviation range
	range_deviation = max_deviation - min_deviation
	if range_deviation == 0:
		range_deviation = 1
	
	# Compute corresponding node colors
	colors = [(cmap((deviation-min_deviation)/range_deviation) if deviation is not None else default_color) for deviation in deviations]
	return(colors)

def node_partition_labelling(tree, sites_partition_tree):
	# Labels are returned as a node-index keyed dictionnary
	labels = dict()
	
	# Retrieve short labels for each node
	for node in tree:
		# Root node has label 'root'
		if node == 'root':
			labels[node] = 'root'
			continue
		
		# Retrieve node partition and parent node partition
		node_partition = sites_partition_tree.get_node_partition(tree.nodes[node]['partition_label'])
		parent_node_label = tuple(tree.predecessors(node))[0]
		prev_partition_label = tree.nodes[parent_node_label]['partition_label']
		# If parent node's partition is a final one, use an empty partition (since we start exploring the next adsorbate on a brand new partition tree)
		if prev_partition_label in sites_partition_tree.tree.successors(sites_partition_tree.final_partition_root):
			prev_partition = Partition()
		else:
			prev_partition = sites_partition_tree.get_node_partition(prev_partition_label)
		
		# Retrieve new properties introduced from parent to current node
		new_properties = [node_partition.__dict__[name] for name in node_partition.get_new_properties(prev_partition)]
		
		# Format handling of multiple properties (multidentates and/or multiple property types)
		properties_label = []
		for new_prop in new_properties:
			properties_label.append(','.join(map(str, new_prop)))
		node_label = '_'.join(properties_label)
		
		# Store node label
		labels[node] = node_label
	
	return(labels)

def draw(mcts, filename, edges_width_scale=1, node_size=20, figsize=(8,8), cmap=None, node_coloring_method='dft_deviation'):
	"""Draw the exploration tree of an MCTS object
	
	Parameters:
		mcts (MCTS object): MCTS object to draw.
		
		filename (str): filename to save generated figure.
	"""
	# Retrieve MCTS tree
	tree = mcts.tree
	
	# Compute circular layout, ensuring constant rotation reference
	pos = circular_layout(tree, root='root', aligned_node=1)
	
	# Compute edges widths (logarithmic in path visits)
	width = [edges_width_scale*math.log(1+tree.nodes[v]['storage'].nb_config) for u, v in tree.edges]
	
	# Compute nodes colors
	cmap = plt.get_cmap(cmap)
	if node_coloring_method == 'dft_deviation':
		colors = dft_node_coloring(tree=tree, cmap=cmap)
	elif node_coloring_method == 'pred_deviation':
		colors = pred_node_coloring(tree=tree, cmap=cmap, model_handler=mcts.model_handler)
	else:
		colors = node_coloring_method(tree=tree, cmap=cmap, model_handler=mcts.model_handler)
	
	# Retrieve nodes labels
	labels = node_partition_labelling(tree, mcts.sites_partition_tree)
	
	# Create figure and save it
	fig = plt.figure(figsize=figsize)
	nx.draw(tree, pos, alpha=1, width=width, node_color=colors, labels=labels, arrows=False, with_labels=True)
	plt.axis('equal')
	plt.savefig(filename)
	plt.close(fig)
